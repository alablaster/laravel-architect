<?php


namespace Alablaster\Architect\Tests\Feature\Frontend\Managers;


use Alablaster\Architect\Domain\Core\Traits\InteractsWithFilesTrait;
use Alablaster\Architect\Domain\Core\Traits\LoadsMapDefinitions;
use Alablaster\Architect\Domain\Frontend\Managers\FrontendManager;
use Alablaster\Architect\Tests\TestCase;

class FrontendManagerTest extends TestCase
{
	public $frontend;

	use LoadsMapDefinitions;
	use InteractsWithFilesTrait;

	protected function setUp(): void
	{
		parent::setUp();
		$this->deleteFile(base_path('routes/primary/web.php'));

		config(['architect.frontend' => 'blade']);

		$map = [
			'web' => [
				'primary' => [
					'children' => [
						'secondary'
					],
					'frontend' => 'blade'
				]
			],
			'api' => [
				'primary' => [
					'children' => [
						'secondary'
					]
				]
			]
		];

		$this->loadMap($map);

		$this->frontend = new FrontendManager($this->map['web']);
	}

	public function test_that_it_generates_the_route_files()
	{
		$this->assertFileExists(base_path('/routes/primary/web.php'));
	}

	public function test_the_route_file_includes_required_fies()
	{
		$file = $this->openFile(base_path('/routes/primary/web.php'));

		$this->assertStringContainsString('require __DIR__ . \'/secondary/web.php\';', $file);
	}
}