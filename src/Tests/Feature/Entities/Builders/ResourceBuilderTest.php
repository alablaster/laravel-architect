<?php


namespace Alablaster\Architect\Tests\Feature\Entities\Builders;


use Alablaster\Architect\Domain\Core\Traits\InteractsWithFilesTrait;
use Alablaster\Architect\Domain\Entities\Builders\ResourceBuilder;
use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Tests\TestCase;

class ResourceBuilderTest extends TestCase
{
	use InteractsWithFilesTrait;

	protected $resource, $resourceCollection, $indexResource, $relationshipResource, $relationshipCollection, $relationshipIndexResource;
	protected function setUp(): void
	{
		parent::setUp();

		$entity = new Entity('Something');

		new ResourceBuilder($entity);
		$this->resource = $this->openFile(base_path('/app/Http/Resources/Something/SomethingResource.php'));
		$this->resourceCollection = $this->openFile(base_path('/app/Http/Resources/Something/SomethingCollection.php'));
		$this->indexResource = $this->openFile(base_path('/app/Http/Resources/Something/SomethingIndexResource.php'));
		$this->relationshipCollection = $this->openFile(base_path('/app/Http/Resources/Something/Relationship/SomethingRelationshipCollection.php'));
		$this->relationshipResource = $this->openFile(base_path('/app/Http/Resources/Something/Relationship/SomethingRelationshipResource.php'));
		$this->relationshipIndexResource = $this->openFile(base_path('/app/Http/Resources/Something/Relationship/SomethingRelationshipIndexResource.php'));

	}

	public function test_it_creates_the_files()
	{
		$this->assertFileExists(base_path('/app/Http/Resources/Something/SomethingResource.php'));
		$this->assertFileExists(base_path('/app/Http/Resources/Something/SomethingCollection.php'));
		$this->assertFileExists(base_path('/app/Http/Resources/Something/SomethingIndexResource.php'));
		$this->assertFileExists(base_path('/app/Http/Resources/Something/Relationship/SomethingRelationshipCollection.php'));
		$this->assertFileExists(base_path('/app/Http/Resources/Something/Relationship/SomethingRelationshipResource.php'));

	}

	public function test_it_is_using_the_correct_stub()
	{
		$this->assertStringContainsString('resource.php.stub', $this->resource);
		$this->assertStringContainsString('resource-collection.php.stub', $this->resourceCollection);
		$this->assertStringContainsString('index-resource.php.stub', $this->indexResource);
		$this->assertStringContainsString('relationship-collection.php.stub', $this->relationshipCollection);
		$this->assertStringContainsString('relationship-resource.php.stub', $this->relationshipResource);
		$this->assertStringContainsString('relationship-index-resource.php.stub', $this->relationshipIndexResource);

	}

	public function test_resource_populates_correctly()
	{
		$placeholders = [
			'entity.namespace' => 'App\\Http\\Resources\\Something;',
			'entity.name.studly' => 'SomethingResource',
			'entity.fullName'=> 'App\\Models\\Something',
			'entity.dotFullName' => 'something.show',
			'entity.name.plural.camel' => 'somethings',
			'attributeMap' => '\'name\' => $this->resource->name',
			'relationships' => ''
		];

		foreach($placeholders as $placeholder => $replacement) {
			$this->assertStringNotContainsString('@<<' . $placeholder . '>>@', $this->resource);
			$this->assertStringContainsString( $replacement, $this->resource);
		}
	}

	public function test_resource_collection_populates_correctly()
	{
		$placeholders = [
			'entity.namespace' => 'App\\Http\\Resources\\Something;',
			'entity.name.studly' => 'SomethingCollection',
		];

		foreach($placeholders as $placeholder => $replacement) {
			$this->assertStringNotContainsString('@<<' . $placeholder . '>>@', $this->resourceCollection);
			$this->assertStringContainsString( $replacement, $this->resourceCollection);
		}
	}

	public function test_index_resource_populates_correctly()
	{
		$placeholders = [
			'entity.namespace' => 'App\\Http\\Resources\\Something;',
			'entity.name.studly' => 'SomethingIndexResource',
			'entity.dotFullName' => 'something.show',
			'entity.name.plural.camel' => 'somethings',
			'attributeMap' => '\'name\' => $this->resource->name',
			'relationships' => ''
		];

		foreach($placeholders as $placeholder => $replacement) {
			$this->assertStringNotContainsString('@<<' . $placeholder . '>>@', $this->indexResource);
			$this->assertStringContainsString( $replacement, $this->indexResource);
		}
	}

	public function test_relationship_collection_populates_correctly()
	{
		$placeholders = [
			'entity.namespace' => 'App\\Http\\Resources\\Something\\Relationship;',
			'entity.name.studly' => 'SomethingRelationshipCollection',
		];

		foreach($placeholders as $placeholder => $replacement) {
			$this->assertStringNotContainsString('@<<' . $placeholder . '>>@', $this->relationshipCollection);
			$this->assertStringContainsString( $replacement, $this->relationshipCollection);
		}
	}

	public function test_relationship_resource_populates_correctly()
	{
		$placeholders = [
			'entity.namespace' => 'App\\Http\\Resources\\Something\\Relationship;',
			'entity.name.studly' => 'SomethingRelationshipResource',
			'entity.dotFullName' => 'something.show',
			'entity.name.plural.camel' => 'somethings',
			'attributeMap' => '\'name\' => $this->resource->name',
			'relationships' => ''
		];

		foreach($placeholders as $placeholder => $replacement) {
			$this->assertStringNotContainsString('@<<' . $placeholder . '>>@', $this->relationshipResource);
			$this->assertStringContainsString( $replacement, $this->relationshipResource);
		}
	}

	public function test_relationship_index_resource_populates_correctly()
	{
		$placeholders = [
			'entity.namespace' => 'App\\Http\\Resources\\Something\\Relationship;',
			'entity.name.studly' => 'SomethingRelationshipIndexResource',
			'entity.dotFullName' => 'something.show',
			'entity.name.plural.camel' => 'somethings',
			'attributeMap' => '\'name\' => $this->resource->name',
			'relationships' => ''
		];

		foreach($placeholders as $placeholder => $replacement) {
			$this->assertStringNotContainsString('@<<' . $placeholder . '>>@', $this->relationshipIndexResource);
			$this->assertStringContainsString( $replacement, $this->relationshipIndexResource);
		}
	}
}