<?php

return [
	"api_version" => 'v1',
	"auth" => true,
	"frontends" => [
		'livewire' => false,
		'blade' => false,
		'inertia' => false,
		'api' => false,
	],
	"frontend" => 'blade',
	"make" => [
		"model" => true,
		"migration" => true,
		"requests" => true,
		"resources" => true,
		"factory" => true,
		"manager" => true,
	],
	"mass_assignable" => true,
	"relationships" => [
		/*
		 * How should a relationship be treated when it is deleted?
		 *
		 * Options include 'restrict' or 'cascade'
		 */
		"on_delete" => 'restrict'
	],
	"settings_path" => '/blueprints.yaml',
	"path" => [
		"controller" => 'app/Http/Controllers',
		"factories" => 'database/factories',
		"livewire" => [
			"view" => "resources/views/livewire",
			"class" => "app/Http/Livewire",
			"test" => "tests/Livewire"
		],
		"manager" => 'app/Managers',
		"migration" => 'database/migrations',
		"model" => 'app/Models',
		"resources" => 'app/Http/Resources',
		"requests" => 'app/Http/Requests',
		"routes" => 'routes',
		"tests" => [
			"feature" => 'tests/feature',
			"unit" => 'tests/unit',
			"e2e" => 'tests/e2e'
		],
		"views" => 'resources/views',
	],
];
