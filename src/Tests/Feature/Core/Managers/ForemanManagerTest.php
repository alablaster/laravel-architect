<?php


namespace Alablaster\Architect\Tests\Feature\Core\Managers;


use Alablaster\Architect\Domain\Core\Managers\ForemanManager;
use Alablaster\Architect\Domain\Core\Traits\InteractsWithFilesTrait;
use Alablaster\Architect\Domain\Settings\Managers\SettingsManager;
use Alablaster\Architect\Tests\TestCase;

class ForemanManagerTest extends TestCase
{

	use InteractsWithFilesTrait;

	/**
	 * @test
	 */
	public function generates_model_files()
	{
		$settings = new SettingsManager();

		$settings->setSettings([]);
		$settings->addToType('entities', 'TestOne', ['default' => true]);
		$settings->addToType('entities', 'TestTwo', ['default' => true]);
		$settings->addToType('entities', 'Nested/TestThree', ['default' => true]);
		$settings->addToType('map','web', [
			'something-one' => [
				'children' => [
					'something-two'
				]
			]
		]);

		$foreman = new ForemanManager();

		$foreman->constructApplication();

		$this->assertFileExists(base_path('/app/Models/TestOne.php'));
		$this->assertFileExists(base_path('/app/Models/TestTwo.php'));
		$this->assertFileExists(base_path('/app/Models/Nested/TestThree.php'));
	}

}