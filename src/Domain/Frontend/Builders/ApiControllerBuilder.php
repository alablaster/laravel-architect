<?php


namespace Alablaster\Architect\Domain\Frontend\Builders;


use Alablaster\Architect\Domain\Core\Builders\StubBuilder;
use Alablaster\Architect\Domain\Core\Factories\FileLocationFactory;
use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Domain\FileLocations\Factories\ApiControllerFileLocationFactory;

class ApiControllerBuilder extends StubBuilder
{
	protected $config;
	protected $locationFactory;
	protected $entity;

	public function __construct(Entity $entity, ?array $config)
	{

		$this->entity = $entity;

		$references = [
			Entity::class => $entity
		];

		parent::__construct(null, null, $references);

		$this->locationFactory = new ApiControllerFileLocationFactory($config['path']);

		$this->buildController();
		$this->buildTests();
	}

	private function buildController()
	{
		$this->setLocation($this->locationFactory->controller);
		$this->getStubFrom(__DIR__ . '/../stubs/controller/api-crud.stub');
		$this->build();
	}

	private function buildTests()
	{
		$this->addSlot('namespace', $this->locationFactory->namespace);
		$this->addSlot('name', $this->locationFactory->name);
		$this->addSlot('node', $this->locationFactory->node);
		$this->addSlot('requiredFields', $this->arrayElements($this->entity->fields->getRequiredFields()));
		$this->addSlot('nullableFields', $this->arrayElements($this->entity->fields->getNullableFields()));
		$this->addSlot('stringFields', $this->arrayElements($this->entity->fields->getStringFields()));
		$this->addSlot('integerFields', $this->arrayElements($this->entity->fields->getIntegerFields()));

		$this->addSlot('requiredTests', $this->createTests($this->entity->fields->getRequiredFields(), '\'\''));
		$this->addSlot('stringTests', $this->createTests($this->entity->fields->getStringFields(), 45));
		$this->addSlot('integerTests', $this->createTests($this->entity->fields->getIntegerFields(), '\'Not an integer\''));

		$this->setLocation($this->locationFactory->tests . 'IndexTest.php');
		$this->getStubFrom(__DIR__ . '/../stubs/test/controller/index_test.php.stub');
		$this->build();

		$this->setLocation($this->locationFactory->tests . 'ShowTest.php');
		$this->getStubFrom(__DIR__ . '/../stubs/test/controller/show_test.php.stub');
		$this->build();

		$this->setLocation($this->locationFactory->tests . 'StoreTest.php');
		$this->getStubFrom(__DIR__ . '/../stubs/test/controller/store_test.php.stub');
		$this->build();

		$this->setLocation($this->locationFactory->tests . 'UpdateTest.php');
		$this->getStubFrom(__DIR__ . '/../stubs/test/controller/update_test.php.stub');
		$this->build();

		$this->setLocation($this->locationFactory->tests . 'DeleteTest.php');
		$this->getStubFrom(__DIR__ . '/../stubs/test/controller/delete_test.php.stub');
		$this->build();
	}

	private function createTests(array $array, $value):string
	{
		$return = [];

		foreach($array  as $field)
		{
			$return[] = '$payload[\''. $field .'\'] = ' . $value .';';
		}

		return implode("\n\t\t", $return);
	}

}