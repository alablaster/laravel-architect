Configuration
==============
The architect.php configuration file is published when you public Architect's vendor folder.

In general, these settings can be set for the entire application here, and can be overwritten in the
definitions from the blueprints. For example, make => model is set to true, you may want to set it to false on the
User model so that you don't over write laravel's default model.
auth
---------------------
**default: true**

If set to true, Architect will use an auth middleware on all routes which don't override this setting.

components
-----------
This array of frontend sets which components will be built by default. By default

frontend
-------------------------
**default: blade**

Sets the default frontend framework, though it can be overwritten on a route by route basis. Options are blade, inertia, and livewire. When you
set this, make sure to include the corresponding components to true as well.

make
------
This array defines which files will be created by default. For example, if you aren't using an API
you may wish to turn off resources since those likely won't be needed.

mass_assignable
--------------------------------
**default: true**

Sets if fields are by default included as mass assignable in models. This can also be overwritten on the individual field.

relationships
-------------

on_delete
"""""""""""
**default: restrict**

How to deal with the deletion of a model in a relationship. If set to restrict destroying the primary model which has children will throw an error. If set to cascade deleting the primary model will delete the children.


settings_path
---------------
**default: '/blueprints.yaml')**
This is the default path to load the blueprints yaml file.

path
-----
This is an array the default locations of several of the file types created throughout Architect. in many cases the route also drives namespaceing, in such cases, the namespaces implied by this path will be used for the namespace as well.
