<?php


namespace Alablaster\Architect\Domain\Entities\Factories;


use Alablaster\Architect\Domain\Core\Factories\FileLocationFactory;
use Alablaster\Architect\Domain\Core\Traits\InteractsWithFilesTrait;
use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Domain\Entities\Factories\LivewirePartialFactory;
use Illuminate\Support\Str;

class LivewireCreateFactory extends LivewirePartialFactory
{
	use InteractsWithFilesTrait;

	protected function buildClass(): void
	{
		$this->builder->getStubFrom(__DIR__ . '/../stubs/partials/livewire/create.class.php.stub');
		$this->builder->setLocation(new FileLocationFactory('livewire.partial.class', $this->entity) . 'Create.php');
		$this->builder->addSlot('refreshMap', $this->makeRefreshMap());
		$this->builder->build();
	}

	protected function buildView(): void
	{
		$this->builder->getStubFrom(__DIR__ . '/../stubs/partials/livewire/create.view.blade.php.stub');
		$this->builder->setLocation(new FileLocationFactory('livewire.partial.view', $this->entity) . 'create.blade.php');
		$this->builder->addSlot('fields.inputs', $this->makeFieldInputs());
		$this->builder->build();
	}

	protected function buildTest(): void
	{
		$this->builder->getStubFrom(__DIR__ . '/../stubs/partials/livewire/create.test.php.stub');
		$this->builder->setLocation(new FileLocationFactory('livewire.partial.test', $this->entity) . 'CreateTest.blade.php');

		$this->builder->build();
	}

	protected function makeRefreshMap()
	{
		$entity = $this->builder->getReference(Entity::class);

		$return = '';

		$fields = $entity->fields->getFields()->filter(function($field) { return $field->visible; });

		foreach($fields as $field)
		{
			$return .= "\n\t\t\t " . '$this->' . $field->name . ' = null;';
		}
	}

	protected function makeFieldInputs()
	{
		$inputs = '';

		$blankStub = $this->openFile(__DIR__ . '/../stubs/partials/livewire/inputs/input.livewire.blade.php.stub');

		foreach($this->entity->fields->getFields() as $field)
		{
			if($field->visible) {
				$stub = $blankStub;
				$stub = str_replace(
					'{{ field.label }}',
					$field->label,
					$stub
				);

				$stub = str_replace(
					'{{ field.name }}',
					Str::camel($field->name),
					$stub
				);

				$stub = str_replace(
					'{{ field.type }}',
					$field->inputType,
					$stub
				);

				$inputs .= $stub;
			}

		}

		return $inputs;

	}
}