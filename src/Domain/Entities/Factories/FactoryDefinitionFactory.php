<?php


namespace Alablaster\Architect\Domain\Entities\Factories;


use Alablaster\Architect\Domain\Entities\Field;

class FactoryDefinitionFactory
{
	private $field, $faker;

	public function __construct(Field $field, $faker = null)
	{

		$this->field = $field;

		if ($faker === null) {
			$this->inferFaker();
		} else {
			$this->faker = $faker;
		}

	}

	public function __toString()
	{
		return $this->createDefinition();
	}

	private function createDefinition() : string
	{
		if(!$this->faker) {
			if(is_array($this->field->type) && $this->field->type['name'] == 'set')
			{
				return '\'' . $this->field->type['set'][rand(0, count($this->field->type['set']) - 1 )] . '\'';
			}
			return false;
		}

		return '\'' . $this->field->name . '\' => $this->faker->' . $this->faker;
	}

	private function inferFaker()
	{
		if(is_array($this->field->type))
			$type = $this->field->type['name'];
		else
			$type = $this->field->type;

		switch ($type)
		{
			case 'boolean':
				$this->faker = 'boolean(50)';
				break;

			case 'time':
				$this->faker = 'time()';
				break;

			case 'date':
				$this->faker = 'date()';
				break;

			case 'dateTime':
			case 'timestampTz':
			case 'timestamp':
			case 'dateTimeTz':
				$this->faker = 'dateTime()';
				break;

			case 'timeTz':
				$this->faker = 'timezone';
				break;

			case 'year':
				$this->faker = 'year()';
				break;

			case 'float':
			case 'decimal':
			case 'double':
				$this->faker = 'float(2)';
				break;

			case 'integer':
			case 'mediumInteger':
			case 'smallInteger':
			case 'bigInteger':
			case 'tinyInteger':
			case 'unsignedBigInteger':
			case 'unsignedDecimal':
			case 'unsignedInteger':
			case 'unsignedMediumInteger':
			case 'unsignedSmallInteger':
			case 'unsignedTinyInteger':
				$this->faker = 'integer()';
				break;

			case 'mediumIncrements':
			case 'increments':
			case 'nullableTimestamp';
				$this->faker = false;
				break;

			case 'ipAddress':
				$this->faker = 'ipAddress';
				break;

			case 'json':
			case 'jsonb':
				$this->faker = 'json';
				break;

			case 'lineString':
			case 'longText':
			case 'text':
			case 'mediumTest':
				$this->faker = 'paragraph()';
				break;

			case 'macAddress':
				$this->faker = 'macAddress';
				break;

			case 'multiPoint':
			case 'multiPolygon':
			case 'point':
			case 'polygon':
				$this->faker = 'polygon';
				break;

			case 'uuid';
				$this->faker = 'uuid';
				break;

			case 'string':
				$this->stringFaker();
				break;

			case 'multiLineString':
			default:
				return false;
		}
	}

	private function stringFaker()
	{

		switch ($this->field->use)
		{
			case 'firstName':
				$this->faker = 'firstName()';
				break;

			case 'lastName':
				$this->faker = 'lastName()';
				break;

			case 'state':
				$this->faker = 'state';
				break;

			case 'country':
				$this->faker = 'country';
				break;

			case 'phone':
				$this->faker = 'phoneNumber';
				break;

			case 'company':
				$this->faker = 'company';
				break;

			case 'email':
				$this->faker = 'safeEmail';
				break;

			case 'image':
				$this->faker = 'imageUrl()';
				break;

			default:
				$this->faker = 'sentence()';
				break;
		}

	}

}