<?php

namespace Alablaster\Architect\Tests\Domain\Entities;

use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Domain\Entities\Field;
use Alablaster\Architect\Domain\Entities\FieldCollection;
use Alablaster\Architect\Domain\Settings\Managers\SettingsManager;
use Alablaster\Architect\Tests\TestCase;

class EntityTest extends TestCase
{
	/**
	 * @test
	 */
	public function sets_defaults_correctly()
	{
		$entity = new Entity('Test/TestWord');
		$this->assertSame('Test/TestWord', $entity->key, 'Key is incorrect');
		$this->assertSame('TestWord',$entity->get('name'),  'Name is incorrect');
		$this->assertSame('test_words', $entity->get('table'),  'Table is incorrect');
		$this->assertSame('Test', $entity->get('namespace'),  'Namespace is incorrect');
		$this->assertInstanceOf(FieldCollection::class, $entity->fields, 'Fields is incorrect');
	}

	/**
	 * @test
	 */
	public function sets_non_defaults_correctly()
	{
		$entity = new Entity('SomeOtherThing', [
			'name' => 'something',
			'table' => 'other_things',
			'relationships' => ['first' => ['defaults' => true]],
			'namespace' => 'Something\\',
			'extends' => [
				'import' => 'Something\\Something',
				'extends' => 'Something'
			],
			'implements' => [
				[
					'import' => 'App\\One',
					'use' => 'One'
				],
				[
					'import' => 'App\\Two',
					'use' => 'Two'
				]
			],
			'traits' => [
				[
					'import' => 'App\\One',
					'use' => 'One'
				],
				[
					'import' => 'App\\Two',
					'use' => 'Two'
				]
			],
			'fields' => [
				'something' => [
					'defaults' => true
				]
			]
		]);


		$this->assertSame('SomeOtherThing', $entity->key);
		$this->assertSame('something', $entity->get('name'));
		$this->assertSame('other_things', $entity->get('table'));
		$this->assertSame('Something\\', $entity->get('namespace'));
		$this->assertInstanceOf(FieldCollection::class, $entity->fields);
	}

	/**
	 * @test
	 */

	public function gets_namespace_from_key()
	{
		$manger	= new SettingsManager();
		$manger->setSettings([]);

		$manger->addToType('entities', 'Namespace/Test');

		$entity = new Entity('Namespace/Test');

		$this->assertSame('Namespace', $entity->get('namespace'));
	}

	/**
	 * @test
	 */

	public function generates_full_name_of_model()
	{
		$entity = new Entity('Test');

		$this->assertSame('App\\Models\\Test', $entity->get('fullName'));
	}


	/**
	 * @test
	 */

	public function generates_full_name_of_model_with_namespace()
	{
		$entity = new Entity('Properties/Test');

		$this->assertSame('App\\Models\\Properties\\Test', $entity->get('fullName'));
	}


	/**
	 * @test
	 */

	public function an_entity_has_several_default_fields()
	{
		$entity = new Entity('Properties/Test');

		$fields = $entity->fields;

		$this->assertInstanceOf(Field::class, $fields->getField('id'), 'id field');
		$this->assertInstanceOf(Field::class, $fields->getField('name'), 'name field');
		$this->assertInstanceOf(Field::class, $fields->getField('created_at'), 'created_at');
		$this->assertInstanceOf(Field::class, $fields->getField('updated_at'), 'updated_at');
	}

	/**
	 * @test
	 */

	public function an_entity_with_custom_fields_still_has_default_fields()
	{
		$entity = new Entity('Properties/Test', ['fields' => ['something' => ['default' => true]]]);

		$fields = $entity->fields;

		$this->assertInstanceOf(Field::class, $fields->getField('id'), 'id field');
		$this->assertFalse($fields->getField('name'), 'name field');
		$this->assertInstanceOf(Field::class, $fields->getField('something'), 'something field');
		$this->assertInstanceOf(Field::class, $fields->getField('created_at'), 'created_at');
		$this->assertInstanceOf(Field::class, $fields->getField('updated_at'), 'updated_at');
	}
}
