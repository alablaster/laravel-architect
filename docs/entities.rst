Entities
==========
An entity is similar to a model, it is responsible for most of the information related to data, its format and its storage. The primary exception to this would be relationships which are stored in a different object, so they can be applied on top of the entities.

Keys
-----
Key should be written in the singular, with their full name space separated by a forward slash.

`Family/Parent`

name
^^^^^^^
The last string in the key after the last forward slash.

`Family/Parent` becomes `Parent`

table
^^^^^^
The default table name is a snake cased plural version of the entity name, though this can be overwritten.

`Parent` becomes `parents`

fields
^^^^^^^
An array of the fields to be included in the entity. Unless passed as false, entities by default with have an 'id', 'created_at' and 'updated_at,' though their characteristics can be overwritten.

frontend
^^^^^^^^^
An array of frontend scaffolding options.