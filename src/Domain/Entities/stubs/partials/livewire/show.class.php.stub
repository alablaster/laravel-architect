<?php
// show.class.php.stub

namespace App\Http\Livewire\Partials\@<< entity.namespace >>@\@<< entity.name.studly >>@;

use @<< entity.fullName >>@;
use Livewire\Component;

class Show extends Component
{
    public @<< entity.variables >>@;
    public $confirmDelete = false;
    public $edit = [];
    public $record;

    protected $rules = [@<< entity.updateRules >>@
    ];

    protected $listeners = [
        'update' => 'updateRecord',
        'clean' => 'cleanEdits',
        'cancel' => 'mount'
    ];

    public function mount()
    {
        $this->edit = [];
        $@<< entity.name.camel >>@ = $this->record;
        @<< entity.mountingMap >>@
        $this->emit('clean');
    }

    public function render()
    {
        return view('livewire.partials.@<< entity.namespace.dot.snake.trailingDot >>@@<< entity.name.snake >>@.show');
    }

    public function updateRecord()
    {
        $this->validate();

        if($this->record->update([
            @<< entity.fieldMap >>@
        ]))
        {
            $this->emit('clean');
        }
    }

    public function destroy()
    {
        $this->confirmDelete = false;
        $this->emitUp('deleteRecord');
    }

    public function cleanEdits()
    {
        $this->edit = [];
    }

    public function edit($field)
    {
        if(!in_array($field, $this->edit))
        {
            $this->edit[] = $field;
        }

    }
}
