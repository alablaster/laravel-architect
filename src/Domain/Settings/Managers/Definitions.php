<?php


namespace Alablaster\Architect\Domain\Settings\Managers;


use Alablaster\Architect\Domain\Core\Definition;
use Alablaster\Architect\Domain\Core\DefinitionsInterface;
use Alablaster\Architect\Domain\Core\Exceptions\DefinitionDoesNotConformToStandard;
use Alablaster\Architect\Domain\Core\Traits\LoadsEntityDefinitions;
use Alablaster\Architect\Domain\Core\Traits\LoadsMapDefinitions;
use Alablaster\Architect\Domain\Core\Traits\SettingsTrait;
use Alablaster\Architect\Domain\Entities\Entity;

class Definitions
{
	protected $settings;
	protected $map;
	protected $relationships;
	protected $count = 0;

	use LoadsEntityDefinitions;
	use LoadsMapDefinitions;

	/**
	 * Definitions constructor.
	 * @throws DefinitionDoesNotConformToStandard
	 */
	public function __construct()
	{
		$this->count++;

		$this->settings = new SettingsManager();

		$this->loadEntities($this->settings->getSettings('entities'));


//		if(array_key_exists('web', $settings->getSettings('map')))
//		{
//			$this->loadWeb($settings->getSettings('map')['web']);
//		}
	}

	public function __invoke()
	{
		print 'count ' . $this->count;
	}

	public function getRelationships()
	{
		if($this->settings->getSettings('relationships'))
		{
			return $this->settings->getSettings('relationships');
		}
		else {
			return [];
		}
	}

	public function getMap($domain = 'map')
	{
		return $this->$domain;
	}

	public function getManagers()
	{
		return $this->settings->getSettings('managers');

	}

	public function findByKey($type, $key, $alwaysFind = false){
		if(array_key_exists($key, $this->settings->getSettings($type)))
		{
			return $this->settings->getSettings($type)[$key];
		} else if ($alwaysFind) {
			return ['key' => $key];
		} else{
			throw new DefinitionDoesNotConformToStandard( '"' . $key .'" is not a defined entity key.');
		}
	}

	/**
	 * Parse if a definition has an array or not, then create
	 * a new instance of the Definition.
	 *
	 * @param $definitions
	 * @param $definition
	 * @return array
	 * @throws DefinitionDoesNotConformToStandard
	 */
	public function parseDefinitions($definitions, $definition)
	{
		$array = [];
		foreach($definitions as $key => $entity)
		{
			if(is_integer($key) && is_string($entity)) {
				$array[$entity] = new $definition($entity);
			} elseif (is_array($entity) && is_string($key)) {
				$array[$key] = new $definition($key, $entity);
			} else {
				throw new DefinitionDoesNotConformToStandard('Definition does not conform to standard: ' . serialize(['key' => $key, 'definition' => $entity]));
			}
		}

		return $array;
	}


}