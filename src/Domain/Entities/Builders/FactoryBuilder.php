<?php


namespace Alablaster\Architect\Domain\Entities\Builders;


use Alablaster\Architect\Domain\Core\Builders\Builder;
use Alablaster\Architect\Domain\Core\Builders\StubBuilder;
use Alablaster\Architect\Domain\Core\Factories\FileLocationFactory;
use Alablaster\Architect\Domain\Entities\Entity;

class FactoryBuilder extends StubBuilder
{
	public function __construct(Entity $entity)
	{
		if(!$entity->make['factory']) return;

		parent::__construct(
			$this->openFile(__DIR__ . '/../stubs/factory.stub'),
			new FileLocationFactory('factory', $entity),
			[Entity::class => $entity]
		);

		$this->build();
	}

}