<?php


namespace Alablaster\Architect\Domain\Entities\Factories;


use Alablaster\Architect\Domain\Core\Builders\StubBuilder;
use Alablaster\Architect\Domain\Core\Factories\FileLocationFactory;
use Alablaster\Architect\Domain\Entities\Entity;

abstract class LivewirePartialFactory
{

	protected $builder, $entity;

	public function __construct(Entity $entity)
	{
		$this->builder = new StubBuilder();
		$this->builder->setReference($entity);

		$this->entity = $entity;

		$this->build();
	}

	protected function build()
	{
		$this->buildClass();
		$this->buildView();
		$this->buildTest();
	}

	abstract protected function buildClass(): void;
	abstract protected function buildView(): void;
	abstract protected function buildTest(): void;
}