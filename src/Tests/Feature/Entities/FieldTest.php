<?php


namespace Alablaster\Architect\Tests\Feature\Entities;


use Alablaster\Architect\Domain\Entities\Field;
use Alablaster\Architect\Tests\TestCase;

class FieldTest extends TestCase
{
	/**
	 * @test
	 */

	public function it_generates_a_string_migration_method()
	{
		$field = new Field('Name', []);

		$this->assertStringContainsString('$table->string(\'name\', 255);', $field->generateMigrationMethod());

	}

	/**
	 * @test
	 */

	public function it_generates_a_decimal_migration_method()
	{
		$field = new Field('Amount', ['type' => ['name' => 'decimal', 'total' => 9, 'decimal' => 3]]);

		$this->assertStringContainsString('$table->decimal(\'amount\', 9, 3);', $field->generateMigrationMethod());

	}

	/**
	 * @test
	 */

	public function it_generates_a_float_migration_method()
	{
		$field = new Field('Amount', ['type' => ['name' => 'float', 'total' => 6, 'decimal' => 1]]);

		$this->assertStringContainsString('$table->float(\'amount\', 6, 1);', $field->generateMigrationMethod());

	}

	/**
	 * @test
	 */

	public function it_generates_store_rules()
	{
		$field = new Field('Number', ['type' => ['name' => 'set', 'set' => [1, 2, 3]]]);

		$this->assertStringContainsString("'number' => [" , $field->generateStoreRule());
	}



}