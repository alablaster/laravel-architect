<?php


namespace Alablaster\Architect\Tests\Feature\Entities\Factory;


use Alablaster\Architect\Domain\Entities\Field;
use Alablaster\Architect\Tests\TestCase;

class FactoryDefinitionFactoryTest extends TestCase
{

	/**
	 * @test
	 */
	public function it_returns_sentence_for_a_generic_string()
	{
		$field = new Field('Something');

		$this->assertSame("'something' => \$this->faker->sentence()", $field->factoryDefinition);
	}

	/**
	 * @test
	 */
	public function it_returns_ip_address_faker()
	{
		$field = new Field('Something', ['type' => 'ipAddress']);

		$this->assertSame("'something' => \$this->faker->ipAddress", $field->factoryDefinition);
	}

	/**
	 * @test
	 */
	public function it_returns_a_phone_number_based_on_the_use()
	{
		$field = new Field('Something', ['use' => 'phone']);

		$this->assertSame("'something' => \$this->faker->phoneNumber", $field->factoryDefinition);
	}

	/**
	 * @test
	 */
	public function it_returns_a_email_based_on_the_use()
	{
		$field = new Field('Something', ['use' => 'email']);

		$this->assertSame("'something' => \$this->faker->safeEmail", $field->factoryDefinition);
	}

	/**
	 * @test
	 */
	public function it_returns_a_boolean_faker()
	{
		$field = new Field('Something', ['type' => 'boolean']);

		$this->assertSame("'something' => \$this->faker->boolean(50)", $field->factoryDefinition);
	}

}