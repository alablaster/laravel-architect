[![pipeline status](https://gitlab.com/alablaster/laravel-architect/badges/production/pipeline.svg)](https://gitlab.com/alablaster/laravel-architect/-/commits/production)
[![coverage report](https://gitlab.com/alablaster/laravel-architect/badges/production/coverage.svg)](https://gitlab.com/alablaster/laravel-architect/-/commits/production)

An application scaffolding utility.

## What it is
This package is designed to scaffoled your appliation based on simple YAML file. Most settings
are overwriteable, but everything comes with a default, allowing for rapid prototyping of an
application.

For example, once you've defined an entity with its key, such as `Family/Parent`, Architect
will now know its namespace is Family, its name is Parent, its table name is parents, and
assume it should have the fields, 'id', 'name', 'created_at', and 'updated_at'. When you
build your foundation, Architect will be able to create not just the model, with mass assignable
fields, but also create the migration, store and update requests, and model factory. All from two words.


## Versions and Laravel Support

| Release | Date | Laravel | Tag 
|---|---|---|---|
|  Beta 0.2 | 6-1-2021  | v.8.0 | 0.2 |
|  Beta 0.1 | 20-11-2020  | v.8.0 | 0.1.3 |

## Beta Cautions
This is a Beta release, mainly because it is the very first version of this package, so your
feedback is much appreciated. Please create an issue in on GitLab for bugs that are discovered.

There remain several undocumented features in the latest release. While some do work, they are not fully tested,
and may have gaps that are not yet implemented. 

While this package is still in a beta form it, it actually never will be 'used in production,' rather
it is a utility for helping you build your project faster. So, any errors in the way Architect does build
your application will hopefully come out early on in your development. My recomendation would be to git commit
before you run Architect for the first time, so you can better see what files have been created and if they
fit with your needs. If they do, then you can continue to develop your application without fear of any
beta bugs getting into your system. 

## How to Install

In a fresh installation of Laravel, run the following.

`composer install-dev Alablaster/Architect`;

Next publish the vendor files:

`php artisan vendor:publish --provider="Vendor\Alablaster\ArchitectServiceProvider"`

Next to generate your blueprints files, blueprint.yaml in the 
project's root directory run: 

`php artisan architect:blueprints`

## How to use it

Open the blueprint.yaml file in your route folder. It has been seeded with a few simple examples
of how to design an application. The file is broken down into several top level domains including
`entities` and  `relationships`,

With in each of these you can define an object with a 'key'. Based on that key, several default
values are assumed, and but can then be overwitten by passing the name of that setting, and  a 
new value.

To learn what options you have, please visit the docs at https://laravel-architect.readthedocs.io/en/latest/

Once you have edited the blueprints for your project, you can build it using the following command

`php artisan architect:foundation`

## Change Log

### Current 
- Create API scaffolding from the entity definition
- Add method to entity to define which of the files will be made. A default for the application can be set in the config.
- Added support for json resources
- Allow placeholders to chain filters

### 0.2 (6-1-2021)
- Create Livewire 'Directory' route type
- Added output to track which files have been created and better errors
- Create new StubBuilder which more cleanly creates files
- Add route model binding to site maps
- Refactor the WebSiteBuilder to generate routes based on the updated definition style as
defined in the [documentation](https://gitlab.com/alablaster/laravel-architect/-/wikis/Map).

### 0.1.4 (22-12-2020)
- Fixed a bug with the required request rule
- Improved indentations for request rules
- Introduced web vs. api based routing, though the API portion isn't yet implemented. 

### 0.1.3 (20-12-2020)
- Fixed formatting error in Factories
- Added support for ignoring fields in a factory

### 0.1.2 (18-12-2020)
- Added default fakers for most field types. If the field type is unsupported, it will
not have a faker value at all.
- Fixed a variety of errors in the relationship migration which adds a foreign key
for many to many relationships.
