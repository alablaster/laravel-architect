<?php


namespace Alablaster\Architect\Tests\Feature\Core;


use Alablaster\Architect\Domain\Core\Definition;
use Alablaster\Architect\Domain\Settings\Managers\Definitions;
use Alablaster\Architect\Tests\TestCase;

class DefinitionTest extends TestCase
{

	public function test_that_it_loads_entities_from_settings()
	{
		$this->setSettings([
			'entities' => [
				'Something/New' => [
					'fields' => [
						'firstName',
						'lastName'
					]
				],
				'Something/Else',
			]
		]);

		$definitions = app(Definitions::class);

		$this->assertSame(2, $definitions->entities()->count());
	}

}