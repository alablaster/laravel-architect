<?php


namespace Alablaster\Architect\Domain\SiteMap\Factories\Livewire;


use Alablaster\Architect\Domain\Core\Builders\StubBuilder;
use Alablaster\Architect\Domain\Core\Factories\FileLocationFactory;
use Alablaster\Architect\Domain\Core\Traits\InteractsWithFilesTrait;
use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Domain\SiteMap\Method;
use Alablaster\Architect\Domain\SiteMap\SiteNode;
use Illuminate\Support\Str;
use Symfony\Component\VarDumper\Cloner\Stub;

class LivewireViewFactory
{
	use InteractsWithFilesTrait;

	protected $method, $fields, $manager, $includes = [];

	public function __construct( $method)
	{
		$this->method = $method;

		$this->manager = new StubBuilder();
		$this->manager->setReference($method);
		$this->manager->setReference( $method->node);
		$this->manager->setReference( $method->node->entity);

		$this->buildView();
		$this->buildClass();
		$this->buildTest();
	}

	protected function buildView()
	{
		$this->manager->getStubFrom(__DIR__ . '/../../stubs/components/livewire/' . $this->method->get('action')[1] . '.view.stub');
		$this->manager->setLocation(new FileLocationFactory('livewire.view', $this->method));

		$this->manager->build();
	}

	protected function buildClass()
	{

		$this->manager->getStubFrom(__DIR__ . '/../../stubs/components/livewire/' . $this->method->get('action')[1] . '.class.stub');
		$this->manager->setLocation(new FileLocationFactory('livewire.class', $this->method));

		$this->manager->build();
	}

	protected function buildTest()
	{
		$this->manager->getStubFrom(__DIR__ . '/../../stubs/components/livewire/' . $this->method->get('action')[1] . '.test.stub');
		$this->manager->setLocation(new FileLocationFactory('livewire.test', $this->method));

		$this->manager->build();
	}
}