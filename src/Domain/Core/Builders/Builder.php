<?php


namespace Alablaster\Architect\Domain\Core\Builders;


use Alablaster\Architect\Domain\Core\Traits\GenerateFileLocationsTrait;
use Alablaster\Architect\Domain\Core\Traits\InteractsWithFilesTrait;
use Illuminate\Http\FileHelpers;

abstract class Builder
{
	use InteractsWithFilesTrait;

	abstract public function getFileLocation();

	abstract function getStub();

	abstract function populateStub($stub);

	public function generateFile()
	{
		$stub = $this->getStub();
		return $this->populateStub($stub, $type = null);
	}



}