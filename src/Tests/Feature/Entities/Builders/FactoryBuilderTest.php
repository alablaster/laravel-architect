<?php


namespace Alablaster\Architect\Tests\Feature\Entities\Builders;


use Alablaster\Architect\Domain\Core\Builders\Builder;
use Alablaster\Architect\Domain\Core\Builders\StubBuilder;
use Alablaster\Architect\Domain\Core\Traits\InteractsWithFilesTrait;
use Alablaster\Architect\Domain\Core\Traits\SettingsTrait;
use Alablaster\Architect\Domain\Entities\Builders\FactoryBuilder;
use Alablaster\Architect\Domain\Entities\Builders\MigrationBuilder;
use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Tests\TestCase;

class FactoryBuilderTest extends TestCase
{

	use SettingsTrait;

	/**
	 * @test
	 */
	public function it_is_an_instance_of_StubBuilder()
	{
		$entity = new Entity('Test/Entity');
		$builder = new FactoryBuilder($entity);

		$this->assertInstanceOf(StubBuilder::class, $builder);
	}


	/**
	 * @test
	 */
	public function it_has_the_correct_namespacing()
	{
		$entity = new Entity('Test/Entity');
		$builder = new FactoryBuilder($entity);


		$this->assertStringContainsString('namespace Database\\Factories\\Test;', $builder);
	}

	/**
	 * @test
	 */
	public function it_imports_the_correct_model()
	{
		$entity = new Entity('Test/Entity');
		$builder = new FactoryBuilder($entity);


		$this->assertStringContainsString('use App\\Models\\Test\\Entity;', $builder);
	}

	/**
	 * @test
	 */
	public function it_has_the_correct_model()
	{
		$entity = new Entity('Test/Entity');
		$builder = new FactoryBuilder($entity);


		$this->assertStringContainsString('protected $model = Entity::class;', $builder);
	}

	/**
	 * @test
	 */
	public function it_has_the_correct_definitions()
	{
		$entity = new Entity('Test/Entity');
		$builder = new FactoryBuilder($entity);


		$this->assertStringContainsString('\'name\' => $this->faker->sentence()', $builder);
	}
}
