<?php


namespace Alablaster\Architect\Domain\Core\Factories;

use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Domain\Relationships\Relationship;
use Alablaster\Architect\Domain\SiteMap\Method;
use Alablaster\Architect\Domain\SiteMap\SiteNode;
use Illuminate\Support\Str;

class FileLocationFactory
{
	private $type;
	private $model;


	public function __construct($type, $model)
	{
		$this->type = $type;
		$this->model = $model;
	}

	public function __toString()
	{
		switch ($this->type)
		{
			case 'model':
				return $this->generateModalLocation($this->model);
				break;

			case 'migration':
				return $this->generateMigrationLocation($this->model);
				break;
			
			case 'migration.relationship':
				return $this->generateRelationshipMigrationLocation($this->model);
				break;

			case 'request.store':
				return $this->generateRequestLocation($this->model, 'Store');
				break;

			case 'request.update':
				return $this->generateRequestLocation($this->model, 'Update');
				break;

			case 'controller':
				return $this->generateControllerLocation($this->model);
				break;

			case 'controller.api':
				return $this->generateApiControllerLocation($this->model);
				break;

			case 'routes':
				return $this->generateRoutesLocation();
				break;

			case 'factory':
				return $this->generateFactoryRoute($this->model);
				break;

			case 'test.feature.store':
				return $this->generateTestLocation();

			case 'livewire.view':
				return $this->generateLivewireViewLocation($this->model);

			case 'livewire.class':
				return $this->generateLivewireClassLocation($this->model);

			case 'livewire.test':
				return $this->generateLivewireTestLocation($this->model);

			case 'livewire.partial.view':
				return $this->generateLivewireViewPartialLocation($this->model);

			case 'livewire.partial.class':
				return $this->generateLivewireClassPartialLocation($this->model);

			case 'livewire.partial.test':
				return $this->generateLivewireTestPartialLocation($this->model);

			case 'resource':
				return $this->generateResourcesLocation($this->model);

			case 'manager':
				return $this->generateManagerLocation($this->model);

			default:
				throw new \Exception("No such file type as $this->type");
		}
	}

	private function generateModalLocation(Entity $entity)
	{
		$path = '';

		$directories = explode('\\', $entity->get('namespace'));

		$directories = array_filter($directories);

		if(count($directories)) {
			$path .= '/' . implode('/', $directories);
		}

		$path .= '/' .  $entity->get('name') . '.php';

		return config('architect.path.model') . $path;
	}

	private function generateMigrationLocation(Entity $entity)
	{
		$path = config('architect.path.migration');
		$path .= '/' . date('Y_m_d_u');
		$path .= '_create_' . $entity->get('table') . '_table.php';

		return $path;
	}
	
	private function generateRelationshipMigrationLocation(Relationship $relationship)
	{
		$path = config('architect.path.migration');
		$path .= '/' . date('Y_m_d') . '_000100';
		$path .= '_create_' . Str::singular($relationship->primaryEntity->table) . '_' . Str::singular($relationship->childEntity->table) . '_relationship.php';
		
		return $path;
	}

	private function generateRequestLocation(Entity $entity, $type)
	{
		$path = config('architect.path.requests');

		$directories = explode('\\', $entity->get('namespace'));
		$directories[] = $entity->get('name');

		$directories = array_filter($directories);
		$path .= '/' . implode('/', $directories);

		return $path . '/' . $type . 'Request.php';
	}

	private function generateControllerLocation(String $path)
	{
		$directories = explode('/', $path);
		$directories = array_filter($directories);
		foreach($directories as $key => $step){
			$directories[$key] = Str::studly(str_replace('-', ' ', $step));
		}
		return config('architect.path.controller') . '/' . (count($directories) == 1 ? last($directories) : implode('/', $directories)) . '/' . Str::studly(last($directories)) .'Controller.php';
	}

	private function generateApiControllerLocation(String $path)
	{
		$directories = explode('/', $path);
		$directories = array_filter($directories);
		foreach($directories as $key => $step){
			$directories[$key] = Str::studly(str_replace('-', ' ', $step));
		}
		return config('architect.path.controller') . '/Api/' . Str::studly(config('architect.api_version')) . '/' . (count($directories) == 1 ? last($directories) : implode('/', $directories)) . '/' . Str::studly(last($directories)) .'Controller.php';
	}

	private function generateRoutesLocation()
	{
		return config('architect.path.routes') . '/' . $this->model .'.php';
	}

	public function generateFactoryRoute(Entity $entity)
	{
		$namespace = explode('\\', $entity->get('namespace'));

		foreach($namespace as $key => $value)
		{

			if($value == null)
			{
				unset($namespace[$key]);
			}
		}

		return config('architect.path.factories') . '/' . implode($namespace) . '/' . $entity->get('name') . 'Factory.php';
	}

	private function generateTestLocation()
	{
		$key = explode('.', $this->type);

		$namepace = explode('\\', $this->model->get('namespace'));

		return config('architect.path.tests.' . $key[1]) . '/' . implode('/', $namepace) . '/' . $this->model->get('name') . '/' . Str::studly($key[2] . $this->model->get('name') . 'Test.php');
	}

	private function generateLivewireViewLocation(Method $method)
	{
 		return config('architect.path.livewire.view') . $method->node->key . '/' . Str::kebab( $method->get('type')) . '.blade.php';
	}

	private function generateLivewireTestLocation(Method $method)
	{

		$namespace = explode('\\', $method->get('action')[0]);

		foreach($namespace as $key => $item)
		{
			if($key == 0 && $item == 'Livewire')
			{
				unset($namespace[$key]);
			} else {
				$namespace[$key] = Str::studly($item);
			}
		}

		return config('architect.path.livewire.test') . '/' . Str::singular(implode('/', $namespace)) . '/' . Str::studly( $method->get('type') . 'Test.php');
	}

	private function generateLivewireClassLocation(Method $method)
	{
		$namespace = explode('\\', $method->get('action')[0]);

		foreach($namespace as $key => $item)
		{
			if($key == 0 && $item == 'Livewire')
			{
				unset($namespace[$key]);
			} else {
				$namespace[$key] = Str::studly($item);
			}
		}

		return config('architect.path.livewire.class') . '/' . Str::singular(implode('/', $namespace)) . '/' . Str::studly( $method->get('type') . '.php');
	}

	private function generateLivewireViewPartialLocation(Entity $entity)
	{

		$namespace = explode('\\', $entity->get('namespace'));
		$namespace[] = $entity->get('name');

		foreach($namespace as $key => $item)
		{
			if($key == 0 && $item == 'Livewire')
			{
				unset($namespace[$key]);
			} else {
				$namespace[$key] = Str::kebab($item);
			}
		}

		return config('architect.path.livewire.view') . '/partials/' . Str::singular(implode('/', $namespace)) . '/';
	}

	private function generateLivewireTestPartialLocation(Entity $entity)
	{

		$namespace = explode('\\', $entity->get('namespace'));
		$namespace[] = $entity->get('name');

		foreach($namespace as $key => $item)
		{
			if($key == 0 && $item == 'Livewire')
			{
				unset($namespace[$key]);
			} else {
				$namespace[$key] = Str::studly($item);
			}
		}

		return config('architect.path.livewire.test') . '/partials/' . Str::singular(implode('/', $namespace)) . '/';
	}

	private function generateLivewireClassPartialLocation(Entity $entity)
	{
		$namespace = explode('\\', $entity->get('namespace'));
		$namespace[] = $entity->get('name');

		foreach($namespace as $key => $item)
		{
			if($key == 0 && $item == 'Livewire')
			{
				unset($namespace[$key]);
			} else {
				$namespace[$key] = Str::studly($item);
			}
		}

		return config('architect.path.livewire.class') . '/Partials/' . Str::singular(implode('/', $namespace)) . '/';
	}

	public function generateResourcesLocation(Entity $entity)
	{
		return config('architect.path.resources') . '/' . $entity->get('namespace') . '/' . Str::studly($entity->get('name'));
	}

	public function generateManagerLocation(Entity $entity)
	{
		$path = explode('\\', $entity->get('namespace'));
		$path[] = $entity->get('name') . 'Manager.php';
		return config('architect.path.manager') . '/' . implode('/', $path);
	}

}
