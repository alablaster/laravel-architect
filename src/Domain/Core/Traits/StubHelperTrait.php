<?php


namespace Alablaster\Architect\Domain\Core\Traits;


trait StubHelperTrait
{

	/**
	 * Takes an array prints each element as a string on a new line,
	 * unless $indent is set to null
	 *
	 * @param $array
	 * @param int $indent
	 * @return string
	 */
	public function arrayElements(array $array, $indent = 3): string
	{
		foreach($array as $key => $item)
		{
			$array[$key] = '\'' . $item . '\'';
		}

		$glue = ', ';

		if($indent !== null)
		{
			$glue .= "\n";

			while($indent > 0) {
				$glue .= "\t";
				$indent -=1;
			}
		}

		return implode($glue, $array);
	}

}