<?php


namespace Alablaster\Architect\Tests\Feature\SiteMap;


use Alablaster\Architect\Domain\Core\Traits\SettingsTrait;
use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Domain\SiteMap\SiteNode;

class SiteNoteTest extends \Alablaster\Architect\Tests\TestCase
{
	use SettingsTrait;

	/**
	 * @test
	 */

	public function it_loads_root_on_key()
	{
		$node = new SiteNode('/');

		$this->assertSame('/', $node->get('path'));
	}

	/**
	 * @test
	 */

	public function it_can_load_an_entity()
	{
		$this->addToSettingType('entities', 'Test');

		$node = new SiteNode('/', ['entity' => 'Test']);

		$this->assertInstanceOf(Entity::class, $node->entity);
	}


	/**
	 * @test
	 */

	public function it_can_infer_an_entity_based_on_the_path()
	{
		$this->setSettings(['entities' => ['Something/Test' => ['default' => true]]]);

		$node = new SiteNode('/something/tests');

		$this->assertSame('Something/Test', $node->entity->key);
	}


//	/**
//	 * @test
//	 */
//	public function it_can_load_a_parent_entities_based_on_route_model_bindings()
//	{
//		$this->setSettings(['entities' => ['Something' => ['default' => true], 'Something/Test' => ['default' => true]]]);
//
//		$node = new SiteNode('/somethings/{Something}/tests');
//
//		$this->assertSame('/somethings/{something}/tests', $node->path);
//		$this->arrayHasKey('something', $node->routeBindings);
//		$this->isInstanceOf(Entity::class, $node->routeBindings['something']);
//
//	}
}