<?php // controller/store_test.php.stub

namespace Tests\E2E\@<< namespace >>@\@<< node.studly >>@;

use @<< entity.fullName >>@;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Http\Response;
use Tests\TestCase;

class @<< node.studly >>@ControllerStoreTest extends TestCase
{
    use DatabaseMigrations;

    public function test_it_loads_the_route()
    {
        $user = factory(User::class)->create();
        $payload = factory(@<< entity.name.studly >>@::class)->make()->toArray();
        $payload['api_token'] = $user->api_token;

        $response = $this->postJson(route('@<< name >>@.store'), $payload);


        $response->assertStatus(Response::HTTP_CREATED);
    }


    public function test_it_stores_a_valid_@<< entity.name.snake >>@()
    {
        $user = factory(User::class)->create();
        $payload = factory(@<< entity.name.studly >>@::class)->make()->toArray();
        $this->assertDatabaseMissing('@<< entity.table >>@', $payload);

        $payload['api_token'] = $user->api_token;

        $response = $this->postJson(route('@<< name >>@.store'), $payload);


        unset($payload['api_token']);

        $response->assertStatus(Response::HTTP_CREATED);
        $this->assertDatabaseHas('@<< entity.table >>@', $payload);
    }


    public function test_it_has_required_fields_to_store_a_@<< entity.name.snake >>@()
    {
        $user = factory(User::class)->create();
        $payload = [];

        $payload['api_token'] = $user->api_token;

        $response = $this->postJson(route('@<< name >>@.store'), $payload);


        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJsonValidationErrors([
            @<< requiredFields >>@
        ]);
    }

    public function test_string_fields_must_be_strings_to_store_a_@<< entity.name.snake >>@()
    {
        $user = factory(User::class)->create();
        $payload = factory(@<< entity.name.studly >>@::class)->make()->toArray();
        $payload['api_token'] = $user->api_token;

        @<< stringTests >>@

        $response = $this->postJson(route('@<< name >>@.store'), $payload);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJsonValidationErrors([
            @<< stringFields >>@
        ]);

        unset($payload['api_token']);

        $this->assertDatabaseMissing('@<< entity.table >>@' , $payload);
    }

    public function test_integer_fields_must_be_integers_to_store_a_@<< entity.name.snake >>@()
    {
        $user = factory(User::class)->create();
        $payload = factory(@<< entity.name.studly >>@::class)->make()->toArray();
        $payload['api_token'] = $user->api_token;

        @<< integerTests >>@

        $response = $this->postJson(route('@<< name >>@.store'), $payload);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJsonValidationErrors([
            @<< integerFields >>@
        ]);

        unset($payload['api_token']);

        $this->assertDatabaseMissing('@<< entity.table >>@' , $payload);
    }

}
