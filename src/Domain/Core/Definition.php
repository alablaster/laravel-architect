<?php


namespace Alablaster\Architect\Domain\Core;



use Alablaster\Architect\Domain\Core\Exceptions\DefinitionDoesNotConformToStandard;
use Alablaster\Architect\Domain\Core\Interfaces\StubBuilderReference;
use Illuminate\Support\Arr;

abstract class Definition implements StubBuilderReference
{
	protected array $properties, $definition;

	/**
	 * This key should be unique among site nodes and
	 * should be how the site node will be called.
	 *
	 * @var string
	 */
	public string $key;

	public function __construct(string $key, array $definition)
	{
		$this->key = $key;
		$this->definition = $definition;
	}

	/**
	 * Retrieve a property based on its key. If property is unset
	 * it will return false.
	 *
	 * @param string $property
	 * @return string|boolean
	 */
	public function get(string $property)
	{
		return Arr::get($this->properties, $property, false);
	}

	/**
	 * Retrieve a property based on its key. If the property is empty
	 * a default value can be passed as well
	 *
	 * @param string $property
	 * @param string $default
	 * @return string
	 */
	public function getElse(string $property, string $default): string
	{
		return $this->get($property) ? $this->get($property) :  $default;
	}

	/**
	 * Set property with default, unless a custom value is provided.
	 * In practice the default might be set by calling a function
	 * which creates the default from a key, where as the custom
	 * would be anything passed by the user as an override to
	 * the default.
	 *
	 * @param $key
	 * @param string $default
	 * @param string $custom
	 * @return void
	 */
	public function set($key, $default = '', string $custom = ''): void
	{
		$this->properties[$key] = $custom ? $custom : $default;
	}

	/**
	 * Set a property from an array's index if valid, otherwise set default.
	 *
	 * @param string $key
	 * @param array $array
	 * @param string $index
	 * @param string $default
	 * @return void
	 */
	public function setFrom(string $key, array $array, string $index, $default = ''): void
	{
		if(is_string(Arr::get($array, $index)) && Arr::get($array, $index) != null)
		{
			$this->set($key, Arr::get($array, $index, $default));
		} else {
			$this->set($key, $default);
		}
	}

}