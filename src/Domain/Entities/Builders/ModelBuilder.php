<?php


namespace Alablaster\Architect\Domain\Entities\Builders;


use Alablaster\Architect\Domain\Core\Builders\StubBuilder;
use Alablaster\Architect\Domain\Core\Factories\FileLocationFactory;
use Alablaster\Architect\Domain\Entities\Entity;

class ModelBuilder extends StubBuilder
{
	public function __construct(Entity $entity)
	{
		if(!$entity->make['model']) return;

		parent::__construct(
			$this->openFile(__DIR__ . '/../stubs/model.stub'),
			new FileLocationFactory('model', $entity),
			[Entity::class => $entity]
		);

		$this->build();
	}

}
