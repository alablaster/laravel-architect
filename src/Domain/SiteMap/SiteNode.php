<?php


namespace Alablaster\Architect\Domain\SiteMap;


use Alablaster\Architect\Domain\Core\Definition;
use Alablaster\Architect\Domain\Core\Exceptions\DefinitionDoesNotConformToStandard;
use Alablaster\Architect\Domain\Core\Interfaces\StubBuilderReference;
use Alablaster\Architect\Domain\Core\Traits\InteractsWithFilesTrait;
use Alablaster\Architect\Domain\Core\Traits\SettingsTrait;
use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Domain\Entities\Traits\LoadsEntities;
use Alablaster\Architect\Domain\SiteMap\Builders\ControllerBuilder;
use Alablaster\Architect\Domain\SiteMap\Builders\ControllerMethodBuilder;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use function PHPUnit\Framework\arrayHasKey;
use function PHPUnit\Framework\stringContains;

class SiteNode extends Definition implements StubBuilderReference
{

	use LoadsEntities;
	use InteractsWithFilesTrait;

	use SettingsTrait;

	/**
	 * Child classes
	 *
	 * @var MethodCollection
	 */
	public MethodCollection $methods;

	/**
	 * Optional linked entity
	 *
	 * @var Null|Entity
	 */
	public $entity;

	/**
	 * Routes referenced in the path
	 *
	 * @var null|array
	 */
	public $routeBindings;

	public function __construct(string $key, array $definition = [])
	{
		parent::__construct($key, $definition);

		// Set custom properties.
		$this->set('path', $key);
		$this->setFrom('todo', $definition, 'todo', '');
		$this->setFrom('name', $definition, 'name', $this->makeName());
		$this->setFrom('frontend', $definition, 'frontend', $this->makeFrontend());
		$this->setFrom('type', $definition, 'type', $this->guessType());
		$this->setFrom('auth', $definition, 'auth', config('architect.auth'));

		$this->loadRouteBindings();

		$this->loadEntity();

		$this->methods = $this->loadMethods();
	}

	public function setKey($key)
	{
		$this->key = $key;
	}

	public function get($property)
	{
		switch ($property)
		{
			case 'route':
				return $this->getRoute();

			case 'routes':
				return $this->getRoutes();

			case 'namespace':
				return $this->getNamespace();

			case 'controllerMethods':
				return $this->getControllerMethods();

			default:
				return parent::get($property);
		}

	}

	public function getControllerMethods()
	{

		$methods = '';
		foreach($this->methods->getMethods() as $key => $method)
		{
			$methods .= "\n" . new ControllerMethodBuilder($method, $key);
		}

		return $methods;
	}

	private function getRoute()
	{
		$steps = explode('/', $this->key);

		return implode('.', array_filter($steps));
	}

	private function guessType()
	{
		if($this->entity)
		{
			return 'crud';
		}

		return 'default';
	}

	private function loadEntity()
	{
		if(isset($this->definition['entity'])) {
			$this->entity = $this->getEntityByKey($this->definition['entity']);
		}

		$this->guessEntity();
	}

	private function loadMethods()
	{
		switch ($this->get('type'))
		{
			case 'crud':
				switch ($this->get('frontend')) {
					case 'blade':
						$types = ['index', 'create', 'store', 'show', 'edit', 'update', 'destroy'];
						break;

					case 'livewire':
						$types = ['index', 'create', 'show', 'edit'];
						break;

					case 'inertia':
						$types = ['index', 'create', 'show'];
						break;

					default:
						$types = ['index', 'store', 'show', 'update', 'destroy'];
						break;
				}
				break;

			case 'directory':
				switch ($this->get('frontend')) {
					case 'inertia':
					case 'livewire':
						$types = ['directory'];
						break;
					default:
						throw new DefinitionDoesNotConformToStandard('No "Directory" type exists for ' . $this->frontend);
						break;
				}
				break;

			default:
				$types = ['invoke'];
		}

		$types = isset($this->definition['methods']) ? $this->definition['methods'] : $types;

		$routes = [];
		foreach($types as $type)
		{
			$routes[$type] = new Method($this, $type);
		}

		return new MethodCollection($routes);
	}

	private function makeName()
	{
		$name = explode('/', $this->get('path'));

		return last($name);
	}

	public function getRoutes()
	{
		return $this->methods->getRoutesActions();
	}

	private function guessEntity()
	{
		$path = explode('/', Str::singular($this->get('path')));

		foreach($path as $key => $value)
		{
			if($value == null) {
				unset($path[$key]);
			}
			$path[$key] = Str::studly($value);
		}
		$path = array_filter($path);

		if($this->entityKeyValid(implode('/', $path)))
		{
			$this->entity = $this->getEntityByKey(implode('/', $path));
			return;
		}

		if($this->routeBindings && count($this->routeBindings))
		{
			$this->entity = last($this->routeBindings);
		}
	}

	private function makeFrontend()
	{
		if(isset($this->definition['blade']) && $this->definition['blade'])
			return 'blade';

		if(isset($this->definition['livewire']) && $this->definition['livewire'])
			return 'livewire';

		if(isset($this->definition['inertia']) && $this->definition['inertia'])
			return 'inertia';

		else return config('architect.frontend');
	}

	private function loadRouteBindings()
	{
		$path = $this->get('path');
		if(strpos($path, '{' ) && strpos($path, '}')) {
			$start = strpos($path, '{' );
			$end = strpos($path, '}' );
			$key = substr($path, $start + 1, $end - $start - 1);
			$entity = $this->getEntityByKey($key);
			$name = Str::kebab($entity->get('name'));
			$this->path = str_replace(
				'/{' . $key . '}',
				'',
				$this->get('path')
			);

			$this->set('path', $this->get('path'));

			$this->routeBindings[$name] = $entity;
		}
	}

	private function getNamespace()
	{
		$namespace = explode('/', $this->key);
		foreach($namespace as $key => $value)
		{
			if($value == null) {
				unset($namespace[$key]);
			} else {
				$namespace[$key] = Str::studly(Str::singular($value));
			}
		}

		return implode('\\', $namespace);
	}


}