<?php

namespace Alablaster\Architect\Tests\Domain\Entities\Factories;

use Alablaster\Architect\Domain\Core\Traits\InteractsWithFilesTrait;
use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Domain\Entities\Factories\LivewireShowFactory;
use Alablaster\Architect\Tests\TestCase;
use Alablaster\Architect\Tests\TestLivewireComponents;

class LivewireShowFactoryTest extends TestCase
{
	use InteractsWithFilesTrait;
	use TestLivewireComponents;

	public $class, $view, $test;

	public function setUp(): void
	{
		parent::setUp();

		$entity = new Entity('Parent/Child');

		$this->deleteFile(base_path('app/Http/Livewire/Partials/Parent/Child/Show.php'));
		$this->deleteFile(base_path('resources/views/livewire/partials/parent/child/show.blade.php'));
		$this->deleteFile(base_path('tests/Livewire/partials/Parent/Child/ShowTest.php'));

		new LivewireShowFactory($entity);

		$this->class = $this->openFile(base_path('app/Http/Livewire/Partials/Parent/Child/Show.php'));
		$this->view = $this->openFile(base_path('resources/views/livewire/partials/parent/child/show.blade.php'));
		$this->test = $this->openFile(base_path('tests/Livewire/partials/Parent/Child/ShowTest.php'));
	}

	public function test_all_files_have_been_generated()
	{
		$this->classHas('show.class.php.stub');
		$this->viewHas('show.view.blade.php.stub');
		$this->tstHas('show.test.php.stub');
	}

	public function test_class_slots_have_been_replaced()
	{
		$this->classMissing('@<< entity.namespace >>@');
		$this->classMissing('@<< entity.fullName >>@');
		$this->classMissing('@<< entity.name.camel >>@');
		$this->classMissing('@<< entity.fields.updateRules >>@');
		$this->classMissing('@<< entity.name.studly >>@');
		$this->classMissing('@<< entity.dotNamespace >>@');
		$this->classMissing('@<< entity.fieldMap >>@');

		$this->classHas('namespace App\\Http\\Livewire\\Partials\\Parent\\Child');
		$this->classHas("'name' => [");
		$this->classHas('\'name\' => $this->name');
		$this->classHas('return view(\'livewire.partials.parent.child.show\');');
	}

	public function test_view_slots_have_been_replaced()
	{
		$this->viewMissing('@<< fields.showInputs >>@');


		$this->viewHas('@if(!in_array(\'name\', $edit)');
		$this->viewHas('wire:click="edit(\'name\')');
		$this->viewHas('wire:model="name"');
	}
}
