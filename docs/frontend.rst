Frontend
============
Fields represent the fields that will be created in a database, but also the fields included in model factories, requests, and tests.

y set a faker use the definitions provided in [FakerPHP](https://fakerphp.github.io/formatters/). If not set, Architect will infer based on the type or use of the field. Most field types supported by Laravel generate a faker, some of the notable exceptions however are geometry fields or other fields which don't have an obvious faker.