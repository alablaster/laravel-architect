<?php


namespace Alablaster\Architect\Domain\SiteMap\Builders;


use Alablaster\Architect\Domain\Core\Exceptions\DefinitionDoesNotConformToStandard;
use Alablaster\Architect\Domain\Core\Traits\InteractsWithFilesTrait;
use Alablaster\Architect\Domain\SiteMap\Factories\ControllerMethods\ControllerCreateMethodFactory;
use Alablaster\Architect\Domain\SiteMap\Factories\ControllerMethods\ControllerDestroyMethodFactory;
use Alablaster\Architect\Domain\SiteMap\Factories\ControllerMethods\ControllerEditMethodFactory;
use Alablaster\Architect\Domain\SiteMap\Factories\ControllerMethods\ControllerIndexMethodFactory;
use Alablaster\Architect\Domain\SiteMap\Factories\ControllerMethods\ControllerInvokeMethodFactory;
use Alablaster\Architect\Domain\SiteMap\Factories\ControllerMethods\ControllerStoreMethodFactory;
use Alablaster\Architect\Domain\SiteMap\Factories\ControllerMethods\ControllerUpdateMethodFactory;
use Alablaster\Architect\Domain\SiteMap\Factories\ControllerMethods\ControllerShowMethodFactory;

use Alablaster\Architect\Domain\SiteMap\Method;
use Illuminate\Support\Str;

class ControllerMethodBuilder
{
	use InteractsWithFilesTrait;

	private $method;
	private $type;

	private $validTypes = [
		'default',
		'index',
		'create',
		'store',
		'show',
		'edit',
		'update',
		'destroy'
	];

	public function __construct(Method $method, $type)
	{
		$this->method = $method;
		$this->type = $type;
	}

	public function __toString()
	{
		return $this->getMethod();
	}

	public function getMethod() : string
	{
		switch ($this->type)
		{
			case 'invoke':
				return new ControllerInvokeMethodFactory($this->method);
				break;

			case 'index':
				return new ControllerIndexMethodFactory($this->method);
				break;

			case 'store':
				return new ControllerStoreMethodFactory($this->method);
				break;

			case 'update':
				return new ControllerUpdateMethodFactory($this->method);
				break;

			case 'create':
				return new ControllerCreateMethodFactory($this->method);
				break;

			case 'show':
				return new ControllerShowMethodFactory($this->method);
				break;

			case 'edit':
				return new ControllerEditMethodFactory($this->method);
				break;

			case 'destroy':
				return new ControllerDestroyMethodFactory($this->method);
				break;

			default:
				return '';
		}
	}



}