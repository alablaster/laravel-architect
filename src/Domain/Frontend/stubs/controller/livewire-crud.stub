<?php

namespace App\Http\Controllers;

use App\Http\Requests\@<< entity.namespace.trailingBackSlash >>@@<< entity.name.studly>>@\StoreRequest;
use App\Http\Requests\@<< entity.namespace.trailingBackSlash >>@@<< entity.name.studly>>@\UpdateRequest;
use App\Http\Resources\@<< entity.namespace.trailingBackSlash >>@@<< entity.name.studly>>@\@<< entity.name.studly>>@Collection;
use App\Http\Resources\@<< entity.namespace.trailingBackSlash >>@@<< entity.name.studly>>@\@<< entity.name.studly>>@Resource;
use App\Managers\@<< entity.namespace.trailingBackSlash >>@@<< entity.name.studly>>@Manager;
use Illuminate\Support\Facades\Redirect;
use @<< entity.fullName >>@;
use Inertia\Inertia;

class @<< entity.name.studly>>@Controller extends Controller
{
    protected $manager;

    public function __construct()
    {
        $this->manager = new @<< entity.name.studly>>@Manager();
    }

    /**
     * Display a listing of the @<< entity.name.title>>@.
     *
     * @return \Inertia\Response
     */
    public function index(Request $request)
    {
        $@<< entity.name.camel.plural>>@ = @<< entity.name.studly>>@::all();

        return Inertia::render('@<< entity.fullName.component >>@/@<< inertia.name.studly >>@Index')
            ->with('@<< entity.name.camel.plural>>@', @<< entity.name.camel.plural>>@);
    }

   /**
     * Display form to create the specified @<< entity.name.title>>@.
     *
     * @param @<< entity.name.studly>>@ $@<< entity.name.camel>>@
     * @return \Inertia\Response
     */
    public function create()
    {
        return Inertia::render('@<< entity.fullName.component >>@/Create@<< inertia.name.studly >>@');

    }

    /**
     * Store a newly created @<< entity.name.title>>@ in storage.
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $this->manager
            ->create($request->validated());

        return Redirect::route('@<< entity.inertiaPath.dot.trailingDot >>@show', $this->manager->get());
    }

    /**
     * Display the specified @<< entity.name.title>>@.
     *
     * @param @<< entity.name.studly>>@ $@<< entity.name.camel>>@
     */
    public function show(@<< entity.name.studly>>@ $@<< entity.name.camel>>@)
    {
        return Inertia::render('entity.fullName.component >>@/Show@<< inertia.name.studly >>@')
            ->with('@<< entity.camel >>@', $currier->toResource())
    }

    /**
     * Display form to edit the specified @<< entity.name.title>>@.
     *
     * @param @<< entity.name.studly>>@ $@<< entity.name.camel>>@
     */
    public function edit(@<< entity.name.studly>>@ $@<< entity.name.camel>>@)
    {
        return Inertia::render('entity.fullName.component >>@/Edit@<< inertia.name.studly >>@')
                    ->with('@<< entity.camel >>@', $currier->toResource())
    }

    /**
     * Update the specified @<< entity.name.title>>@ in storage.
     *
     * @param UpdateRequest $request
     * @param @<< entity.name.studly>>@ $@<< entity.name.camel>>@
     */
    public function update(UpdateRequest $request, @<< entity.name.studly>>@ $@<< entity.name.camel>>@)
    {
        $this->manager->load@<< entity.name.studly>>@($@<< entity.name.camel>>@);

        $this->manager
            ->update($request->validated());

        Session::flash('success', '@<< entity.name.title >>@ successfully updated.'
        return Redirect::route('@<< entity.bladePath.dot.trailingDot >>@show', $this->manager->get());
    }

    /**
     * Remove the specified @<< entity.name.title>>@ from storage.
     *
     * @param @<< entity.name.studly>>@ $@<< entity.name.camel>>@
     */
    public function destroy(@<< entity.name.studly>>@ $@<< entity.name.camel>>@)
    {
        $this->manager
            ->load@<< entity.name.studly>>@($@<< entity.name.camel>>@);

        $this->manager
            ->delete();

        Session::flash('info', '@<< entity.name.title >>@ successfully deleted.'

        return Redirect::route('@<< entity.bladePath.dot.trailingDot >>@index');
    }
}
