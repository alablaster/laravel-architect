<?php


namespace Alablaster\Architect\Domain\SiteMap\Traits;


trait FrontendDependencies
{
	protected function checkForFrontendDependencies()
	{
		$files = [
			[
				'from' => '/../stubs/components/confirmation-modal.blade.php.stub',
				'to' => 'resources/views/components/confirmation-modal.blade.php'
			],
			[
				'from' => '/../stubs/components/dialog-modal.blade.php.stub',
				'to' => 'resources/views/components/dialog-modal.blade.php'
			],
			[
				'from' => '/../stubs/components/input-error.blade.php.stub',
				'to' => 'resources/views/components/input-error.blade.php'
			],
			[
				'from' => '/../stubs/components/label.blade.php.stub',
				'to' => 'resources/views/components/label.blade.php'
			],
			[
				'from' => '/../stubs/components/modal.blade.php.stub',
				'to' => 'resources/views/components/modal.blade.php'
			],
			[
				'from' => '/../stubs/components/panel.blade.php.stub',
				'to' => 'resources/views/components/panel.blade.php'
			],
			[
				'from' => '/../stubs/components/button.blade.php.stub',
				'to' => 'resources/views/components/button.blade.php'
			],
			[
				'from' => '/../stubs/components/primary-button.blade.php.stub',
				'to' => 'resources/views/components/primary-button.blade.php'
			],
			[
				'from' => '/../stubs/components/secondary-button.blade.php.stub',
				'to' => 'resources/views/components/secondary-button.blade.php'
			],
			[
				'from' => '/../stubs/components/danger-button.blade.php.stub',
				'to' => 'resources/views/components/danger-button.blade.php'
			],
			[
				'from' => '/../stubs/components/directory-index-item.blade.php.stub',
				'to' => 'resources/views/components/directory-index-item.blade.php'
			],
			[
				'from' => '/../stubs/components/search.blade.php.stub',
				'to' => 'resources/views/components/search.blade.php'
			],
		];

		echo "\nBlade Components Added.";


		if($this->nodes->filter(function($node) {return $node->frontend === 'livewire';}))
		{
			$files = array_merge([
				[
					'from' => '/../stubs/components/livewire/base-directory.blade.php.stub',
					'to' => 'resources/views/livewire/base/base-directory.blade.php'
				],
				[
					'from' => '/../stubs/components/livewire/editable-input.blade.php.stub',
					'to' => 'resources/views/components/livewire/editable-input.blade.php'
				],
				[
					'from' => '/../stubs/components/livewire/BaseDirectory.php.stub',
					'to' => 'app/Http/Livewire/Base/BaseDirectory.php'
				],
				[
					'from' => '/../stubs/components/livewire/Input.php.stub',
					'to' => 'app/View/Components/Livewire/Input.php'
				],
				[
					'from' => '/../stubs/components/livewire/checkbox.blade.php.stub',
					'to' => 'resources/views/components/livewire/checkbox.blade.php'
				],
				[
					'from' => '/../stubs/components/livewire/input.blade.php.stub',
					'to' => 'resources/views/components/livewire/input.blade.php'
				],
				[
					'from' => '/../stubs/components/livewire/textarea.blade.php.stub',
					'to' => 'resources/views/components/livewire/textarea.blade.php'
				],
				[
					'from' => '/../stubs/components/livewire/save-tab.blade.php.stub',
					'to' => 'resources/views/livewire/base/save-tab.blade.php'
				],
				[
					'from' => '/../stubs/components/livewire/SaveTab.php.stub',
					'to' => 'app/Http/Livewire/Base/SaveTab.php'
				],
			], $files);

			echo "\nLivewire Dependencies Added.";
		}

		foreach($files as $file) {
			$stub = $this->openFile(__DIR__ . $file['from']);
			$this->saveFile(base_path($file['to']), $stub);
		}
	}
}