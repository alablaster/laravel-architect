<?php


namespace Alablaster\Architect\Domain\Relationships;


use Alablaster\Architect\Domain\Core\Exceptions\DefinitionDoesNotConformToStandard;
use Alablaster\Architect\Domain\Core\Traits\InteractsWithFilesTrait;
use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Domain\Core\Traits\SettingsTrait;
use Alablaster\Architect\Domain\Entities\Traits\LoadsEntities;
use Alablaster\Architect\Domain\Core\Exceptions\NoDefinitionExistsForKey;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Str;

use function PHPUnit\Framework\isInstanceOf;
use function PHPUnit\Framework\stringContains;
use function PHPUnit\Framework\throwException;

class Relationship
{
	use InteractsWithFilesTrait;
	use SettingsTrait;
	use LoadsEntities;

	public $key;
	public $type;
	public $primaryEntity;
	public $childEntity;
	public $childEntities;
	public $includes = [];
	public $definition;
	public $onDelete;

	static $TYPES = [
		'oneToOne',
		'oneToMany',
		'manyToMany',
	];

	public function __construct(string $key, $primaryEntity = null, $childEntities = null)
	{
		$this->key = $key;
		$this->loadDefinition();
		$this->loadType();
		$this->onDelete = isset($this->definition['on_delete']) ? $this->definition['on_delete'] : config('architect.relationships.on_delete');
		$this->loadEntities($primaryEntity, $childEntities);
	}
	
	public function getParentImports()
	{
		if(isset($this->childEntities)) {
			$return = [];
			foreach($this->childEntities as $child) {
				$return[] = $child->getFullName();
			}
			return $return;
		} else {
			return [$this->childEntity->getFullName()];
		}
	}

	public function getChildImport()
	{
		return $this->primaryEntity->getFullName();
	}

	public function getParentRelationship()
	{
		if(isset($this->childEntities)) {
			$return = '';
			foreach($this->childEntities as $child) {
				$return .= $this->createPrimaryMethod($child->key);
			}
			return $return;
		} else {
			return $this->createPrimaryMethod($this->childEntity->key);
		}
	}

	public function getChildRelationship()
	{
		return $this->createChildMethod();
	}

	private function createPrimaryMethod($key)
	{
		$stub = $this->openFile(__DIR__ . '/stubs/relationship.stub');

		$stub = str_replace(
			'{{ name }}',
			$this->makeLocalMethodName($key), $stub
		);

		$stub = str_replace(
			'{{ doc_block }}',
			Str::ucfirst($this->relationshipType('local')), $stub
		);

		$stub = str_replace(
			'{{ relationship }}',
			$this->relationshipType('local'), $stub
		);

		$stub = str_replace(
			'{{ relationship_arguments }}',
			$this->relationshipArguments($key), $stub
		);

		return $stub;
	}

	private function createChildMethod()
	{
		$stub = $this->openFile(__DIR__ . '/stubs/relationship.stub');

		$stub = str_replace(
			'{{ name }}',
			$this->makeForeignMethodName(), $stub
		);

		$stub = str_replace(
			'{{ doc_block }}',
			Str::ucfirst($this->relationshipType('foreign')), $stub
		);

		$stub = str_replace(
			'{{ relationship }}',
			$this->relationshipType('foreign'), $stub
		);

		$stub = str_replace(
			'{{ relationship_arguments }}',
			$this->relationshipArguments(), $stub
		);

		return $stub;
	}

	private function makeLocalMethodName($key = null)
	{
		switch ($this->type) {
			case 'oneToMany':
			case 'manyToMany':
				return Str::camel(Str::plural($this->getEntityByKey($key)->get('name')));
				break;

			default:
				return $this->getEntityByKey($key)->get('name');
				break;
		}
	}

	private function makeForeignMethodName()
	{
		switch ($this->type) {
			case 'ManyToOne':
			case 'manyToMany':
				return Str::camel(Str::plural(Str::lower($this->primaryEntity->name)));
				break;

			default:
				return Str::camel($this->primaryEntity->get('name'));
				break;
		}
	}

	private function relationshipType($side)
	{
		switch ($this->type) {
			case 'oneToOne':
				return $side == 'local' ? 'hasOne' : 'belongsTo';
			case 'oneToMany':
				return $side == 'local' ? 'hasMany' : 'belongsTo';
			case 'manyToOne':
				return $side == 'local' ? 'belongsTo' : 'hasMany';
			case 'manyToMany':
				return 'belongsToMany';
		}
	}

	private function relationshipArguments($key = null)
	{
		if($key) {
			return $this->getEntityByKey($key)->get('name') . '::class';
		} else {
			return $this->primaryEntity->get('name') . '::class';
		}
	}

	private function loadEntities($primaryEntity, $childEntities)
	{
		if ($primaryEntity == null) {
			$this->primaryEntity = $this->getEntityByKey($this->getEntityKey('primary'));
		} else {
			$this->primaryEntity = $primaryEntity;
		}

		if ($childEntities == null) {
			if(array_key_exists('children', $this->definition)) {
				$this->childEntities = new Collection();
				foreach($this->definition['children'] as $child) {
					$this->childEntities->push($this->getEntityByKey($child));
				}
			} else if(is_array($this->getEntityKeys()['children']) ) {
				$this->childEntities = new Collection();
				dd($this->getEntityKey('children'));
				foreach($this->getEntityKey('children') as $child) {
					$this->childEntities->push($this->getEntityByKey($child));
				}
			} else {
				$this->childEntity = $this->getEntityByKey($this->getEntityKey('child'));
			}
		}
	}

	private function getEntityKey($key)
	{
		$keys = $this->getEntityKeys();
		$entityTypes = [
			'primary',
			'child',
			'children',
		];

		if(in_array($key, $entityTypes)) {
			return $keys[$key];
		} else {
			return null;
		}
	}

	private function getEntityKeys()
	{
		$key = [];
		$primary = null;
		$child = null;
		$children = null;
		
		if(strpos($this->key, '>'))
		{
			$keys = explode('>', $this->key);
		} elseif(strpos($this->key, '<')) {
			$keys = explode('<', $this->key);
		} elseif(strpos($this->key, '=')){
			$keys = explode('=', $this->key);
		}
		
		if (count($keys) == 0 || count($keys) > 2) {
			throw new DefinitionDoesNotConformToStandard(
				'Relationship key has ' . count($keys) . ' elements. It should have either 1 or 2.'
			);
		}
		
		if (count($keys) == 1 && array_key_exists('children', $this->definition)) {
			return [
				'primary' => $keys[0],
				'children' => $this->definition['children'],
				'child' => null
			];
		}
		if(strpos($this->key, '><')) {
			$key = explode('><', $this->key);
			$primary = $key[1];
			$child = $key[0];
			$this->type = 'manyToMany';
		} elseif(strpos($this->key, '>')) {
			$key = explode('>', $this->key);
			$primary = $key[1];
			$child = $key[0];
			$this->type = 'oneToMany';
		} elseif(strpos($this->key, '<')) {
			$key = explode('<', $this->key);
			$primary = $key[0];
			$child = $key[1];
			$this->type = 'oneToMany';
		} elseif(strpos($this->key, '=')){
			$key = explode('=', $this->key);
			$primary = $key[0];
			$child = $key[1];
			$this->type = 'oneToOne';
		}
		if(strpos($child, ':')) {
			$this->type = 'morphsMany';
			$children = explode(':', $child);
			$child = null;
		}

		return [
			'primary' => $primary,
			'child' => $child,
			'children' => $children
		];
	}

	private function loadDefinition()
	{
		if (!array_key_exists($this->key, $this->getSettings()['relationships'])) {
			throw new NoDefinitionExistsForKey('relationships', $this->key);
		} else {
			$this->definition = $this->getSettings()['relationships'][$this->key];
		}
	}

	private function loadType()
	{

		if($this->definition == null) {
			throw new DefinitionDoesNotConformToStandard('No definition found for ' . $this->key . ' relationship.');
		}

		if (array_key_exists('type', $this->definition)) {
			$type = $this->definition['type'];
		} else {
			$this->type = 'oneToMany';
			return;
		}

		if (in_array($type, Relationship::$TYPES)) {
			$this->type = $type;
		} else {
			throw new \Exception('"' . $type . '" is not a valid relationship type.');
		}
	}
	


}
