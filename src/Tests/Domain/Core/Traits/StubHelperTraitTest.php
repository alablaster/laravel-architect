<?php

namespace Alablaster\Architect\Tests\Domain\Core\Traits;

use Alablaster\Architect\Domain\Core\Traits\StubHelperTrait;
use Alablaster\Architect\Tests\TestCase;

class StubHelperTraitTest extends TestCase
{
	use StubHelperTrait;

	public function test_it_includes_the_elements_as_string()
	{
		$test = ['one', 'two'];

		$result = $this->arrayElements($test);


		$this->assertIsString($result);
		$this->assertStringContainsString("'one',", $result);
		$this->assertStringContainsString("'two'", $result);
	}

	public function test_it_adds_line_breaks_by_default()
	{
		$test = ['one', 'two'];

		$result = $this->arrayElements($test);


		$this->assertStringContainsString("\n", $result);
	}

	public function test_it_adds_two_indents_by_default()
	{
		$test = ['one', 'two'];

		$result = $this->arrayElements($test);


		$this->assertStringContainsString("\t\t", $result);
	}

	public function test_it_does_note_have_line_breaks_if_indent_is_false()
	{
		$test = ['one', 'two'];

		$result = $this->arrayElements($test, null);


		$this->assertStringNotContainsString("\n", $result);
	}

	public function test_it_adds_five_indents_if_5_is_passed()
	{
		$test = ['one', 'two'];

		$result = $this->arrayElements($test, 5);


		$this->assertStringContainsString("\t\t\t\t\t", $result);
	}
}
