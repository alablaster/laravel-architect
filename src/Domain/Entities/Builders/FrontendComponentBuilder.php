<?php


namespace Alablaster\Architect\Domain\Entities\Builders;


use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Domain\Entities\Factories\LivewireCreateFactory;
use Alablaster\Architect\Domain\Entities\Factories\LivewireShowFactory;

class FrontendComponentBuilder
{
	private $entity;

	public function __construct(Entity $entity)
	{
		$this->entity = $entity;

		if(!$entity->frontends)
		{
			return;
		}

		if($entity->frontends['livewire'])
		{
			$this->makeLivewireComponents();
		}
	}

	private function makeLivewireComponents()
	{
		new LivewireCreateFactory($this->entity);
		new LivewireShowFactory($this->entity);
	}
}