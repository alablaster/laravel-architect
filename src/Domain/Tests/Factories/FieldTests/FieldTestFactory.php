<?php


namespace Alablaster\Architect\Domain\Tests\Factories\FieldTests;


use Alablaster\Architect\Domain\Core\Traits\InteractsWithFilesTrait;
use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Domain\Entities\Field;

abstract class FieldTestFactory
{
	protected $field;
	protected $entity;
	protected $title;
	protected $test;

	use InteractsWithFilesTrait;

	public function __construct(Field $field, Entity $entity)
	{
		$this->entity = $entity;
		$this->field = $field;
	}

	public function __toString()
	{
		$this->configureTest();
		$this->configureTitle();

		return $this->make();
	}

	abstract protected function configureTitle() : void;
	abstract protected function configureTest() : void;

	protected function make(): string
	{
		return $this->generateTest();
	}

	protected function generateTest() : string
	{
		$stub = $this->openFile(__DIR__ . '/../../stubs/test.stub');

		$stub = str_replace(
			'{{ test }}',
			$this->test,
			$stub);

		$stub = str_replace(
			'{{ title }}',
			$this->title,
			$stub);

		return $stub;
	}

	protected function addToTest($test) : void
	{
		if(is_array($test)){
			$this->test .= implode("\n\t\t", $test);
		} elseif (is_string($test)) {
			$this->test .= "\n\t\t" . $test;
		}
	}
}