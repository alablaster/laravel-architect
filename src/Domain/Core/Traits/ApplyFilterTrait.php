<?php


namespace Alablaster\Architect\Domain\Core\Traits;


use Illuminate\Support\Str;

trait ApplyFilterTrait
{
	public function applyFilter($filter, $string)
	{
		switch($filter) {
			case 'trailingBackSlash':
				return $string ? $string . '\\' : null;
			case 'leadingBackSlash':
				return $string ? '\\' . $string : null;
			case 'leadingForwardSlash':
				return $string ? '/' . $string : null;
			case 'trailingForwardSlash':
				return $string ? $string . '/' : null;
			case 'trailingDot':
				return $string ? $string . '.' : null;
			case 'dot':
				return $string ? $this->dot($string) : null;
			case 'component':
				return $string ? $this->component($string) : null;
			default:
				return Str::$filter($string);
		}
	}

	private function dot($string)
	{
		$words = $this->breakApartWords($string);

		return implode('.', $words);
	}

	private function component($string)
	{
		$words = $this->breakApartWords($string);

		foreach($words as $key => $value)
		{
			$words[$key] = Str::studly($value);
		}
		return implode('/', $words);
	}

	private function breakApartWords($string)
	{
		return preg_split('/\\\\|\//', $string);
	}
}