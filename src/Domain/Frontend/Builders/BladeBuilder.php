<?php


namespace Alablaster\Architect\Domain\Frontend\Builders;


use Alablaster\Architect\Domain\Core\Builders\StubBuilder;
use Alablaster\Architect\Domain\Core\Factories\FileLocationFactory;
use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Domain\SiteMap\Method;
use Alablaster\Architect\Domain\SiteMap\SiteNode;

class BladeBuilder extends StubBuilder
{
	public $name;

	public function __construct(Method $method)
	{

		$location = $this->makeLocation($method);

		$slots = [
			'todo' => '// TODO: ' . $method->node->getElse('todo', 'Implement blade template.')
		];

		$resources = [
			Method::class => $method,
			SiteNode::class => $method->node,
			Entity::class => $method->node->entity
		];

		parent::__construct(
			$this->loadStub($method->get('type')),
			$location,
			$resources,
			$slots
		);

		$this->build();
	}

	protected function loadStub($type): string
	{
		switch ($type)
		{
			default:
				return $this->openFile(__DIR__ . '/../stubs/blade/default.stub');
		}
	}

	protected function makeLocation($method): string
	{
		$path = explode('/', $method->node->get('path'));

		if($method->get('type') != 'invoke') {
			$path[] = $method->get('type');
		}

		$this->name = implode('.', $path);

		return config('architect.path.views') . '/' . implode('/', $path) . '.blade.php';
	}
}