<?php


namespace Alablaster\Architect\Tests\Feature\Settings\Traits;


use Alablaster\Architect\Domain\Core\Traits\LoadsMapDefinitions;

class LoadsMapDefinitionsTest
{
	use LoadsMapDefinitions;

	public function test_it_loads_map_routes()
	{
		$web = [ 'something-one' => [
					'children' => [
						'something-two'
					]
				]
			];

		$this->loadWeb($web);

	}
}