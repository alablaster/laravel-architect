<?php


namespace Alablaster\Architect\Domain\Core\Builders;


use Alablaster\Architect\Domain\Core\Interfaces\StubBuilderReference;
use Alablaster\Architect\Domain\Core\Traits\InteractsWithFilesTrait;
use Alablaster\Architect\Domain\Core\Traits\ApplyFilterTrait;
use Alablaster\Architect\Domain\Core\Traits\StubHelperTrait;
use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Domain\Entities\Field;
use Alablaster\Architect\Domain\SiteMap\Method;
use Alablaster\Architect\Domain\SiteMap\SiteNode;

class StubBuilder
{
	use InteractsWithFilesTrait;
	use ApplyFilterTrait;
	use StubHelperTrait;

	protected $stub, $location;
	protected array $references, $slots;

	const CLASSES = [
		'entity' => Entity::class,
		'node' => SiteNode::class,
		'method' => Method::class,
		'field' => Field::class
	];

	public function __construct(String $stub = null, String $location = null, array $references = [], array $slots = [])
	{
		$this->stub = $stub;
		$this->location = $location ? base_path($location) : null;
		$this->references = $references;
		$this->slots = $slots;
		$this->addSlot('includes', '');
	}

	/**
	 * If the manager is cast as a string it should provide a hydrated
	 * version of the stub, but without saving it.
	 *
	 * @return string
	 */
	public function __toString(): string
	{
		$this->hydrateStub();

		return $this->stub;
	}

	/**
	 * Hydrate the stub with the current parameters and save to the chosen
	 * location. If successful, it will return true, else it will return false.
	 *
	 * @return boolean
	 */
	public function build(): bool
	{
		try {
			if($this->stub == null || $this->location == null)
			{
				throw new \Exception('Both a stub and location are required to use the StubBuilder');
			}

			$this->hydrateStub();
			$this->saveFile($this->location, $this->stub);

			echo "\n Created" . $this->location ;

			return true;
		}
		catch (\Exception $exception) {
			echo "\n Error generating " . $this->location;
 			echo "\n -> " . $exception->getMessage();
			return false;
		}

	}

	/**
	 * When using the manger, the user must provide the various references needed
	 * to hydrate the stub.
	 *
	 * @param StubBuilderReference $reference
	 * @return void
	 */
	public function setReference(StubBuilderReference $reference)
	{
		$this->references[get_class($reference)] = $reference;

		$this->slots = [];
	}

	/**
	 * When using the manger, the user must provide the various references needed
	 * to hydrate the stub.
	 *
	 * @param string $type
	 * @return boolean | object
	 */
	public function getReference(string $type)
	{
		if(!array_key_exists($type, $this->references)){
			return false;
		}

		return $this->references[$type];

	}

	/**
	 * To set a non-default slot, or overwrite an existing slot, pass the slot name used in the stub
	 * as well as the content which should replace it.
	 *
	 * @param string $slot
	 * @param string $content
	 * @return void
	 */
	public function addSlot($slot, $content): void
	{
		$this->slots[$slot] = $content;
	}

	/**
	 * Appends to the content already in a slot. If no slot exists, it will create a new one
	 * with the passed content.
	 *
	 * @param $slot
	 * @param $content
	 * @return bool
	 */
	public function addToSlot(string $slot, string $content):void
	{
		if(isset($this->slots[$slot])) {
			$this->slots[$slot] .= $content;
		} else {
			$this->slots[$slot] = $content;
		}
	}

	/**
	 * Appends to the content already in a solot. If no slot exists, it will create a new one
	 * with the passed content.
	 *
	 * @param $slot
	 * @param $content
	 * @return bool
	 */
	public function addInclude(string $include):void
	{
		$this->addToSlot('includes', "\nuse $include;");
	}

	/**
	 * Set the location where the finished file should be saved. The path should be relative to the
	 * projects base folder and include the file name and extension.
	 *
	 * @param string $location
	 * @return void
	 */
	public function setLocation($location): void
	{
		$this->location = base_path($location);
	}

	/**
	 * Takes the full location of a stub and loads the contents.
	 *
	 * @param string $location
	 * @return void
	 */
	public function getStubFrom($location) : void
	{
		$this->stub = $this->openFile($location);
	}

	/**
	 * Set the stub which needs to be hydrated. Should include slots encapsulated with {{ double curly brackets }}
	 *
	 * @param string $stub
	 */
	public function setStub($stub): void
	{
		$this->stub = $stub;
	}

	/**
	 * An internal function to hydrate the stub based on the slot array.
	 *
	 * @return void
	 */
	private function hydrateStub(): void
	{
		$stub = $this->stub;

		// hydrate dynamically defined slots
		while(strpos($stub,'@<<'))
		{
			$start = strpos($stub,'@<<') + 3;
			$length = strpos($stub,'>>@') - $start;
			$key = substr($stub, $start, $length);

			$replacement = $this->fillPlaceholder($key);

			$stub = str_replace(
				'@<<' . $key . '>>@',
				$replacement,
				$stub
			);
		}

		$this->stub = $stub;
	}

	/**
	 * Takes a stub placeholder and brakes it into both a key and an a default value,
	 * then determines ehe replacement value.
	 *
	 * @param String $key
	 * @return string|null
	 */
	protected function fillPlaceholder(String $key): ?string
	{
		// Check for if there is a default value, to return if no value is provided.
		// If there isn't a default, set the default to null.
		$keys = explode('||', $key);
		$default = isset($keys[1]) ? $keys[1] : null;

		return $this->makeReplacement($keys[0], $default);
	}

	/**
	 * Take the key from the stub and find either a custom slot, or a reference class element,
	 * then apply any additional filters to the replacement string
	 *
	 *
	 * @param $key
	 * @param string $replacement
	 * @return string|null
	 */
	protected function makeReplacement($key, $replacement = ''): ?string
	{

		try {
			$keys = explode('.', trim($key));

			// get the primary key and remove from array
			$primaryKey = array_shift($keys);

			// Check for matching custom designed slot
			if(key_exists($primaryKey, $this->slots))
			{
				$replacement = $this->slots[$primaryKey];

			// else use a reference and choose the property.
			} else {

				// load the reference class
				$reference = $this->getReference(self::CLASSES[$primaryKey]);

				// get the property from the class
				$replacement = $reference->get(array_shift($keys));
			}

			// apply any optional filters to the property
			while(count($keys)) {
				$replacement = $this->applyFilter(array_shift($keys), $replacement);
			}
		} catch (\Exception $exception) {
			print "\n\033[33m Error Building " . $this->location;
			print "\n -> " . $exception->getMessage() . "\033[0m";
		} finally {
			return $replacement;
		}
	}
}