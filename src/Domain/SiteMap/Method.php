<?php


namespace Alablaster\Architect\Domain\SiteMap;


use Alablaster\Architect\Domain\Core\Definition;
use Alablaster\Architect\Domain\Core\Interfaces\StubBuilderReference;
use Alablaster\Architect\Domain\SiteMap\Builders\ControllerMethodBuilder;
use Alablaster\Architect\Domain\SiteMap\Factories\ControllerMethods\ControllerIndexMethodFactory;
use Illuminate\Support\Str;

class Method extends Definition
{
	public $node;

	/**
	 * Method constructor.
	 * @param SiteNode $node
	 * @param string $type
	 */
	public function __construct(SiteNode $node, $type = 'invoke')
	{
		$this->node = $node;
		$this->set('type', $type);
	}

	public function get(string $property)
	{
		switch ($property) {
			case 'method':
				return $this->getMethod();
			case 'namespace':
				return $this->node->get('namespace');
			case 'controllerMethod':
				return $this->getControllerMethod();
			case 'path':
				return $this->generatePath($this->node->get('path'));
			case 'action':
				return $this->generateAction();
			case 'title':
				return $this->generateTitle();
			case 'route':
				return $this->generateRoute();
			case 'todo':
				return $this->node->get('todo');
		}

		return parent::get($property);
	}

	public function getControllerMethod()
	{
		return new ControllerMethodBuilder($this, $this->get('type'));
	}

	private function generatePath($basePath)
	{
		switch ($this->get('type')) {
			case 'create';
				return $basePath . '/create';
				break;

			case 'edit':
				return $basePath . '/{' . Str::kebab($this->node->entity->get('name')) . '}/edit';
				break;

			case 'update':
			case 'show':
			case 'destroy':
				return $basePath . '/{' . Str::kebab($this->node->entity->get('name')) . '}';
				break;

			case 'index':
			default:
				return $basePath;
				break;
		}
	}

	private function generateAction()
	{
		$path = explode('/', $this->node->get('path'));
		foreach($path as $key => $step) {
			$path[$key] = Str::ucfirst(Str::camel($step));
		}
		if($path[0] == null) {
			unset($path[0]);
		}
		$class = count($path) === 1 ? last($path) : implode('\\', $path);
		if($this->node->get('frontend') == 'livewire' || $this->get('type') == 'directory')
		{
			return ['Livewire\\' . Str::singular($class), $this->get('type')];
		} else {
			return [$class . 'Controller', $this->get('type') == 'invoke' ? null : $this->get('type')];
		}
	}

	private function getMethod()
	{
		switch ($this->get('type')) {

			case 'store':
				return 'POST';
				break;

			case 'update':
				return 'PUT';
				break;

			case 'destroy':
				return 'DELETE';
				break;

			default:
				return 'GET';
				break;
		}
	}

	private function generateTitle()
	{
		if($this->get('method') == 'GET' && $this->get('path') != "/") {
			return Str::title(str_replace('-', ' ', last(explode('/', $this->get('path'))))) . ' Index';
		} else {
			return '';
		}
	}

	public function generateRoute()
	{
		$names = explode('/', $this->get('path'));

		foreach($names as $key => $value)
		{
			$names[$key] = Str::singular($value);

			if($value == null || strpos($value, '{') !== null && strpos($value, '}')){
				unset($names[$key]);
			}
		}

		if(!in_array($this->get('type'), ['create', 'edit', 'invoke']))
		{
			$names[] = $this->get('type');
		}

		return implode('.', $names);
	}

}