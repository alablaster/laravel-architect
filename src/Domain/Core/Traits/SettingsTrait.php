<?php


namespace Alablaster\Architect\Domain\Core\Traits;


use Alablaster\Architect\Domain\Settings\Managers\SettingsManager;

trait SettingsTrait
{
	private $LOCATION = 'architect';


    public function getSettings()
    {
	    $manager = app(SettingsManager::class);
	    return $manager->getSettings();
    }

    public function setSettings($settings)
    {
	    $manager = new SettingsManager();
        $manager->setSettings($settings);
    }

    public function addToSettingType($type, $key, $data = [])
    {
	    $manager = new SettingsManager();
	    $manager->addToType($type, $key, $data);
    }


}