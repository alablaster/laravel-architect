<?php

namespace Alablaster\Architect\Console\Commands;

use Alablaster\Architect\Domain\Core\Managers\ForemanManager;
use Illuminate\Console\Command;

class Foundation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'architect:foundation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Builds the foundation of the application based on the existing yaml file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

	/**
	 * Execute the console command.
	 *
	 * @param ForemanManager $manager
	 * @return int
	 */
    public function handle(ForemanManager $manager)
    {
    	$manager->constructApplication();

        return 0;
    }
}
