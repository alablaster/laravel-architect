<?php

namespace Alablaster\Architect\Tests\Domain\FileLocations\Factories;

use Alablaster\Architect\Domain\FileLocations\Factories\ApiControllerFileLocationFactory;
use Alablaster\Architect\Tests\TestCase;

class ApiControllerFileLocationFactoryTest extends TestCase
{
	public function test_that_it_has_the_correct_controller_path()
	{
		config(['architect.path.controller' => 'app/Http/Controllers']);
		config(['architect.api_version' => 'v1']);

		$factory = new ApiControllerFileLocationFactory('/something/test');

		$this->assertSame('app/Http/Controllers/Api/V1/Something/TestController.php', $factory->controller);

	}


	public function test_that_it_has_the_correct_tests_path()
	{
		config(['architect.path.tests.e2e' => 'tests/e2e']);
		config(['architect.api_version' => 'v1']);

		$factory = new ApiControllerFileLocationFactory('/something/test');

		$this->assertSame('tests/e2e/Api/V1/Something/Test/TestController', $factory->tests);

	}
}
