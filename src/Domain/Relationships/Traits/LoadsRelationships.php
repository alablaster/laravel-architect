<?php


namespace Alablaster\Architect\Domain\Relationships\Traits;


use Alablaster\Architect\Domain\Relationships\Relationship;
use Alablaster\Architect\Domain\Settings\Managers\Definitions;

trait LoadsRelationships
{

	public function relationships()
	{

		$definitions = new Definitions();

		$relationships = [];

		foreach($definitions->getRelationships() as $key => $relationship)
		{
			$relationships[] = new Relationship($key);
		}

		return collect($relationships);
	}

	public function getRelationshipByKey($key)
	{
		return $this->relationships()->where('key', $key)->first();
	}

	public function getParentRelationships($key)
	{

		return $this->relationships()->filter(function($value) use ($key){
			return ($value->primaryEntity->key == $key);
		});

	}

	public function getChildRelationships($key)
	{
		return $this->relationships()->filter(function($value) use ($key){
			if($value->childEntity->key === $key) {return true;}
			if($value->childEntities != null && $value->childEntities->where('key', $key)->count() > 0) {return true;}
		});
	}



}