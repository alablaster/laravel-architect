<?php


namespace Alablaster\Architect\Tests\Feature\Entities\Traits;


use Alablaster\Architect\Domain\Core\Blueprints;
use Alablaster\Architect\Domain\Entities\Field;
use Alablaster\Architect\Domain\Entities\Traits\GenerateFieldRulesTrait;
use Alablaster\Architect\Domain\Entities\Traits\LoadsEntities;
use Alablaster\Architect\Domain\Settings\Managers\SettingsManager;
use Alablaster\Architect\Tests\TestCase;
use Alablaster\Architect\Domain\Core\Traits\SettingsTrait;

class GenerateFieldRulesTraitTest extends TestCase
{
	use SettingsTrait;

	use GenerateFieldRulesTrait;

	/**
	 * @test
	 */

	public function by_default_field_is_required()
	{
		$field = new Field('Something', ['type' => ['name' => 'set', 'set' => [1, 2, 3]]]);

		$this->assertStringContainsString('required', $field->generateStoreRule());
	}

	/**
	 * @test
	 */
	public function field_can_be_set_to_nullable()
	{
		$field = new Field('Nullable', ['nullable' => true]);

		$this->assertStringNotContainsString('required', $field->generateStoreRule());
	}

	/**
	 * @test
	 */
	public function by_default_must_be_a_string()
	{
		$field = new Field('Nullable', ['default' => true]);

		$this->assertStringContainsString('string', $field->generateStoreRule());
		$this->assertStringContainsString('max:255', $field->generateStoreRule());

	}

	/**
	 * @test
	 */
	public function rule_can_be_an_integer()
	{
		$field = new Field('Nullable', ['type' => 'integer']);

		$this->assertStringContainsString('integer', $field->generateStoreRule());
	}


	/**
	 * @test
	 */
	public function must_be_unique_by_entity_key()
	{
		$this->setSettings([]);
		$this->addToSettingType('entities', 'Property', ['default' => true]);
		$field = new Field('Nullable', ['unique' => ['key' => 'Property']]);

		$this->assertStringContainsString('unique:App\\Models\\Property,id', $field->generateStoreRule());
	}

}
