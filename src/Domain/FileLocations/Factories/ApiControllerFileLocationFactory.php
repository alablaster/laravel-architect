<?php


namespace Alablaster\Architect\Domain\FileLocations\Factories;


use Illuminate\Support\Str;

class ApiControllerFileLocationFactory extends FileLocationFactory
{
	protected $path;
	public $controller;
	public $tests;
	public $namespace;
	public $name;

	public function __construct(string $path)
	{

		parent::__construct('', []);

		$this->namespace = $this->makeNamespace($path);
		$this->name = $this->makeName($path);

		$this->set('namespace', $this->namespace);
		$this->set('name', $this->name);

		$this->setController($path);
		$this->setTest($path);
	}

	public function makeNamespace(string $path): string
	{
		$prefix[] = 'Api';
		$prefix[] = Str::studly(config('architect.api_version'));

		return implode('\\', $prefix) . '\\' . parent::makeNamespace($path);
	}

	protected function setController(string $path): void
	{
		$this->controller = config('architect.path.controller');
		$this->controller .= '/' . $this->makeStudly($this->namespace);
		$this->controller .= '/' . Str::studly($this->node) . 'Controller.php';
	}

	protected function setTest($path): void
	{
		$this->tests = config('architect.path.tests.e2e');
		$this->tests .= '/' . $this->makeStudly($this->namespace);
		$this->tests .= '/' . Str::studly($this->node) . '/' . Str::studly($this->node) . 'Controller';
	}

}