<?php


namespace Alablaster\Architect\Domain\Entities\Traits;


use Alablaster\Architect\Domain\Entities\Factories\RequestRuleFactory;
use Illuminate\Support\Str;

trait GenerateFieldRulesTrait
{
	public function generateStoreRule()
	{
		$factory = new RequestRuleFactory($this);
		return $this->makeStringRule($factory->store());
	}

	public function generateUpdateRule()
	{
		$factory = new RequestRuleFactory($this);
		return $this->makeStringRule($factory->update());
	}

	private function makeStringRule($rules)
	{
		$return = '';

		if ($this->inRequest) {
			$return .= "\n\t\t\t'" . Str::camel($this->name) . "' => [ ";
			foreach ($rules as $rule) {
				$return .= "\n\t\t\t\t'" . $rule . "',";
			}
			$return .= "\n\t\t\t],";

		}

		return $return;
	}
}
