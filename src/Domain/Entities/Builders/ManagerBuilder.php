<?php


namespace Alablaster\Architect\Domain\Entities\Builders;


use Alablaster\Architect\Domain\Core\Builders\StubBuilder;
use Alablaster\Architect\Domain\Core\Factories\FileLocationFactory;
use Alablaster\Architect\Domain\Entities\Entity;

class ManagerBuilder extends StubBuilder
{
	private $entity;

	public function __construct(Entity $entity)
	{
		$this->entity = $entity;

		$location = $this->getLocation();

		$stub = $this->openFile(__DIR__ . '/../stubs/manager.stub');

		$references = [
			Entity::class => $entity
		];

		parent::__construct($stub, $location, $references);

		$this->build();
	}

	private function getLocation()
	{
		return new FileLocationFactory('manager', $this->entity);
	}
}