<?php

namespace Alablaster\Architect\Domain\Core\Traits;


use Alablaster\Architect\Domain\Core\Exceptions\DefinitionDoesNotConformToStandard;
use Alablaster\Architect\Domain\Entities\Entity;

trait LoadsEntityDefinitions
{
	protected $entities;

	protected function loadEntities(array $entities): void
	{
		$this->entities = collect($this->parseDefinitions($entities, Entity::class));
	}

	/**
	 * @return mixed
	 */
	public function entities()
	{
		return $this->entities;
	}

	public function getEntity(string $key)
	{
		return $this->entities->where('key', $key)->first();
	}
}