<?php


namespace Alablaster\Architect\Domain\Entities;


use Alablaster\Architect\Domain\Core\Exceptions\DefinitionDoesNotConformToStandard;
use Alablaster\Architect\Domain\Core\Interfaces\StubBuilderReference;
use Alablaster\Architect\Domain\Entities\Factories\FactoryDefinitionFactory;
use Alablaster\Architect\Domain\Entities\Factories\MigrationFunctionFactory;
use Alablaster\Architect\Domain\Entities\Traits\GenerateFieldRulesTrait;
use Alablaster\Architect\Domain\Entities\Traits\LoadsEntities;
use Illuminate\Support\Str;

class Field implements StubBuilderReference
{

	use GenerateFieldRulesTrait;
	use LoadsEntities;

	private $definition;
	public $key;
	public $name;
	public $label;
	public $type;
	public $inputType;
	public $massAssignable;
	public $index;
	public $nullable;
	public $unique;
	public $visible;
	public $factoryDefinition;
	public $inRequest;
	public $use;

	const FIELDTYPES = [
		'bigInteger' => ['input' => 'number'],
		'binary' => ['input' => 'number'],
		'boolean' => ['input' => 'checkbox'],
		'char' => ['input' => 'number'],
		'data' => ['input' => 'textarea'],
		'dateTime' => ['input' => 'datetime'],
		'dateTimeTz' => ['input' => 'datetime'],
		'decimal'  => ['input' => 'number'],
		'double' => ['input' => 'number'],
		'enum' => ['input' => 'select'],
		'float'  => ['input' => 'number'],
		'geometry' => ['input' => 'textarea'],
		'geometryCollection' => ['input' => 'textarea'],
		'integer'  => ['input' => 'number'],
		'id'  => ['input' => 'number'],
	    'increments'  => ['input' => 'number'],
	    'ipAddress'  => ['input' => 'textarea'],
		'json' => ['input' => 'textarea'],
		'jsonb' => ['input' => 'textarea'],
		'lineString' => ['input' => 'text'],
		'longText' => ['input' => 'textarea'],
		'macAddress' => ['input' => 'text'],
		'mediumIncrements' => ['input' => 'number'],
		'mediumInteger' => ['input' => 'number'],
		'mediumText' => ['input' => 'textarea'],
		'multiLineString' => ['input' => 'text'],
		'multiPoint' => ['input' => 'text'],
		'multiPolygon' => ['input' => 'text'],
		'nullableTimestamps' => ['input' => 'datetime'],
		'point' => ['input' => 'text'],
		'polygon' => ['input' => 'text'],
		'smallInteger' => ['input' => 'number'],
		'string' => ['input' => 'text'],
	    'set' => ['input' => 'select'],
		'text' => ['input' => 'textarea'],
		'time' => ['input' => 'time'],
		'timeTz' => ['input' => 'time'],
		'timestamp' => ['input' => 'datetime'],
		'timestampTz' => ['input' => 'datetime'],
		'tinyInteger' => ['input' => 'number'],
		'unsignedBigInteger' => ['input' => 'number'],
		'unsignedDecimal' => ['input' => 'number'],
		'unsignedInteger' => ['input' => 'number'],
		'unsignedMediumInteger' => ['input' => 'number'],
		'unsignedSmallInteger' => ['input' => 'number'],
		'unsignedTinyInteger' => ['input' => 'number'],
		'uuid' => ['input' => 'text'],
		'year' => ['input' => 'date']
	];

	public function __construct($key, $field = [])
	{
		$this->definition = $field;
		$this->key = $key;
		$this->name = isset($field['name']) ? $field['name'] : Str::lower(Str::snake($key));
		$this->label = isset($field['label']) ? $field['label'] : Str::title(str_replace('_', ' ', Str::snake($key)));
		$this->type = isset($field['type']) ? $field['type'] : ['name' => 'string', 'max' => 255];
		$this->massAssignable = isset($field['mass_assignable']) ? $field['mass_assignable'] : config('architect.mass_assignable');
		$this->index = isset($field['index']) ? $field['index'] : false;
		$this->nullable = isset($field['nullable']) ? $field['nullable'] : false;
		$this->unique = isset($field['unique']) ? $this->generateUniqueArray($field['unique']) : $this->generateUniqueArray();
		$this->visible = isset($field['visible']) ? $field['visible'] : true;
		$this->inRequest = isset($field['in_request']) ? $field['in_request'] : true;
		$this->use = isset($field['use']) ? $field['use'] : $this->setUse();
		$this->inputType = isset($field['input_type']) ? $field['input_type'] : $this->guessInputType();
		$this->factoryDefinition = (string) new FactoryDefinitionFactory($this, isset($field['faker']) ? $field['faker'] : null);
	}

	public function __toString()
	{
		return $this->name;
	}

	public function get($property)
	{
		switch ($property)
		{
			default:
				return $this->$property;
		}
	}

	public function generateMigrationMethod()
	{
		return new MigrationFunctionFactory($this);
	}

	private function generateUniqueArray($setting = array())
	{

		if($setting == null) {
			return null;
		}

		if(isset($setting['key'])) {
			$entity = $this->getEntityByKey($setting['key']);
			if ($entity != null) {
				$array['class'] = $entity->get('fullName');
			}
		} else if(isset($setting['class'])) {
			$array['class'] = $setting['class'];
		} else if(isset($setting['table'])) {
			$array['class'] = $setting['table'];
		} else {
			throw new DefinitionDoesNotConformToStandard('Unique rule on ' . $this->key . ' requires an entity Key, Class, or Table name.');
		}

		if(isset($setting['id'])) {
			$array['id'] = $setting['id'];
		} else {
			$array['id'] = 'id';
		}

		return $array;
	}


	private function setUse()
	{
		if($this->type != 'string') return null;

		if(strpos('email', $this->name))
			return 'email';

		if(strpos('phone', $this->name))
			return 'phone';

		if(strpos('image', $this->name))
			return 'image';

		$types = [
			'first_name' => 'firstName',
			'last_name' => 'lastName',
			'city' => 'city',
			'country' => 'country',
			'state' => 'state'
		];

		if(key_exists($this->name, $types))
			return $types[$this->name];

		return null;
	}

	private function guessInputType()
	{
		if($this->use)
		{
			switch ($this->use)
			{
				case 'email':
					return 'email';

				case 'image';
					return 'upload';

				case 'country':
					return 'country';
			}
		}

		if(is_array($this->type)) {
			return self::FIELDTYPES[$this->type['name']]['input'];
		} else {
			return self::FIELDTYPES[$this->type]['input'];
		}
	}

}
