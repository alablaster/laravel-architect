<?php


namespace Alablaster\Architect\Domain\Entities\Builders;


use Alablaster\Architect\Domain\Core\Builders\Builder;
use Alablaster\Architect\Domain\Core\Builders\StubBuilder;
use Alablaster\Architect\Domain\Core\Factories\FileLocationFactory;
use Alablaster\Architect\Domain\Core\Traits\InteractsWithFilesTrait;
use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Domain\Entities\Traits\LoadsEntities;
use Faker\Provider\File;
use Symfony\Component\VarDumper\Cloner\Stub;

class UpdateRequestBuilder extends StubBuilder
{
	public function __construct(Entity $entity)
	{
		if(!$entity->make['requests']) return;

		parent::__construct(
			$this->openFile(__DIR__ . '/../stubs/update-request.stub'),
			new FileLocationFactory('request.update', $entity),
			[Entity::class => $entity]
		);

		$this->build();
	}

}