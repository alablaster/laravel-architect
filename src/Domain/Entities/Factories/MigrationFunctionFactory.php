<?php


namespace Alablaster\Architect\Domain\Entities\Factories;


use Alablaster\Architect\Domain\Core\Exceptions\DefinitionDoesNotConformToStandard;
use Alablaster\Architect\Domain\Entities\Field;

class MigrationFunctionFactory
{

	private $field;

	public function __construct(Field $field)
	{
		$this->field = $field;
	}

	public function __toString()
	{
		return $this->returnFunction();
	}

	private function returnFunction() : string
	{
		if(is_array($this->field->type))
		{
			$type = $this->field->type['name'];
		} else if (is_string($this->field->type)) {
			$type = $this->field->type;
		} else {
			throw new DefinitionDoesNotConformToStandard($this->field->key . ' does not have a valid field type.');
		}

		if(!array_key_exists($type, Field::FIELDTYPES)){
			throw new DefinitionDoesNotConformToStandard($type . ' is not a valid field type');
		}

		$method = "\n\t\t\t" . '$table->';
		$method .= $this->makeFunction($type);
		$method .= $this->addModifiers();
		$method .= ';';

		return $method;
	}



	private function makeFunction($type)
	{
		if($this->field->type == 'id')
		{
			return 'id()';
		}

		if(!is_array($this->field->type))
		{
			return $type . '(\'' . $this->field->name . '\')';
		}

		switch ($type) {
			case 'string':
				$max = is_array($this->field->type) && isset($this->type['max']) ? $this->field->type['max'] : '255';
				return $type . '(\'' . $this->field->name . '\', ' . $max . ')';
				break;
			case 'double':
			case 'float':
			case 'decimal':
			case 'unsignedDecimal':
				$total = isset($this->field->type['total']) ? $this->field->type['total'] : 8;
				$decimal = isset($this->field->type['decimal']) ? $this->field->type['decimal'] : 2;
				return $type . '(\'' . $this->field->name . '\', ' . $total . ', ' . $decimal . ')';
				break;
			case 'set':
				$set = isset($this->field->type['set']) ? $this->field->type['set'] : [];
				return $type . '(\'' . $this->field->name . '\', [\'' . implode('\', \'', $set) . '\'])';
				break;

			case 'id':
				return 'id()';
				break;

			default:
				return $type . '(\'' . $this->field->name . '\')';

		}


	}

	private function addModifiers()
	{
		$modifiers = '';

		if($this->field->nullable) {
			$modifiers .= '->nullable()';
		}

		return $modifiers;
	}

}