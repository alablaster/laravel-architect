<?php


namespace Alablaster\Architect\Domain\Frontend\Builders;


use Alablaster\Architect\Domain\Core\Builders\StubBuilder;
use Alablaster\Architect\Domain\Core\Factories\FileLocationFactory;
use Alablaster\Architect\Domain\Entities\Entity;

class ControllerBuilder extends StubBuilder
{
	private $entity;

	public function __construct(Entity $entity)
	{
		$this->entity = $entity;

		$references =[
			Entity::class => $entity
		];

		parent::__construct(null, null, $references);

		$this->buildControllers();
	}

	private function buildControllers()
	{
		if(array_key_exists('api', $this->entity->frontends)) {
			$this->buildApiController($this->entity->frontends['api']);
		}

		if(array_key_exists('blade', $this->entity->frontends)) {
			$this->buildApiController($this->entity->frontends['blade']);
		}

		if(array_key_exists('livewire', $this->entity->frontends)) {
			$this->buildApiController($this->entity->frontends['livewire']);
		}

		if(array_key_exists('inertia', $this->entity->frontends)) {
			$this->buildApiController($this->entity->frontends['inertia']);
		}
	}

	protected function buildApiController($config) {

		$this->setLocation(new FileLocationFactory('controller.api', $config['path']));

		switch ($config['type']) {
			case 'crud':
				$this->getStubFrom(__DIR__ . '/../stubs/controller/api-crud.stub');
				break;
		}

		$this->build();
	}

	protected function buildBladeController($config) {

		$this->setLocation(new FileLocationFactory('controller', $config['path']));

		switch ($config['type']) {
			case 'crud':
				$this->getStubFrom(__DIR__ . '/../stubs/controller/blade-crud.stub');
				break;
		}

		$this->build();
	}

	protected function buildLivewireController($config) {

		$this->setLocation(new FileLocationFactory('controller', $config['path']));

		switch ($config['type']) {
			case 'crud':
				$this->getStubFrom(__DIR__ . '/../stubs/controller/livewire-crud.stub');
				break;
			case 'directory':
				$this->getStubFrom(__DIR__ . '/../stubs/controller/livewire-directory.stub');
				break;
		}

		$this->build();
	}

	protected function buildInertiaController($config) {

		$this->setLocation(new FileLocationFactory('controller', $config['path']));

		switch ($config['type']) {
			case 'crud':
				$this->getStubFrom(__DIR__ . '/../stubs/controller/blade-crud.stub');
				break;
			case 'directory':
				$this->getStubFrom(__DIR__ . '/../stubs/controller/blade-directory.stub');
				break;
		}

		$this->build();
	}
}