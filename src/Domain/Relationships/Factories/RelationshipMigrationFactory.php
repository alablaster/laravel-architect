<?php


namespace Alablaster\Architect\Domain\Relationships\Factories;


use Alablaster\Architect\Domain\Core\Traits\InteractsWithFilesTrait;
use Alablaster\Architect\Domain\Relationships\Relationship;
use Illuminate\Support\Str;

class RelationshipMigrationFactory
{
	use InteractsWithFilesTrait;
	
	protected $relationship;
	
	public function __construct(Relationship $relationship)
	{
		$this->relationship = $relationship;
	}
	
	public function __toString()
	{
		return $this->migrationContent();
	}
	
	private function migrationContent() : string
	{
		$stub = $this->openFile(__DIR__ . '/../stubs/blank_migration.stub' );

		$stub = str_replace('{{ class }}', $this->class(), $stub);
		
		$stub = str_replace('{{ up }}', $this->up(), $stub);
		
		$stub = str_replace('{{ down }}', $this->down(), $stub);
		
		return $stub;
	}
	
	private function class()
	{
		return 'Create' . $this->relationship->primaryEntity->get('name') . $this->relationship->childEntity->get('name') . 'Relationship';
	}
	
	private function up()
	{
		
		$content = '';
		
		$content .= $this->addForeignKey($this->relationship->primaryEntity, $this->relationship->childEntity);
		
		return $content;
	}
	
	private function down()
	{
		
		$content = $this->removeForeignKey($this->relationship->primaryEntity, $this->relationship->childEntity);
		
		return $content;
		
		
	}
	
	public function addForeignKey($entity, $relation)
	{
		$content = "\n\t\tSchema::table('" . $entity->get('table') . '\', function (Blueprint $table) {';
		$content .= "\n\t\t\t" . '$table->unsignedBigInteger(\'' . Str::singular($relation->get('table')) . "_id');";
		$content .= "\n\t\t\t" . '$table->foreign(\'' . Str::singular($relation->get('table')) . '_id\')';
		$content .= "\n\t\t\t\t" . '->references(\'id\')';
		$content .= "\n\t\t\t\t" . '->on(\'' . $relation->get('table') .'\')';
		$content .= "\n\t\t\t\t" . '->onDelete(\'' . $this->relationship->onDelete .'\');';
		$content .=	"\n\t\t});";
		
		return $content;
	}
	
	public function removeForeignKey($entity, $relation)
	{
		$content = "\n\t\tSchema::table('" . $entity->get('table') . '\', function (Blueprint $table) {';
		$content .= "\n\t\t\t\$table->dropForeign('" . $entity->get('table') . '_' . Str::singular($relation->get('table')) . "_id_foreign');";
		$content .= "\n\t\t\t\$table->dropColumn('" . $entity->get('table') . '_' . Str::singular($relation->get('table')) . "');";
		$content .=	"\n\t\t});";

		return $content;
	}
}
