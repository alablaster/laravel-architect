<?php


namespace Alablaster\Architect\Tests\Feature\Core\Factories;


use Alablaster\Architect\Domain\Core\Factories\FileLocationFactory;
use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Domain\SiteMap\Method;
use Alablaster\Architect\Domain\SiteMap\SiteNode;
use Alablaster\Architect\Tests\TestCase;

class FileLocationFactoryTest extends TestCase
{
	/**
	 * @test
	 */
	public function it_generates_a_location_for_a_model()
	{
		$entity = new Entity('Test');

		$result = new FileLocationFactory('model', $entity);

		$this->assertStringContainsString('app/Models/Test.php', $result);
	}

	/**
	 * @test
	 */
	public function it_generates_a_location_for_a_migration()
	{
		$entity = new Entity('Test');
		$result = new FileLocationFactory('migration', $entity);

		$this->assertStringContainsString(config('architect.path.migration'), $result);
		$this->assertStringContainsString('create_tests_table.php', $result);
	}

	/**
	 * @test
	 */
	public function it_generates_a_location_for_a_store_request()
	{
		$entity = new Entity('Test/Entity');
		$result = new FileLocationFactory('request.store', $entity);

		$this->assertStringContainsString('app/Http/Requests/Test/Entity/StoreRequest', $result);
	}

	/**
	 * @test
	 */

	public function it_generates_a_location_for_a_update_request()
	{
		$entity = new Entity('Test');
		$result = new FileLocationFactory('request.update', $entity);

		$this->assertStringContainsString('app/Http/Requests/Test/UpdateRequest', $result);
	}


	/**
	 * @test
	 */

	public function it_generates_a_location_for_a_factory()
	{
		$entity = new Entity('Something/Test');
		$result = new FileLocationFactory('factory', $entity);

		$this->assertStringContainsString('database/factories/Something/TestFactory.php', $result);
	}

	/**
	 * @test
	 */

	public function it_generates_a_location_for_a_feature_test()
	{
		$entity = new Entity('Something/Test');
		$result = new FileLocationFactory('test.feature.store', $entity);

		$this->assertStringContainsString('tests/feature/Something/Test/StoreTestTest.php', $result);
	}


	/**
	 * @test
	 */

	public function it_generates_a_location_for_a_livewire_view_index()
	{
		$this->addToSettingType('entities', 'Test/Entity');

		$node = new SiteNode('/entity', ['frontend' => 'livewire']);

		$result = new FileLocationFactory('livewire.view', new Method($node, 'index'));

		$this->assertStringContainsString('resources/views/livewire/entity/index.blade.php', $result);
	}


	/**
	 * @test
	 */

	public function it_generates_a_location_for_a_livewire_class_index()
	{
		$this->addToSettingType('entities', 'Test/Entity');

		$node = new SiteNode('/entity', ['frontend' => 'livewire']);

		$result = new FileLocationFactory('livewire.view',  new Method($node, 'index'));

		$this->assertStringContainsString('resources/views/livewire/entity/index.blade.php', $result);
	}

	/**
	 * @test
	 */

	public function it_generates_a_location_for_a_group_of_resources()
	{
		$result = new FileLocationFactory('resource',  new Entity('Test/Entity'));

		$this->assertStringContainsString('app/Http/Resources/Test/Entity', $result);
	}


	/**
	 * @test
	 */

	public function it_generates_a_location_for_a_manager()
	{
		$this->addToSettingType('entities', 'Test/Entity');

		$result = new FileLocationFactory('manager',  new Entity('Test/Entity'));

		$this->assertStringContainsString('app/Managers/Test/EntityManager.php', $result);
	}

	/**
	 * @test
	 */

	public function it_generates_a_location_for_an_api_controller()
	{

		$result = new FileLocationFactory('controller.api',  '/something/else');

		$this->assertStringContainsString('app/Http/Controllers/Api/V1/Something/Else/ElseController.php', $result);
	}

}