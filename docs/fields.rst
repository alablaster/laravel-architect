Fields
============
Fields represent the fields that will be created in a database, but also the fields included in model factories, requests, and tests.

Key
^^^^^
A key should be the snake cased singular version of the field name.

label
^^^^^^^^
**default: name**

Defaults to the key name in snake case. As the field name in the database, and where ever the field is referred to programatically.

`name: name`

label
^^^^^^^^^^
**default: Name**

Defaults to a title case version of the key. This is used for front end usage of the field name, such as on forms.

`label: Name`

type
^^^^^
**default: [name: string, max: 255])**

The type can be set as either a string with the name of the type, or as an array with the name of the field type, plus a [supported field characteristics](Supported Field Characteristics). All [field types defined in Laravel](https://laravel.com/docs/8.x/migrations#available-column-types) are useable, except for Increments

mass_assignable
^^^^^^^^^^^^^^^^
**default: true**

By default all fields are set to mass assignable, meaning on the model they are included in the mass assignable array. This default can be over written the an config file.

index
^^^^^^
**default: false**

Creates an index for the column.

nullable
^^^^^^^^
**default: false**

Allows the database field to be nullable and adds required rules to requests, forms, and unit tests.

unique
^^^^^^^
**default: false**

Adds a unique database rule, and checks for uniqueness in the request rules.

visible
^^^^^^^^^
**default: true**

Includes the fields on front end of system.

in_request
^^^^^^^^^^^
**default: true**

Includes validation rules on requests

use
^^^^
**default: null**

Defines the field use, (eg. phone, email) which allows for a variety of magic settings in the system, specifically around factory fakers and input types. Currently the only uses supported are uses related to string fields. Some keys will also result in an automatically set name, for example, `first_name`, `last_name`, `city`, `state`, `*phone*`, `*email*`,  `*image*`

+-------------+-------------------------------------------------------+
| Key         | Result                                                |
+=============+=======================================================+
| firstName   | First name faker                                      |
+-------------+-------------------------------------------------------+
| lastName    | Last name faker                                       |
+-------------+-------------------------------------------------------+
| company     | Company name faker                                    |
+-------------+-------------------------------------------------------+
| phone       | Phone faker                                           |
+-------------+-------------------------------------------------------+
| city        | City name faker                                       |
+-------------+-------------------------------------------------------+
| state       | US state name faker and drop down on forms            |
+-------------+-------------------------------------------------------+
| country     | Country name faker and drop down on forms             |
+-------------+-------------------------------------------------------+
| email       | Safe email faker and validation on forms and requests |
+-------------+-------------------------------------------------------+
| image       | A placeholder image url                               |
+-------------+-------------------------------------------------------+

faker
^^^^^^
**default: based on type & use**

Explicitly set a faker use the definitions provided in [FakerPHP](https://fakerphp.github.io/formatters/). If not set, Architect will infer based on the type or use of the field. Most field types supported by Laravel generate a faker, some of the notable exceptions however are geometry fields or other fields which don't have an obvious faker.