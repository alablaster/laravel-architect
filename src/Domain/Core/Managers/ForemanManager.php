<?php


namespace Alablaster\Architect\Domain\Core\Managers;


use Alablaster\Architect\Domain\Core\Traits\InteractsWithFilesTrait;
use Alablaster\Architect\Domain\Entities\Builders\FactoryBuilder;
use Alablaster\Architect\Domain\Entities\Builders\FrontendComponentBuilder;
use Alablaster\Architect\Domain\Entities\Builders\ManagerBuilder;
use Alablaster\Architect\Domain\Entities\Builders\MigrationBuilder;
use Alablaster\Architect\Domain\Entities\Builders\ModelBuilder;
use Alablaster\Architect\Domain\Entities\Builders\ResourceBuilder;
use Alablaster\Architect\Domain\Entities\Builders\StoreRequestBuilder;
use Alablaster\Architect\Domain\Entities\Builders\UpdateRequestBuilder;
use Alablaster\Architect\Domain\Frontend\Builders\ApiControllerBuilder;
use Alablaster\Architect\Domain\Relationships\Builders\RelationshipBuilder;
use Alablaster\Architect\Domain\Relationships\Traits\LoadsRelationships;
use Alablaster\Architect\Domain\Settings\Managers\Definitions;

class ForemanManager
{
	use LoadsRelationships;
	use InteractsWithFilesTrait;

	public $definitions;
	private $siteMapManger;

	public function __construct()
	{
		$this->definitions = app(Definitions::class);
	}

	public function constructApplication()
	{
		$this->buildModels();
		$this->buildRelationships();
	}

	protected function buildModels()
	{
		echo "\nBuilding Entities";

		foreach($this->definitions->entities() as $entity)
		{
			echo "\n \033[32mGenerating entity files for " . $entity->key . "\033[39m";
			new MigrationBuilder($entity);
			new ModelBuilder($entity);
			new FactoryBuilder($entity);
			new StoreRequestBuilder($entity);
			new UpdateRequestBuilder($entity);
			new FrontendComponentBuilder($entity);
			new ResourceBuilder($entity);
			new ManagerBuilder($entity);

			if(key_exists('api', $entity->frontends) && $entity->frontends['api'])
			{
				new ApiControllerBuilder($entity, $entity->frontends['api']);
			}
		}
	}

	private function buildRelationships()
	{
		echo "\nAdding Relationships";

		$manager = new RelationshipBuilder();
		
		foreach($this->relationships() as $relationship)
		{
			$manager->build($relationship);
			echo '.';
		}
	}
}
