<?php


namespace Alablaster\Architect\Domain\Entities\Builders;


use Alablaster\Architect\Domain\Core\Builders\Builder;
use Alablaster\Architect\Domain\Core\Builders\StubBuilder;
use Alablaster\Architect\Domain\Core\Factories\FileLocationFactory;
use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Domain\Entities\Traits\LoadsEntities;
use Illuminate\Support\Str;

class MigrationBuilder extends StubBuilder
{
	use LoadsEntities;

	public $key;
	public $entity;

	public function __construct(Entity $entity)
	{
		if(!$entity->make['migration']) return;

		parent::__construct(
			$this->openFile(__DIR__ . '/../stubs/create_table_migration.stub'),
			new FileLocationFactory('migration', $entity),
			[Entity::class => $entity]);

		$this->build();
	}

	public function getFileLocation()
	{
		return new FileLocationFactory('migration', $this->entity);
	}

	public function getStub()
	{
		return $this->openFile(__DIR__ . '/../stubs/create_table_migration.stub');
	}


	public function populateStub($stub)
	{
		$stub = str_replace(
			'{{ class }}',
			$this->createClassName(), $stub
		);

		$stub = str_replace(
			'{{ fields }}',
			$this->gatherFields(), $stub
		);

		$stub = str_replace(
			'{{ tableName }}',
			$this->entity->table, $stub
		);
		return $stub;
	}

	private function createClassName()
	{
		return 'Create' . Str::plural($this->entity->name) . 'Table';
	}

	private function gatherFields()
	{
		$fields = '';

		$fields .= $this->entity->fields->getMigrationMethods();

		$fields .= $this->createIndices();

		return $fields;
	}

	private function createIndices()
	{
		$indicies = $this->entity->fields->getFields()->where('index', true);

		if($indicies->count())
		{
			$return = "\n\t\t\t" . '$table->index(';

			foreach($indicies as $index)
			{
				$return .= "'" . $index . "', ";
			}

			$return .= ');';

			return $return;
		}

		return '';
	}
}