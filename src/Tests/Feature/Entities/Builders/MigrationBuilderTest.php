<?php


namespace Alablaster\Architect\Tests\Feature\Entities\Builders;


use Alablaster\Architect\Domain\Core\Builders\Builder;
use Alablaster\Architect\Domain\Core\Builders\StubBuilder;
use Alablaster\Architect\Domain\Core\Traits\InteractsWithFilesTrait;
use Alablaster\Architect\Domain\Core\Traits\SettingsTrait;
use Alablaster\Architect\Domain\Entities\Builders\MigrationBuilder;
use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Tests\TestCase;
use http\Encoding\Stream\Enbrotli;

class MigrationBuilderTest extends TestCase
{

	use SettingsTrait;

	/**
	 * @test
	 */
	public function it_is_an_instance_of_stub_Builder()
	{
		$builder = new MigrationBuilder(new Entity('Test'));

		$this->assertInstanceOf(StubBuilder::class, $builder);
	}


	/**
	 * @test
	 */
	public function it_has_the_correct_class_name()
	{
		$builder = new MigrationBuilder(new Entity('Test'));


		$this->assertStringContainsString('class CreateTestsTable extends Migration', $builder);
	}

	/**
	 * @test
	 */
	public function it_contains_a_string_field()
	{
		$this->setSettings([]);

		$this->addToSettingType('entities', 'Test', ['fields' => [
			'name' => []
		]]);
		$builder = new MigrationBuilder(new Entity('Test'));


		$this->assertStringContainsString('$table->string(\'name\', 255);', $builder);
	}

	/**
	 * @test
	 */
	public function it_generates_an_id_field()
	{
		$this->setSettings([]);

		$this->addToSettingType('entities', 'Test');
		$builder = new MigrationBuilder(new Entity('Test'));


		$this->assertStringContainsString('$table->id();', $builder);
	}


	/**
	 * @test
	 */
	public function it_contains_the_create_table_command()
	{
		$builder = new MigrationBuilder(new Entity('User'));


		$this->assertStringContainsString('Schema::create(\'users\', function (Blueprint $table) {', $builder);
	}
	
	
	/**
	 * @test
	 */
	public function it_add_nullable_to_fields()
	{
		$builder = new MigrationBuilder(new Entity('User', ['fields' => ['test' => ['nullable' => true]]]));


		$this->assertStringContainsString('$table->string(\'test\', 255)->nullable()', $builder);
	}

	/**
	 * @test
	 */
	public function it_creates_indecies_for_indexed_fields()
	{
		$builder = new MigrationBuilder(new Entity('User', ['fields' => ['test' => ['index' => true]]]));


		$this->assertStringContainsString('$table->index(\'test\', );', $builder);
	}
}
