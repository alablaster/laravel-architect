<?php


namespace Alablaster\Architect\Tests\Feature\Entities\Builders;


use Alablaster\Architect\Domain\Core\Builders\Builder;
use Alablaster\Architect\Domain\Core\Builders\StubBuilder;
use Alablaster\Architect\Domain\Core\Traits\SettingsTrait;
use Alablaster\Architect\Domain\Entities\Builders\UpdateRequestBuilder;
use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Tests\TestCase;

class UpdateRequestBuilderTest extends TestCase
{

	use SettingsTrait;

	/**
	 * @test
	 */
	public function it_instantiants_as_an_instance_of_stub_builder()
	{

		$this->addToSettingType('entities', 'Test', ['default' => true]);

		$builder = new UpdateRequestBuilder(new Entity('Test'));

		$this->assertInstanceOf(StubBuilder::class, $builder);
	}

	/**
	 * @test
	 */
	public function it_contains_the_correct_class()
	{

		$this->setSettings([]);
		$this->addToSettingType('entities', 'Test', ['default' => true]);

		$request = new UpdateRequestBuilder(new Entity('Test'));

		$this->assertStringContainsString('class UpdateRequest', $request);
	}

	/**
	 * @test
	 */
	public function it_contains_the_rules()
	{

		$this->setSettings([]);
		$this->addToSettingType('entities', 'Test', ['fields' => ['name' => ['default' => true]]]);

		$request = new UpdateRequestBuilder(new Entity('Test'));

		$this->assertStringContainsString('string', $request);
		$this->assertStringContainsString('max:255', $request);
	}




}