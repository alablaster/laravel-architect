<?php


namespace Alablaster\Architect\Domain\Core\Interfaces;


interface StubBuilderReference
{
	/**
	 * Accesses the properties of the class
	 *
	 * @param string $property
	 * @return string
	 */
	public function get(string $property);
}