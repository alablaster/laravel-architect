<?php


namespace Alablaster\Architect\Domain\Entities;


use Alablaster\Architect\Domain\Core\Definition;
use Alablaster\Architect\Domain\Core\Interfaces\StubBuilderReference;
use Alablaster\Architect\Domain\Core\Traits\SettingsTrait;
use Alablaster\Architect\Domain\Relationships\Traits\LoadsRelationships;
use Illuminate\Support\Str;

class Entity extends Definition implements StubBuilderReference
{
	use LoadsRelationships;

	use SettingsTrait;

	public $fields;
	public $make;
	public $frontends;

	public function __construct($key, $definition = [])
	{
		parent::__construct($key, $definition);

		$this->setFrom('name', $definition, 'name', $this->makeName());
		$this->setFrom('table', $definition, 'table', $this->makeTableName());
		$this->setFrom('namespace', $definition, 'namespace', $this->makeNamespace());
		$this->set('fullName', $this->makeFullName());

		// Load a collection of fields
		$this->fields = isset($definition['fields']) ? new FieldCollection($definition['fields'], $this->key) : new FieldCollection([], $this->key);

		// load a list of frontends which should be built for this entity
		$this->frontends = isset($definition['frontend']) ? $this->makeFrontends($definition['frontend']) : $this->makeFrontends();

		// Set setting for which builders will be triggered for this entity
		$this->make = [
				'manager' => isset($definition['make']['manager']) ? $definition['make']['manager'] : config('architect.make.manager'),
				'migration' => isset($definition['make']['migration']) ? $definition['make']['migration'] : config('architect.make.migration'),
				'model' => isset($definition['make']['model']) ? $definition['make']['model'] : config('architect.make.model'),
				'requests' => isset($definition['make']['requests']) ? $definition['make']['requests'] : config('architect.make.requests'),
				'resources' => isset($definition['make']['resources']) ? $definition['make']['resources'] : config('architect.make.resources'),
				'factory' => isset($definition['make']['factory']) ? $definition['make']['factory'] : config('architect.make.factory'),
			];
	}

	/**
	 * Entity placeholders which will be called at runtime.
	 *
	 * @param string $property
	 * @return bool|string
	 */
	public function get($property)
	{
		switch($property)
		{
			case 'factoryDefinitions':
				return $this->fields->getFactoryDefinitions();

			case 'fillables':
				return $this->makeFillables();

			case 'migrationMethods':
				return $this->fields->getMigrationMethods();

			case 'storeRules':
				return $this->fields->getStoreRequestRules();

			case 'updateRules':
				return $this->fields->getUpdateRequestRules();

			case 'mountingMap':
				return $this->fields->getMountingMap();

			case 'fieldMap':
				return $this->fields->getFieldMap();

			case 'livewirePath':
				return $this->frontends['livewire']['path'] ? $this->frontends['livewire']['path'] : null;

			case 'bladePath':
				return $this->frontends['blade']['path'] ? $this->frontends['blade']['path'] : null;

			case 'inertiaPath':
				return $this->frontends['inertia']['path'] ? $this->frontends['inertia']['path'] : null;

			case 'variables':
				return $this->fields->getVariables();
		}

		return parent::get($property);
	}

	/**
	 * Create a full name
	 *
	 * @return string
	 */
	private function makeFullName()
	{
		$basespace = explode('/', config('architect.path.model'));

		$basespace[] = $this->get('namespace');

		foreach($basespace as $key => $value)
		{
			if($value == null) {
				unset($basespace[$key]);
			} else {
				$basespace[$key] = Str::studly($value);
			}
		}


		$basespace = implode('\\', $basespace);

		return $basespace . '\\' . $this->get('name');
	}

	/**
	 * Create an array of frontend components which define if component
	 * partials need to be created for this entity.
	 *
	 * @param array $definition
	 * @return bool|\Illuminate\Config\Repository|\Illuminate\Contracts\Foundation\Application|mixed
	 */
	private function makeFrontends($definition = [])
	{
		$components =  config('architect.frontends');

		if($definition === false)
		{
			return false;
		}

		foreach($components as $key => $value)
		{
			if(array_key_exists($key, $definition)) {
				$components[$key] = $definition[$key];
			}
		}


		return $components;
	}

	/**
	 * Generate model name from key.
	 *
	 * @return string
	 */
	private function makeName()
	{
		$key = explode('/', $this->key);
		return Str::studly(last($key));
	}

	/**
	 * Generate table name for the entity from the key.
	 *
	 * @return string
	 */
	private function makeTableName()
	{
		$key = explode('/', $this->key);
		return Str::plural(Str::snake(last($key)));
	}

	/**
	 * Generate namespace from the key.
	 *
	 * @param $entity
	 * @return string|null
	 */
	private function makeNamespace()
	{
		$key = array_filter(explode('/', $this->key));
		$key = array_map( function ($item)  { return Str::studly($item); }, $key);
		array_pop($key);
		$key = array_filter($key);

		return implode('\\', $key);
	}

	/**
	 * Create a list of the fillable fields for the entity.
	 *
	 * @return string
	 */
	private function makeFillables()
	{
		$names = $this->fields->massAssignableFieldNames()->toArray();

		foreach($names as $key => $name) {
				$names[$key] = "'" . $name . "'";
		}

		return implode(', ' , $names);
	}

}
