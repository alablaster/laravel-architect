<?php


namespace Alablaster\Architect\Domain\Settings\Managers;


use Alablaster\Architect\Domain\Core\Traits\InteractsWithFilesTrait;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Yaml\Yaml;

class SettingsManager
{
	use InteractsWithFilesTrait;
	
	private $settings;
	private $path;

	public function __construct($settings = null)
	{
		$this->path = base_path() . config('architect.settings_path');
	}

	public function getSettings($key = null)
	{
		if($this->settings == null)
		{
			$this->getSettingsFromFile();
		}
		if($key == null) {
			return $this->settings;
		} else if(array_key_exists($key, $this->settings)) {
			return $this->settings[$key];
		} else {
			return [];
		}
	}

	private function getSettingsFromFile()
	{
		if(!file_exists($this->path)) {
			$this->settings = [];
		} else {
			$this->settings = (array) Yaml::parse(
				$this->openFile($this->path)
			);
		}
	}

	public function setSettings($settings, $key = false)
	{
		if($key) {
			$this->settings[$key] = $settings;
		} else {
			$this->settings = $settings;
		}

		$this->saveArray($this->settings);
	}

	public function addToType($type, $key, $data = ['default' => true])
	{
		$this->settings[$type][$key] = $data;
		$this->saveArray($this->settings);
	}

	protected function saveArray(Array $array)
	{
		$yaml = Yaml::dump($array, 10);
		$this->saveFile($this->path, $yaml);
	}
}
