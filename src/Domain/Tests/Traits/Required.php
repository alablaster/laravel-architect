<?php


namespace Alablaster\Architect\Domain\Tests\Traits;


use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Domain\Entities\Field;
use Alablaster\Architect\Domain\Tests\Factories\FieldTests\FieldRequiredFactory;

trait Required
{
	protected function required(Field $field, Entity $entity) : string
	{
		return new FieldRequiredFactory($field, $entity);
	}
}