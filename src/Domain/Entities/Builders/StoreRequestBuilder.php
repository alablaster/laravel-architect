<?php


namespace Alablaster\Architect\Domain\Entities\Builders;


use Alablaster\Architect\Domain\Core\Builders\StubBuilder;
use Alablaster\Architect\Domain\Core\Factories\FileLocationFactory;
use Alablaster\Architect\Domain\Entities\Entity;

class StoreRequestBuilder extends StubBuilder
{
	public function __construct(Entity $entity)
	{
		if(!$entity->make['requests']) return;

		parent::__construct(
			$this->openFile(__DIR__ . '/../stubs/store-request.stub'),
			new FileLocationFactory('request.store', $entity),
			[Entity::class => $entity]
		);

		$this->build();

	}

}