<?php


namespace Alablaster\Architect\Tests\Feature\Console;


use Alablaster\Architect\Domain\Core\Traits\InteractsWithFilesTrait;
use \Alablaster\Architect\Tests\TestCase;
use Illuminate\Support\Facades\Artisan;

class Blueprints extends TestCase
{
	use InteractsWithFilesTrait;


	
	public function it_creates_a_blueprints_file_in_the_project_root()
	{
		$path = base_path() . '/blueprints.yaml';

		$this->deleteFile($path);
		
		$this->assertFileDoesNotExist($path);
		
		Artisan::call('architect:blueprints');
		
		$this->assertFileExists($path);
		
		unlink($path);
	}
}
