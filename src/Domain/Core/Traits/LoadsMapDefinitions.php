<?php


namespace Alablaster\Architect\Domain\Core\Traits;


use Alablaster\Architect\Domain\Core\Exceptions\DefinitionDoesNotConformToStandard;
use Alablaster\Architect\Domain\SiteMap\SiteNode;
use Illuminate\Support\Arr;

trait LoadsMapDefinitions
{
	protected $web;
	protected $api;
	protected $map;

	public function loadMap(array $map)
	{
		$this->loadWeb($map['web']);
	}

	public function getWebNode($key)
	{
		return Arr::get($this->web, "$key");
	}

	protected function loadWeb(array $web)
	{
		$this->map['web'] = $this->parseNodes($web, SiteNode::class);
	}


	protected function parseNodes($definitions, $definition)
	{
		$array = [];

		foreach($definitions as $key => $value)
		{
			$array = array_merge($array, $this->parseNode($key, $value, $definition));
		}

		$this->setNodeKeys($array);

		return collect($array);
	}

	protected function setNodeKeys(&$array, $parent = '')
	{
		$parentKey = '';


		if(isset($array['_node'])) {

			// create a key based on the nested position
			$parentKey = $parent ? $parent . '.' . $array['_node']->key : $array['_node']->key;

			// set the correct key on the parent
			$array['_node']->setKey($parentKey);

			// store a pointer to the node in the web array
			$this->web[$parentKey] = &$array['_node'];
		}

		// recursively trigger function for child nodes.
		foreach ($array as $key => $item) {
			if ($key != '_node') {
				$this->setNodeKeys($item, $parentKey);
			}
		}

	}

	protected function parseNode($key, $value, $definition)
	{

		if(is_integer($key) && is_string($value)) {
			return [$value => ['_node' => new $definition($value)]];
		} elseif (is_array($value) && is_string($key)) {
			$array = ['_node' => new $definition($key, $value)];
			if(isset($value['children'])) {
				foreach($value['children'] as $k => $v)
				{
					$array = array_merge($array, $this->parseNode($k, $v, $definition));
				}
			}

			return [$key => $array];
		} else {
			throw new DefinitionDoesNotConformToStandard('Definition does not conform to standard: ' . json_encode(['key' => $key, 'definition' => $value]));
		}
	}
}