<?php


namespace Alablaster\Architect\Domain\Entities\Builders;


use Alablaster\Architect\Domain\Core\Builders\StubBuilder;
use Alablaster\Architect\Domain\Core\Factories\FileLocationFactory;
use Alablaster\Architect\Domain\Entities\Entity;
use Illuminate\Support\Str;

class ResourceBuilder extends StubBuilder
{
	private $entity;

	public function __construct(Entity $entity)
	{
		if(!$entity->make['resources']) return;

		parent::__construct(null, null, [Entity::class => $entity]);

		$this->entity = $entity;
		$this->makeResource();
		$this->makeIndexResource();
		$this->makeCollection();
		$this->makeRelationshipResource();
		$this->makeRelationshipIndexResource();
		$this->makeRelationshipCollection();
	}

	private function makeResource()
	{
		$this->setLocation(new FileLocationFactory('resource', $this->entity) . '/' . Str::studly($this->entity->get('name')) . 'Resource.php');
		$this->getStubFrom(__DIR__ . '/../stubs/resources/resource.php.stub');
		$this->addSlot('attributeMap', $this->attributeMap());
		$this->addSlot('relationships', $this->relationships());
		$this->build();
	}

	private function makeIndexResource()
	{
		$this->setLocation(new FileLocationFactory('resource', $this->entity) . '/' . Str::studly($this->entity->get('name')) . 'IndexResource.php');
		$this->getStubFrom(__DIR__ . '/../stubs/resources/index-resource.php.stub');
		$this->addSlot('attributeMap', $this->attributeMap());
		$this->build();
	}

	private function makeCollection()
	{
		$this->setLocation(new FileLocationFactory('resource', $this->entity) . '/' . Str::studly($this->entity->get('name')) . 'Collection.php');
		$this->getStubFrom(__DIR__ . '/../stubs/resources/resource-collection.php.stub');
		$this->build();
	}

	private function makeRelationshipResource()
	{
		$this->setLocation(new FileLocationFactory('resource', $this->entity) . '/Relationship/' . Str::studly($this->entity->get('name')) . 'RelationshipResource.php');
		$this->getStubFrom(__DIR__ . '/../stubs/resources/relationship-resource.php.stub');
		$this->build();
	}

	private function makeRelationshipIndexResource()
	{
		$this->setLocation(new FileLocationFactory('resource', $this->entity) . '/Relationship/' . Str::studly($this->entity->get('name')) . 'RelationshipIndexResource.php');
		$this->getStubFrom(__DIR__ . '/../stubs/resources/relationship-index-resource.php.stub');
		$this->build();
	}

	private function makeRelationshipCollection()
	{
		$this->setLocation(new FileLocationFactory('resource', $this->entity) . '/Relationship/' . Str::studly($this->entity->get('name')) . 'RelationshipCollection.php');
		$this->getStubFrom(__DIR__ . '/../stubs/resources/relationship-collection.php.stub');
		$this->build();
	}

	private function attributeMap()
	{
		$attributes = [];
		foreach($this->entity->fields->getVisibleFields() as $field)
		{
			$attributes[] = '\'' . $field->name . '\' => $this->resource->' . $field->name;
		}

		return implode(",\n\t\t\t\t", $attributes);
	}

	private function relationships()
	{
		// TODO: add relationships
	}
}