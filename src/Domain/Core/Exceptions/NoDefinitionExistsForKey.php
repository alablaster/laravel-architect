<?php


namespace Alablaster\Architect\Domain\Core\Exceptions;


use Throwable;

class NoDefinitionExistsForKey extends \Exception
{
	public function __construct($type, $key, $code = 0, Throwable $previous = null)
	{
		$message = 'No ' . $type . ' definition exists for "' . $key . '"';
		parent::__construct($message, $code, $previous);
	}
}