<?php


namespace Alablaster\Architect\Tests\Feature\Relationships;


use Alablaster\Architect\Domain\Core\Traits\SettingsTrait;
use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Domain\Relationships\Relationship;
use Alablaster\Architect\Tests\TestCase;
use Alablaster\Architect\Domain\Core\Exceptions\NoDefinitionExistsForKey;

class RelationshipTest extends TestCase
{
	public $relationship;
	use SettingsTrait;

	public function setUp(): void
	{
		parent::setUp();

		$this->setSettings([
				'entities' => [
					'Test' => [
						'default' => true
					],
					'SubTest' => [
						'default' => true
					],
					'ManyTest' => [
						'default' => true
					]
				],
				'relationships' => [
					'Test>SubTest' => [
						'default' => true
					],
					'Test=ManyTest' => [
						'default' => true
					],
					'ManyTest<Test' => [
						'default' => true
					]
				]
			]
		);
	}

	/**
	 * @test
	 */

	public function definition_can_be_loaded()
	{
		$relationship = new Relationship('Test>SubTest');

		$this->assertNotEmpty($relationship->definition);
	}

	/**
	 * @test
	 */

	public function if_definition_cant_be_loaded_an_exception_is_thrown()
	{
		$this->expectException(NoDefinitionExistsForKey::class);
		$this->expectDeprecationMessage('No relationships definition exists for "not_a_real>key"');
		new Relationship('not_a_real>key');
	}


	/**
	 * @test
	 */
	public function default_type_is_one_to_many()
	{
		$relationship = new Relationship('Test>SubTest');

		$this->assertArrayNotHasKey('type', $relationship->definition);
		$this->assertSame('oneToMany', $relationship->type);
	}

	/**
	 * @test
	 */
	public function if_a_valid_type_is_set_it_is_loaded()
	{

		$relationship = new Relationship('Test=ManyTest');
		$this->assertSame('oneToOne', $relationship->type);
	}


	/**
	 * @test
	 */
	public function loads_primary_entity_from_key()
	{
		$relationship = new Relationship('Test>SubTest');

		$this->assertInstanceOf(Entity::class, $relationship->primaryEntity);
		$this->assertSame('SubTest', $relationship->primaryEntity->key);
	}

	/**
	 * @test
	 */
	public function it_generates_the_child_methods()
	{
		$result = new Relationship('ManyTest<Test');

		$expected = 'return $this->belongsTo(ManyTest::class);';

		$this->assertStringContainsString($expected, $result->getChildRelationship());
	}

	/**
	 * @test
	 */
	public function it_generates_the_parent_methods()
	{
		$result = new Relationship('ManyTest<Test');

		$expected = 'return $this->hasMany(Test::class);';
		$this->assertStringContainsString($expected, $result->getParentRelationship('Test'));
	}

	/**
	 * @test
	 */
	public function it_loads_one_to_many_entities()
	{
		$this->setSettings([
			'entities' => [
				'Family/Parent' => ['defaults' => true],
				'Family/Child' => ['defaults' => true]
			],
			'relationships' => [
				'Family/Parent<Family/Child' =>
					['default' => true]
			]
		]);

		$relationship = new Relationship('Family/Parent<Family/Child');

		$this->assertInstanceOf(Entity::class, $relationship->primaryEntity);
		$this->assertInstanceOf(Entity::class, $relationship->childEntity);

	}

}
