<?php


namespace Alablaster\Architect\Tests\Feature\Entities\Builders;


use Alablaster\Architect\Domain\Core\Builders\StubBuilder;
use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Tests\TestCase;

class StubBuilderTest extends TestCase
{

	public function test_it_replaces_an_entity_name()
	{
		$entity = new Entity('Something/Else');

		$builder = new StubBuilder();

		$builder->setReference($entity);

		$builder->setStub('  @<< entity.name >>@');

		$result = (string) $builder;

		$this->assertStringContainsString('Else', $result);
	}

	public function test_it_utilizes_the_camel_filter()
	{
		$entity = new Entity('Something/OneMoreThing');

		$builder = new StubBuilder();

		$builder->setReference($entity);

		$builder->setStub('  @<< entity.name.camel >>@');

		$result = (string) $builder;

		$this->assertStringContainsString('oneMoreThing', $result);
	}

	public function test_it_utilizes_the_snake_filter()
	{
		$entity = new Entity('Something/OneMoreThing');

		$builder = new StubBuilder();

		$builder->setReference($entity);

		$builder->setStub('  @<< entity.name.snake >>@');

		$result = (string) $builder;

		$this->assertStringContainsString('one_more_thing', $result);
	}

	public function test_it_utilizes_the_upper_filter()
	{
		$entity = new Entity('Something/OneMoreThing');

		$builder = new StubBuilder();

		$builder->setReference($entity);

		$builder->setStub('  @<< entity.name.upper >>@');

		$result = (string) $builder;

		$this->assertStringContainsString('ONEMORETHING', $result);
	}
}