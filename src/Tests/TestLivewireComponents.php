<?php


namespace Alablaster\Architect\Tests;


/**
 * @codeCoverageIgnore
 */
trait TestLivewireComponents
{
	public function classHas($test)
	{
		$this->assertStringContainsString($test, $this->class);
	}

	public function viewHas($test)
	{
		$this->assertStringContainsString($test, $this->view);
	}

	public function tstHas($test)
	{
		$this->assertStringContainsString($test, $this->test);
	}

	public function classMissing($test)
	{
		$this->assertStringNotContainsString($test, $this->class);
	}

	public function viewMissing($test)
	{
		$this->assertStringNotContainsString($test, $this->view);
	}

	public function tstMissing($test)
	{
		$this->assertStringNotContainsString($test, $this->test);
	}
}