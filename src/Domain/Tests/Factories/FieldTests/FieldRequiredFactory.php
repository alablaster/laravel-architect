<?php


namespace Alablaster\Architect\Domain\Tests\Factories\FieldTests;


class FieldRequiredFactory extends FieldTestFactory
{
	protected  $title = 'FIELD_is_required_to_create_a_CLASS';

	protected function configureTitle() : void
	{
		$this->title = str_replace(
			'FIELD',
			$this->field->get('name'),
			$this->title);

		$this->title = str_replace(
			'CLASS',
			$this->entity->get('name'),
			$this->title);
	}

	protected function configureTest(): void
	{
		$test = [];

		$test[] = '$response = $this->create([';
		$test[] = "\t'" . $this->field->get('name') . "' => null";
		$test[] = ']);';
		$test[] = null;
		$test[] = null;
		$test[] = '$this->assertValidationError(\'' . $this->field->get('name') . '\', $response);';

		$this->addToTest($test);
	}

}