<?php


namespace Alablaster\Architect\Domain\Entities\Factories;


use Alablaster\Architect\Domain\Core\Builders\StubBuilder;
use Alablaster\Architect\Domain\Core\Factories\FileLocationFactory;
use Alablaster\Architect\Domain\Core\Traits\InteractsWithFilesTrait;
use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Domain\Entities\Factories\LivewirePartialFactory;
use Alablaster\Architect\Domain\Entities\Field;
use Illuminate\Support\Str;

class LivewireShowFactory extends LivewirePartialFactory
{
	use InteractsWithFilesTrait;

	protected function buildClass(): void
	{
		$this->builder->getStubFrom(__DIR__ . '/../stubs/partials/livewire/show.class.php.stub');
		$this->builder->setLocation(new FileLocationFactory('livewire.partial.class', $this->entity) . 'Show.php');

		$this->builder->build();
	}

	protected function buildView(): void
	{
		$this->builder->getStubFrom(__DIR__ . '/../stubs/partials/livewire/show.view.blade.php.stub');
		$this->builder->setLocation(new FileLocationFactory('livewire.partial.view', $this->entity) . 'show.blade.php');
		$this->builder->addSlot('fields', $this->makeShowInputs());
		$this->builder->build();
	}

	protected function buildTest(): void
	{

		$this->builder->getStubFrom(__DIR__ . '/../stubs/partials/livewire/show.test.php.stub');
		$this->builder->setLocation(new FileLocationFactory('livewire.partial.test', $this->entity) . 'ShowTest.php');

		$this->builder->build();
	}

	protected function makeShowInputs()
	{

		$inputs = '';

		$builder = new StubBuilder();

		$builder->getStubFrom(__DIR__ . '/../stubs/partials/livewire/show-input.stub');

		foreach($this->entity->fields->getFields() as $field)
		{
			if($field->visible)
			{
				$builder->setReference($field);

				$inputs .= $builder;
			}
		}

		return $inputs;
	}
}