<?php


namespace Alablaster\Architect\Domain\Tests\Factories;


use Alablaster\Architect\Domain\Core\Builders\StubBuilder;
use Alablaster\Architect\Domain\Core\Factories\FileLocationFactory;
use Alablaster\Architect\Domain\Core\Traits\InteractsWithFilesTrait;
use Alablaster\Architect\Domain\Entities\Entity;
use Illuminate\Support\Str;

class CreateTestFactory extends StubBuilder
{
	private $entity;
	use InteractsWithFilesTrait;

	public function __construct(Entity $entity)
	{
		$stub = $this->openFile(__DIR__ . '/../stubs/create_test.stub');
		$location = new FileLocationFactory('factory', $entity);
		$references = [Entity::class => $entity];

		parent::__construct($stub, $location, $references);
	}

//	public function __toString()
//	{
//		return $this->makeCreateTest();
//	}
//
//	private function makeCreateTest() : string
//	{
//
//
//		$stub = str_replace(
//			'{{ namespace }}',
//			$this->getNamespace(),
//			$stub);
//
//		$stub = str_replace(
//			'// Imports',
//			"// Imports\nuse " . $this->entity->getFullName() . ';',
//			$stub);
//
//		$stub = str_replace(
//			'{{ class }}',
//			$this->entity->name,
//			$stub);
//
//		$stub = str_replace(
//			'{{ name }}',
//			Str::snake($this->entity->get('name')),
//			$stub);
//
//		$stub = str_replace(
//			'{{ table }}',
//			$this->entity->get('table'),
//			$stub);
//
//		$stub = str_replace(
//			'{{ store }}',
//			$this->storeRouteName(),
//			$stub);
//
//		$stub = str_replace(
//			'// Field Tests',
//			$this->fieldTests(),
//			$stub);
//
//		return $stub;
//	}
//
//	private function getNamespace()
//	{
//		$path = explode('/', config('architect.path.tests.feature'));
//		$path = array_merge($path, explode('\\', $this->entity->get('namespace')));
//
//		foreach($path as $key => $item)
//		{
//			if($item == null)
//			{
//				unset($path[$key]);
//			} else {
//				$path[$key] = Str::studly($item);
//			}
//		}
//
//		return implode('\\', $path);
//	}
//
//	private function storeRouteName()
//	{
//		$name = explode('\\', $this->entity->get('namespace'));
//		$name[] = $this->entity->get('name');
//
//		foreach($name as $key => $value)
//		{
//			if($value == null)
//			{
//				unset($name[$key]);
//			} else {
//				$name[$key] = Str::plural(Str::kebab($value));
//			}
//		}
//
//		return implode('.', $name) . '.store';
//	}
//
//	private function fieldTests()
//	{
//		$tests = '';
//
//		$ignore = ['id', 'created_at', 'updated_at'];
//		foreach($this->entity->fields->getFields() as $field)
//		{
//			if(!key_exists($field->name, $ignore)){
//				$tests .= new CreateFieldTestFactory($field, $this->entity);
//			}
//		}
//
//		return $tests;
//	}
//

}