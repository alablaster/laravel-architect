<?php


namespace Alablaster\Architect\Tests\Feature\Entities\Builders;


use Alablaster\Architect\Domain\Core\Builders\Builder;
use Alablaster\Architect\Domain\Core\Builders\StubBuilder;
use Alablaster\Architect\Domain\Core\Traits\SettingsTrait;
use Alablaster\Architect\Domain\Entities\Builders\RequestBuilder;
use Alablaster\Architect\Domain\Entities\Builders\StoreRequestBuilder;
use Alablaster\Architect\Domain\Entities\Builders\UpdateRequestBuilder;
use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Tests\TestCase;
use Illuminate\Session\Store;

class StoreRequestBuilderTest extends TestCase
{

	use SettingsTrait;

	/**
	 * @test
	 */
	public function it_is_an_instance_of_StubBuilder()
	{
		$builder = new StoreRequestBuilder(new Entity('Test'));

		$this->assertInstanceOf(StubBuilder::class, $builder);
	}

	/**
	 * @test
	 */
	public function it_returns_the_correct_class_name()
	{

		$this->setSettings([]);

		$entity = new Entity('Test');

		$builder = new StoreRequestBuilder($entity);

		$this->assertStringContainsString('class StoreRequest', $builder);
	}

	/**
	 * @test
	 */
	public function it_ignores_fields_with_in_request_set_to_false()
	{

		$entity = new Entity('Test', ['fields' => ['not_included' => ['in_request' => false], 'included' => ['default' => true]]]);

		$request = new StoreRequestBuilder($entity);


		$this->assertStringNotContainsString('not_included', $request);
		$this->assertStringContainsString('included', $request);

	}



}