<?php


namespace Alablaster\Architect\Domain\Entities\Traits;


use Alablaster\Architect\Domain\Core\Exceptions\DefinitionDoesNotConformToStandard;
use Alablaster\Architect\Domain\Core\Traits\SettingsTrait;
use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Domain\Settings\Managers\Definitions;

trait LoadsEntities
{

	public function entities()
	{
		$definitions = app(Definitions::class);
		$entities = [];
		foreach($definitions->entities() as $key => $entity)
		{
			$entities[] = new Entity($key);
		}

		return collect($entities);
	}

	public function getEntityByKey($key)
	{
		if($this->entities()->where('key',  trim($key))->count())
		{
			return $this->entities()->where('key',  trim($key))->first();
		} else {
			throw new DefinitionDoesNotConformToStandard($key . ' is not a defined entity key');
		}
	}

	public function entityKeyValid($key)
	{
		return (boolean) $this->entities()->where('key',  $key)->count();
	}
}
