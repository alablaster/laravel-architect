<?php


namespace Alablaster\Architect\Tests\Feature\SiteMap;


use Alablaster\Architect\Domain\SiteMap\Method;
use Alablaster\Architect\Domain\SiteMap\SiteNode;

class MethodTest extends \Alablaster\Architect\Tests\TestCase
{
	/**
	 * @test
	 */

	public function it_stores_the_path()
	{
		$node = new SiteNode('/test/test');
		$method = new Method($node, 'default', );

		$this->assertSame('/test/test', $method->get('path'));
	}

	/**
	 * @test
	 */

	public function it_has_a_default_method()
	{
		$node = new SiteNode('Test', [], '/test/test');
		$method = new Method($node, 'default', );

		$this->assertSame('GET', $method->get('method'));
	}

	/**
	 * @test
	 */

	public function it_has_a_default_action_with_a_namespace()
	{
		$node = new SiteNode('/something/test');
		$method = new Method($node, 'index');

		$this->assertSame(['Something\\TestController', 'index'], $method->get('action'));
	}

	/**
	 * @test
	 */

	public function it_has_a_default_action_without_a_namespace()
	{
		$node = new SiteNode('Test', [], '/test');
		$method = new Method($node, 'default');

		$this->assertSame(['TestController', 'default'], $method->get('action'));
	}
	/**
	 * @test
	 */

	public function it_has_a_default_title()
	{
		$node = new SiteNode('Test', [], '/test/test');
		$method = new Method($node, 'default');

		$this->assertSame( 'Test Index', $method->get('title'));
	}

}
