<?php

namespace Alablaster\Architect\Tests;

use Alablaster\Architect\Domain\Core\Traits\SettingsTrait;
use Illuminate\Support\Facades\Artisan;
use \Orchestra\Testbench\TestCase as BaseTestCase;
use Alablaster\Architect\ArchitectServiceProvider;

class TestCase extends BaseTestCase
{
	use SettingsTrait;

	public function getPackageProviders($app)
    {
        return [
            ArchitectServiceProvider::class
        ];
    }

}
