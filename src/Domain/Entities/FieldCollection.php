<?php


namespace Alablaster\Architect\Domain\Entities;

use Alablaster\Architect\Domain\Entities\Field;
use Illuminate\Support\Str;

class FieldCollection
{
	private $entityKey;
	private $fields;
	public $updateRules, $storeRules, $variables;

	const SYSTEM_FIELDS = [
		'id',
		'updated_at',
		'created_at'
	];

	public function __construct(array $fields, string $entityKey = null)
	{
		$this->entityKey = $entityKey;
		$this->loadFields($fields);
		$this->variables = $this->getVariables();
		$this->updateRules = $this->getUpdateRequestRules();
		$this->storeRules = $this->getStoreRequestRules();
	}

	public function getFields()
	{
		return $this->fields;
	}

	private function loadFields($fields)
	{
		$collection = array();

		$collection = $this->setId($fields, $collection);
		unset($fields['id']);

		if(count($fields) == 0) {
			$collection['name'] = new Field('name');
		}

		foreach($fields as $key => $field)
		{
			$collection[$key] = new Field($key, $field);
		}

		$collection = $this->setTimestamps($fields, $collection);

		$this->fields = collect($collection);
	}

	public function getField($key)
	{
		if($this->fields->where('key', $key)->count())
		{
			return $this->fields->where('key', $key)->first();
		}

		return false;
	}

	public function getRequiredFields()
	{
		return array_filter($this->fields->map(function($i) {
			if(!$i->nullable && !in_array($i->name, self::SYSTEM_FIELDS)) {
				return $i->name;
			}
		})->toArray());
	}

	public function getNullableFields()
	{
		return array_filter($this->fields->map(function($i) {
			if($i->nullable) {
				return $i->name;
			}
		})->toArray());
	}

	public function getStringFields()
	{
		return array_filter($this->fields->map(function($i) {
			if($i->type == 'string' || is_array($i->type) && $i->type['name'] == 'string') {
				return $i->name;
			}
		})->toArray());
	}

	public function getIntegerFields()
	{
		return array_filter($this->fields->map(function($i) {
			if($i->type == 'integer') {
				return $i->name;
			}
		})->toArray());
	}

	public function getFactoryDefinitions()
	{
		$definitions = $this->fields->map(function ($i) { return $i->factoryDefinition; });

		$return = '';

		foreach($definitions  as $definition){
			if($definition)
			{
				$return .= "\n\t\t\t" . $definition . ',';
			}
		}

		return $return;
	}

	public function addField($field)
	{
		$this->fields->push(new Field($field));
	}

	public function massAssignableFields()
	{
		return $this->fields->where('massAssignable', true);
	}

	public function massAssignableFieldNames()
	{
		return $this->massAssignableFields()->pluck('name');
	}

	public function getVisibleFields()
	{
		return $this->fields->where('visible');
	}


	public function getMigrationMethods()
	{
		$return = '';
		foreach($this->fields as $field) {
			$return .= $field->generateMigrationMethod();
		}

		$indicies = $this->getFields()->where('index', true);

		if($indicies->count())
		{
			$return .= "\n\t\t\t" . '$table->index(';

			foreach($indicies as $index)
			{
				$return .= "'" . $index . "', ";
			}

			$return .= ');';
		}

		return $return;
	}

	public function getStoreRequestRules()
	{
		$rules = '';
		foreach($this->fields as $field) {
			$rules .= $field->generateStoreRule();
		}

		return $rules;
	}

	public function getUpdateRequestRules()
	{
		$rules = '';
		foreach($this->fields as $field) {
			$rules .= $field->generateUpdateRule();
		}

		return $rules;
	}

	/**
	 * @param $fields
	 * @param array $collection
	 * @return array
	 */
	private function setId($fields, array $collection)
	{
		$definition = [
			'type' => 'id',
			'name' => 'id',
			'label' => 'ID',
			'mass_assignable' => false,
			'in_request' => false,
			'visible' => false,
		];

		if(isset($fields['id'])) {
			if($fields['id'] == false) {
				return $collection;
			}
			$definition = array_merge($fields['id'], $definition);
		}

		$collection['id'] = new Field('id', $definition);

		return $collection;
	}

	private function setTimestamps($fields, $collection)
	{
		$createdAt = [
			'key' => 'created_at',
			'label' => 'Created At',
			'type' => 'timestamp',
			'visible' => false,
			'faker' => false,
			'in_request' => false,
		];

		$updatedAt = [
			'key' => 'updated_at',
			'label' => 'Updated At',
			'type' => 'timestamp',
			'visible' => false,
			'faker' => false,
			'in_request' => false,
		];

		$deletedAt = [
			'key' => 'deleted_at',
			'label' => 'Deleted At',
			'type' => 'timestamp',
			'nullable' => 'true',
			'visible' => false,
			'faker' => false,
			'in_request' => false,
		];

		$archivedAt = [
			'key' => 'archived_at',
			'label' => 'Archived At',
			'type' => 'timestamp',
			'nullable' => 'true',
			'visible' => false,
			'faker' => false,
		];


		if(isset($fields['created_at'])) {
			$createdAt = array_merge($fields['created_at'], $createdAt);
		}
		if(!isset($fields['created_at']) || $fields['created_at'] != false)
		{
			$collection['created_at'] = new Field('created_at', $createdAt);
		}


		if(isset($fields['updated_at'])) {
			$updatedAt = array_merge($fields['updated_at'], $updatedAt);
		}

		if(!isset($fields['updated_at']) || $fields['updated_at'] != false)
		{
			$collection['updated_at'] = new Field('updated_at', $updatedAt);
		}

		if(isset($fields['deleted_at']) || isset($fields['softDeletes'])) {
			if(is_array($fields['deleted_at'])) {
				$deletedAt = array_merge($fields['deleted_at'], $deletedAt);
				$collection['deleted_at'] = new Field('deleted_at', $deletedAt);
			}
			if(is_array($fields['softDeletes'])) {
				$deletedAt = array_merge($fields['softDeletes'], $deletedAt);
				$collection['deleted_at'] = new Field('deleted_at', $deletedAt);
			}
		}

		if(isset($fields['archived_at']) || isset($fields['archives'])) {
			if(isset($fields['archived_at']) && is_array($fields['archived_at'])) {
				$deletedAt = array_merge($fields['archived_at'], $archivedAt);
				$collection['deleted_at'] = new Field('archived_at', $archivedAt);
			}
			if(isset($fields['archives']) && is_array($fields['archives'])) {
				$deletedAt = array_merge($fields['archives'], $deletedAt);
				$collection['archived_at'] = new Field('archived_at', $archivedAt);
			}
		}

		return $collection;
	}

	public function getVariables()
	{
		$variables = $this->getFields()->map(function($field) { if($field->visible) { return '$' . Str::camel($field->name); }})
			->toArray();

		$variables = array_filter($variables);

		return implode( ', ', $variables);
	}

	public function getFieldMap(): string
	{
		$map = [];
		foreach ($this->getFields() as $field){
			if($field->visible) {
				$map[] = "'" . $field->name . '\' => $this->' . Str::camel($field->name);
			}
		}

		return "\n\t\t\t\t" . implode(",\n\t\t\t\t", $map);
	}

	public function getMountingMap(): string
	{
		$map = [];
		foreach ($this->getFields() as $field){
			if($field->visible) {
				$map[] = '$this->' . Str::camel($field->name) . ' = $@<< entity.name.camel >>@->' . Str::camel($field->name) . ';';
			}
		}

		return "\n\t\t" . implode("\n\t\t", $map);
	}

}