<?php

namespace Alablaster\Architect\Tests\Feature\SiteMap\Factories;

use Alablaster\Architect\Domain\Core\Traits\SettingsTrait;
use Alablaster\Architect\Domain\SiteMap\Factories\Livewire\LivewireFactory;
use Alablaster\Architect\Domain\SiteMap\Factories\Livewire\LivewireViewFactory;
use Alablaster\Architect\Domain\SiteMap\Method;
use Alablaster\Architect\Domain\SiteMap\SiteNode;
use Alablaster\Architect\Tests\TestCase;

class LivewireFactoryTest extends TestCase
{
	use SettingsTrait;

	public $node;

	public function setUp(): void
	{
		parent::setUp();

		$this->addToSettingType('entities', 'Test/Entity');

		$this->node = new SiteNode('/entity', ['frontend' => 'livewire', 'entity' => 'Test/Entity']);
	}

	public function test_view_has_been_created()
	{
		new LivewireViewFactory(new Method($this->node, 'index'));

		$this->assertFileExists(base_path('/resources/views/livewire/entity/index.blade.php'));
	}

	public function test_class_has_been_created()
	{
		new LivewireViewFactory(new Method($this->node, 'index'));

		$this->assertFileExists(base_path('/resources/views/livewire/entity/index.blade.php'));
	}

}
