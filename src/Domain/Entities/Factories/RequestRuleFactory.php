<?php


namespace Alablaster\Architect\Domain\Entities\Factories;


use Alablaster\Architect\Domain\Entities\Field;

class RequestRuleFactory
{
	public $field;
	private $storeRules = [];
	private $updateRules = [];

	public function __construct(Field $field)
	{
		$this->field = $field;

		$this->stringRules();
		$this->uniqueRule();
		$this->integerRule();
		$this->requiredRule();
	}

	public function store() : array
	{
		return $this->storeRules;
	}

	public function update() : array
	{
		return $this->updateRules;
	}

	private function addRule(string $rule)
	{
		$this->addStoreRule($rule);
		$this->addUpdateRule($rule);
	}

	private function addStoreRule(string $rule)
	{
		$this->storeRules[] = $rule;
	}
	private function addUpdateRule(string $rule)
	{
		$this->updateRules[] = $rule;
	}

	private function integerRule()
	{
		if(is_string($this->field->type) && $this->field->type === 'integer'){
			$this->addRule('integer');
		}
	}

	private function uniqueRule()
	{
		if($this->field->unique != null){
			$this->addRule('unique:' . $this->field->unique['class'] . ',' . $this->field->unique['id']);
		}
	}

	private function stringRules()
	{
		if(is_array($this->field->type) && $this->field->type['name'] === 'string')
		{
			$this->addRule('string');
			$this->addRule('max:' . $this->field->type['max']);
		}
		if(is_string($this->field->type) && $this->field->type === 'string'){
			$this->addRule('string');
			$this->addRule('max:255');
		}
	}
	
	private function requiredRule()
	{
		if(!$this->field->nullable)
		{
			$this->addStoreRule('required');
			$this->addUpdateRule('sometimes');
		}
	}
}
