<?php


namespace Alablaster\Architect\Tests\Feature\Tests\Factories;


use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Domain\Tests\Factories\CreateTestFactory;
use Alablaster\Architect\Tests\TestCase;

class CreateTestFactoryTest extends TestCase
{

	public $test;

	public function setUp(): void
	{
		parent::setUp(); // TODO: Change the autogenerated stub

		$entity = new Entity('Family/Parent');

		$this->test = new CreateTestFactory($entity);
	}

	/**
	 * @test
	 */
	public function it_return_a_create_test_stub()
	{
		$this->assertStringContainsString('// create_test.stub', $this->test);
	}

	/**
	 * @test
	 */
	public function it_replaces_namespace_correctly()
	{
		$this->assertStringContainsString('namespace Tests\\Feature\\Family', $this->test);
		$this->assertStringNotContainsString('{{ namespace }}', $this->test);
	}

	/**
	 * @test
	 */
	public function it_uses_the_entity_model()
	{
		$this->assertStringContainsString('use App\\Models\\Family\\Parent', $this->test);
	}

	/**
	 * @test
	 */
	public function it_replaces_the_class_name_correctly()
	{
		$this->assertStringContainsString('CreateParentTest', $this->test);
		$this->assertStringNotContainsString('{{ class }}', $this->test);
	}

	/**
	 * @test
	 */
	public function it_replaces_name_correctly()
	{
		$this->assertStringContainsString('user_can_create_parent', $this->test);
		$this->assertStringNotContainsString('{{ name }}', $this->test);
	}


	/**
	 * @test
	 */
	public function it_replaces_the_store_route_correctly()
	{
		$this->assertStringContainsString('families.parents.store', $this->test);
		$this->assertStringNotContainsString('{{ store }}', $this->test);
	}

	/**
	 * @test
	 */
	public function it_replaces_the_table_correctly()
	{
		$this->assertStringContainsString('parents', $this->test);
		$this->assertStringNotContainsString('{{ table }}', $this->test);
	}

	/**
	 * @test
	 */
	public function it_replaces_all_place_holders()
	{
		$this->assertStringNotContainsString('{{', $this->test);
		$this->assertStringNotContainsString('}}', $this->test);
	}

	/**
	 * @test
	 */
	public function it_add_test_for_string_fields()
	{
		$this->assertStringContainsString('families.parents.store', $this->test);
	}

}