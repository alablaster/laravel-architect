<?php

namespace Alablaster\Architect\Console\Commands;

use Alablaster\Architect\Domain\Core\Managers\ForemanManager;
use Alablaster\Architect\Domain\Core\Traits\InteractsWithFilesTrait;
use Illuminate\Console\Command;

class Blueprints extends Command
{
	
	use InteractsWithFilesTrait;
	
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'architect:blueprints';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Builds the blueprint yaml file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

	/**
	 * Execute the console command.
	 *
	 *
	 * @return int
	 */
    public function handle()
    {
    	$blueprints = $this->openFile(__DIR__ . '/stubs/blueprints.yaml');
    	
    	if(file_exists(base_path() . '/blueprints.yaml')) {
	    	print "Blueprints already exist.";
	    } else {
		    $this->saveFile(base_path() . '/blueprints.yaml', $blueprints);
		    print "Blueprints created.";
	    }
    }
}
