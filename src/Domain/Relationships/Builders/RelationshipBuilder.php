<?php


namespace Alablaster\Architect\Domain\Relationships\Builders;


use Alablaster\Architect\Domain\Core\Builders\Builder;
use Alablaster\Architect\Domain\Core\Factories\FileLocationFactory;
use Alablaster\Architect\Domain\Core\Traits\InteractsWithFilesTrait;
use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Domain\Relationships\Factories\RelationshipMigrationFactory;
use Alablaster\Architect\Domain\Relationships\Relationship;

class RelationshipBuilder
{
	
	use InteractsWithFilesTrait;
	
	protected $relationship;
	
	public function build(Relationship $relationship)
	{
		$this->relationship = $relationship;
		$this->modifyPrimary();
		$this->modifyChildren();
		$this->createMigration();
	}
	
	private function modifyPrimary() {

		$parent = $this->loadModel($this->relationship->primaryEntity);
		
		$imports = $this->relationship->getParentImports();
		$parent = $this->addImports($parent, $imports);
		
		$method = $this->relationship->getParentRelationship();
		$parent = $this->addMethod($parent, $method);
		
		$this->saveModel($this->relationship->primaryEntity, $parent);
	}
	
	private function modifyChildren() {
		$child = $this->loadModel($this->relationship->childEntity);
		
		$import = $this->relationship->getChildImport();
		$child = $this->addImports($child, [$import]);
		
		$method = $this->relationship->getChildRelationship();
		$child = $this->addMethod($child, $method);
		
		$this->saveModel($this->relationship->childEntity, $child);
	}

	private function addImports($model, $imports)
	{
		foreach($imports as $import)
		{
			if(!strpos($model, $import))
			{
				$import = "// Imports \nuse " . $import . ';';
				
				$model = str_replace('// Imports', $import, $model);
			}
		}
		
		return $model;
	}
	
	private function addMethod($model, $method)
	{
		$import = "// Relationships \n\t\t" . $method;
		
		return str_replace('// Relationships', $import, $model);
	}

	private function loadModel(Entity $entity)
	{
		return $this->openFile(base_path(new FileLocationFactory('model', $entity)));
	}

	private function saveModel(Entity $entity, string $content)
	{
		$this->saveFile(base_path(new FileLocationFactory('model', $entity)), $content);
	}

	private function createMigration()
	{
		$path = new FileLocationFactory('migration.relationship', $this->relationship);
		
		$migration = new RelationshipMigrationFactory($this->relationship);
		
		$this->saveFile(base_path() . $path, $migration);
	}
}
