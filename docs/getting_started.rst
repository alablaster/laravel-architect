Getting Started
================

How to Install
^^^^^^^^^^^^^^^^^
In a fresh installation of Laravel, run the following.

`composer install-dev Alablaster/Architect`;

Next publish the vendor files:

`php artisan vendor:publish --provider="Vendor\\Alablaster\\ArchitectServiceProvider"`

Next to generate your blueprints file, `blueprint.yaml` in the project's root directory run:

`php artisan architect:blueprints`

How to use Architect
^^^^^^^^^^^^^^^^^^^^^^

Open the `blueprint.yaml` file in your route folder. It has been seeded with a few simple examples
of how to design an application. The file is broken down into several top level domains including
`entities`, `relationships`, and `map`.

With in each of these you can define an object with a 'key'. Based on that key, several default
values are assumed, and but can then be overwitten by passing the name of that setting, and  a
new value.

Once you have edited the blueprints for your project, you can build it using the following command

`php artisan architect:foundation`

Tips
^^^^^^^^^^^^^^

Create a new git branch in order to build Architect Foundation, then while you merge the branch back into your main
branch you can review each file that has been created. This can be particularly helpful if you are trying to run
Architect on an application which has already been created.

Plan in advance. We all like to dive straight into code, but Architect pays big dividends to pre-planning. Specifically,
for planning out all your entities, and relationships before starting your application. Think less about your MVP and more
about your ultimate goal with the application. If you run Architect on a separate branch you could then merge in
just those files needed for your MVP, and store the rest on the branch until you are ready to use them.

Contribute! If you are finding yourself doing something repetitively, consider implementing it into the Architect
utility.