<?php


namespace Alablaster\Architect\Domain\SiteMap;


use Alablaster\Architect\Domain\Entities\Field;
use Alablaster\Architect\Domain\Frontend\Factories\RouteFactory;
use Alablaster\Architect\Domain\SiteMap\Factories\Livewire\LivewireViewFactory;
use Illuminate\Support\Str;

class MethodCollection
{
	private $methods;

	public function __construct(array $methods)
	{
		$this->methods = collect($methods);
	}

	public function __invoke()
	{
		return $this->methods;
	}

	public function getMethods()
	{
		return $this->methods;
	}

	public function getControllerMethods()
	{
		$return = '';

		foreach($this->methods as $method)
		{
			$return .= "\n" . $method->getControllerMethod();
		}
	}

	public function getMethod($type)
	{
		return $this->methods->where('type', $type)->first();
	}

	// TODO: This method feels like a good candidate for refactor.
	public function getRoutesActions()
	{
		$routes = [];

		foreach($this->getMethods() as $method)
		{
			$routes[] = new RouteFactory($method);
//
//			if(in_array($method->get('type'), ['default', 'invoke']))
//			{
//				$routes[] = 'Route::' . Str::lower($method->get('method')) . '(\'' . $method->get('path') . '\', [\'' . $method->get('action')[0] . '\'])->name(\'' . $method->get('route') . '\');';
//			}
//
//			if($method->node->get('frontend') == 'livewire') {
//
//				// generate Livewire files
//				new LivewireViewFactory($method);
//
//				// generate route to Livewire
//				$routes[] = 'Route::' . Str::lower($method->get('method')) . '(\'' . $method->get('path') . '\', App\\Http\\' . $method->get('action')[0] . '\\' .  Str::studly($method->get('action')[1]) . '::class)->name(\'' . $method->get('route') . '\');';
//			} else {
//				$routes[] = 'Route::' . Str::lower($method->get('method')) . '(\'' . $method->get('path') . '\', [\'' . $method->get('action')[0] . '\', \'' .  $method->get('action')[1] . '\'])->name(\'' . $method->get('route') . '\');';
//			}
		}

		return implode("\n",  $routes);
	}
}