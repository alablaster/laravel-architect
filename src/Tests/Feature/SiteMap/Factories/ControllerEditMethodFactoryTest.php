<?php


namespace Alablaster\Architect\Tests\Feature\SiteMap\Factories;


use Alablaster\Architect\Domain\Core\Traits\SettingsTrait;
use Alablaster\Architect\Domain\SiteMap\Builders\ControllerBuilder;
use Alablaster\Architect\Domain\SiteMap\Method;
use Alablaster\Architect\Domain\SiteMap\MethodCollection;
use Alablaster\Architect\Domain\SiteMap\SiteNode;
use Alablaster\Architect\Tests\TestCase;

class ControllerEditMethodFactoryTest extends TestCase
{
	use SettingsTrait;

	public $node;

	public function setUp(): void
	{
		parent::setUp();

		$this->setSettings([
			'entities' => [
				'Test' => ['fields' => [
					'name' => ['default' => true]
					]
				]
			]
		]);

		$this->node = new SiteNode('/tests', [
			'entity' => 'Test',
			'type' => 'resource',
			'methods' => ['edit']
		]);

	}

	public function getMethod()
	{
		return $this->node->methods->getMethod('edit');
	}


	public function test_methods_on_node_are_an_instance_of_method_collection()
	{
		$this->assertInstanceOf(MethodCollection::class, $this->node->methods);
	}

}