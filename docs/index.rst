.. Laravel Architect documentation master file, created by
   sphinx-quickstart on Sat Jan 23 23:15:02 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Laravel Architect
=============================================
Laravel Architect is a highly opinionated application scaffolding tool. By highly opinionated, we mean
that there is almost always an assumed default value, which means you only need to define preferences
which are different than the defaults.

For example, if you define an 'Entity' with the key of `Family/Child,` Architect will make the following
assumptions.

* Throughout the application files should namespaced as `Family`.
* A model should be created called Child.
* A migration should be created for a table called children.
* Be cause no fields were passed, it as assumed the table should have the fields `id`, `name`, `created_at`, `updated_at`, with `name` being a varchar(255).
* The `name` field will be added to the Child model as mass assignable.
* A Store and Update request will be created for child, which requires, `name` to be a 255 long string.
* A resource and collection file will be created using the JSON API v.1 standard which include `name` as an attribute.
* Then, depending on the frontend being used, various snipits will be created.

This isn't meant to be no code tool, but rather a tool for minimizing taking care of the repetitive work
which comes when you are first starting a project. Once the basics have been scaffolded, you can then go
in to make changes to the part which really are custom to your project. As a bonus, you get to have
the consistency of every single portion of your application following a consistent design pattern.

.. toctree::
   :maxdepth: 2
   :caption: Overview:

   getting_started
   configuration

.. toctree::
   :maxdepth: 2
   :caption: YAML File:

   entities
   relationships
   fields
   frontend

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
