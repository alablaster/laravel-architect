<?php


namespace Alablaster\Architect\Domain\Frontend\Managers;


use Alablaster\Architect\Domain\Core\Builders\StubBuilder;
use Alablaster\Architect\Domain\Core\Interfaces\StubBuilderReference;
use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Domain\SiteMap\Builders\RoutesBuilder;
use Alablaster\Architect\Domain\SiteMap\SiteNode;
use function PHPUnit\Framework\isInstanceOf;

class FrontendManager
{
	protected $map;

	protected $routeBuilder;

	public function __construct($map)
	{
		$this->map = $map;

		$this->routeBuilder = new RoutesBuilder();

		$this->generateRoutes($map, config('architect.path.routes'));

	}

	private function generateRoutes($nodes, $parent)
	{
		foreach($nodes as $level => $subnodes)
		{
			$this->generateLevelRouteFile($level, $subnodes, $parent);
		}
	}

	/**
	 * generate the route file at the path level
	 *
	 * @param string $level
	 * @param array $nodes
	 * @param string $parent
	 */
	private function generateLevelRouteFile(string $level, array $nodes, $parent = '')
	{
		$fileName = $parent . '/' . $level;
		$this->routeBuilder->setLocation($fileName . '/web.php');
		$this->routeBuilder->getStubFrom(__DIR__ . '/../stubs/routes/default.stub');
		if($nodes['_node']->entity) {
			$this->routeBuilder->setReference($nodes['_node']->entity);
		}

		$this->routeBuilder->setReference($nodes['_node']);
		unset($nodes['_node']);

		$this->routeBuilder->addSlot('requireds', $this->createRequireds(array_keys($nodes)));

		$this->routeBuilder->build();

		$this->generateRoutes($nodes, $parent);
	}


	/**
	 * generate a list of the sub routes which need to be required.
	 *
	 * @param array $includes
	 * @return string
	 */
	private function createRequireds(array $includes)
	{
		$return = '';

		foreach($includes as $include)
		{
			$return .= "\nrequire __DIR__ . '/$include/web.php';";
		}

		return $return;
	}
}