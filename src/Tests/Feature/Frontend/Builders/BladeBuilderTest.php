<?php


namespace Alablaster\Architect\Tests\Feature\Frontend\Builders;


use Alablaster\Architect\Domain\Core\Traits\InteractsWithFilesTrait;
use Alablaster\Architect\Domain\Frontend\Builders\BladeBuilder;
use Alablaster\Architect\Domain\SiteMap\Builders\ControllerBuilder;
use Alablaster\Architect\Domain\SiteMap\Method;
use Alablaster\Architect\Domain\SiteMap\SiteNode;
use Alablaster\Architect\Tests\TestCase;

class BladeBuilderTest extends TestCase
{

	use InteractsWithFilesTrait;

	public function test_it_creates_the_expected_default_file()
	{
		$this->deleteFile(base_path('/resources/views/something/test.blade.php'));
		$this->assertFileDoesNotExist(base_path('/resources/views/something/test.blade.php'));

		$node = new SiteNode('something/test', ['frontend' => 'blade']);
		$method = new Method($node);
		new BladeBuilder($method);

		$this->assertFileExists(base_path('/resources/views/something/test.blade.php'));
	}

	public function test_it_creates_the_expected_show_file()
	{
		$this->deleteFile(base_path('/resources/views/something/test/show.blade.php'));
		$this->assertFileDoesNotExist(base_path('/resources/views/something/test/show.blade.php'));

		$node = new SiteNode('something/test', ['frontend' => 'blade']);
		$method = new Method($node, 'show');
		new BladeBuilder($method);

		$this->assertFileExists(base_path('/resources/views/something/test/show.blade.php'));
	}

	public function test_it_uses_the_correct_template()
	{
		$this->deleteFile(base_path('/resources/views/something/test/show.blade.php'));
		$this->assertFileDoesNotExist(base_path('/resources/views/something/test/show.blade.php'));

		$node = new SiteNode('something/test', ['frontend' => 'blade']);
		$method = new Method($node, 'show');
		new BladeBuilder($method);
		$blade = $this->openFile(base_path('/resources/views/something/test/show.blade.php'));

		$this->assertStringContainsString('blade/default.stub', $blade);
	}

	public function test_it_includes_the_default_todo()
	{
		$this->deleteFile(base_path('/resources/views/something/test/show.blade.php'));
		$this->assertFileDoesNotExist(base_path('/resources/views/something/test/show.blade.php'));
		$node = new SiteNode('something/test', ['frontend' => 'blade']);
		$method = new Method($node, 'show');
		new BladeBuilder($method);
		$blade = $this->openFile(base_path('/resources/views/something/test/show.blade.php'));

		$this->assertStringContainsString('TODO: Implement blade template.', $blade);
	}
}