<?php

namespace Tests\Feature\Traits;

use Alablaster\Architect\Domain\Core\Traits\SettingsTrait;
use Illuminate\Support\Facades\App;
use Alablaster\Architect\Tests\TestCase;
use Illuminate\Support\Facades\Storage;

class SettingsTraitTest extends TestCase
{
    use SettingsTrait;

    /**
     * @test
     */
    public function trait_saves_array_as_file_and_reopens_it_as_array()
    {
        $test = [
            'foo' => 'woo'
        ];

        //clear settings
        $this->setSettings([]);

        $this->setSettings($test);

        $result = $this->getSettings();

        $this->assertSame($test, $result);
    }

    /**
     * @test
     */
    public function getSettings_provides_blank_array_if_no_data_exists()
    {

        unlink(base_path() . '/blueprints.yaml');

        $result = $this->getSettings();

        $this->assertSame([], $result);
    }
}
