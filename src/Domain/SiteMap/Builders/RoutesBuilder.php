<?php


namespace Alablaster\Architect\Domain\SiteMap\Builders;


use Alablaster\Architect\Domain\Core\Builders\Builder;
use Alablaster\Architect\Domain\Core\Builders\StubBuilder;
use Alablaster\Architect\Domain\Core\Factories\FileLocationFactory;
use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Domain\SiteMap\SiteNode;
use Illuminate\Support\Str;
use Symfony\Component\VarDumper\Cloner\Stub;

class RoutesBuilder extends StubBuilder
{
	public $nodes;
	private $type;

	public function __construct()
	{
		$stub =$this->openFile( __DIR__ . '/../stubs/routes.stub');

		$location = new FileLocationFactory('routes', $this->type);

		parent::__construct($stub, $location);
	}


	private function generateRoutes($nodes)
	{
		$routes = '';

		echo "\nGenerating Frontend";

		foreach($nodes as $node) {
			$routes .= "\n" . $node->getRoutes();
			echo '.';
		}

		return $routes;
	}

}