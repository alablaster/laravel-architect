<?php


namespace Alablaster\Architect\Domain\Frontend\Factories;


use Alablaster\Architect\Domain\Core\Builders\StubBuilder;
use Alablaster\Architect\Domain\SiteMap\Method;
use phpDocumentor\Reflection\Utils;

class RouteFactory extends StubBuilder
{
	protected $method;
	protected $stubs;

	public function __construct(Method $method)
	{
		$this->method = $method;

		$references = [Method::class => $method];

		parent::__construct('', '', $references);

		$this->stubs = __DIR__ . '/../stubs/routes/route/';

		if($this->method->node->get('auth')) {
			$this->stubs .= 'auth/';
		}

		$this->loadStub($method->get('type'));

	}
	protected function loadStub(string $type)
	{
		switch ($this->method->node->get('frontend')) {
			case 'livewire':
				$this->loadLivewireStub($type);
			default:
				$this->loadDefaultStub($type);
		}
	}

	protected function loadDefaultStub($type)
	{

		switch ($type) {
			default:
				$this->getStubFrom($this->stubs . 'default.stub');
		}

	}

	protected function loadLivewireStub($type)
	{
		switch ($type) {
			default:
				$this->getStubFrom($this->stubs . 'default.stub');
		}

	}
}