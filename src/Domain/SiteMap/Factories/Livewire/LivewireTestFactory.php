<?php


namespace Alablaster\Architect\Domain\SiteMap\Factories\Livewire;


use Illuminate\Support\Str;
use function PHPUnit\Framework\stringContains;

class LivewireTestFactory extends LivewireViewFactory
{

	public function __construct($method)
	{
		parent::__construct($method);
	}

}