<?php


namespace Alablaster\Architect\Domain\FileLocations\Factories;


use Alablaster\Architect\Domain\Core\Definition;
use Alablaster\Architect\Domain\Core\Interfaces\StubBuilderReference;
use Illuminate\Support\Str;

class FileLocationFactory extends Definition
{
	public $name;
	public $node;

	/**
	 * Break a path with eitehr forward or backslashes into an array
	 *
	 * @param string $string
	 * @return array|null
	 */
	protected function breakApart(string $string): array
	{
		return array_filter(preg_split('/\\\\|\//', $string));
	}

	/**
	 * Create a namespace based on a path and set node
	 *
	 * @param string $path
	 * @return string
	 */
	public function makeNamespace(string $path): string
	{
		$namespace = $this->breakApart($path);
		$this->node = array_pop($namespace);

		foreach($namespace as $key => $value)
		{
			$namespace[$key] = Str::studly($value);
		}

		return implode('\\', $namespace);
	}

	/**
	 * Create a name based on a path
	 *
	 * @param string $path
	 * @return string
	 */
	public function makeName(string $path): string
	{
		$path = $this->breakApart($path);
		foreach($path as $key => $value) {
			$path[$key] = Str::kebab($value);
		}
		return implode('.', $path);
	}

	/**
	 * Break a path with eitehr forward or backslashes into an array
	 *
	 * @param string $string
	 * @return string
	 */
	protected function makeStudly(string $path, $glue = '/'): string
	{
		$words = $this->breakApart($path);

		$words = array_map(function($word) {
			return Str::studly($word);
		}, $words);

		return implode($glue, $words);
	}
}