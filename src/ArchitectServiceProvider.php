<?php

namespace Alablaster\Architect;

use Alablaster\Architect\Console\Commands\Blueprints;
use Alablaster\Architect\Console\Commands\Foundation;
use Alablaster\Architect\Domain\BaseComponents\Classes\Livewire\Input;
use Alablaster\Architect\Domain\Core\DefinitionsInterface;
use Alablaster\Architect\Domain\Settings\Managers\Definitions;
use Alablaster\Architect\Domain\Settings\Managers\SettingsManager;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;

class ArchitectServiceProvider extends ServiceProvider
{
    public function register()
    {
	    $this->app->singleton(SettingsManager::class);
	    $this->app->singleton(Definitions::class);

        $this->mergeConfigFrom(__DIR__.'/config/config.php', 'architect');
    }

    public function boot()
    {

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/config/config.php' => config_path('architect.php'),
            ], 'config');
        }

	    $this->loadViewsFrom(__DIR__ . '/src/Domain/BaseComponents/views', 'architect');

	    Blade::component('architect-input', Input::class);

        $this->commands([
        	Foundation::class,
	        Blueprints::class
        ]);
    }
}
