<?php


namespace Alablaster\Architect\Domain\Tests\Factories;


use Alablaster\Architect\Domain\Core\Traits\InteractsWithFilesTrait;
use Alablaster\Architect\Domain\Entities\Entity;
use Alablaster\Architect\Domain\Entities\Field;
use Alablaster\Architect\Domain\Tests\Traits\Required;

class CreateFieldTestFactory
{
	use InteractsWithFilesTrait;
	use Required;

	private $field;
	protected $entity;

	public function __construct(Field $field, Entity $entity)
	{
		$this->field = $field;
		$this->entity = $entity;
	}

	public function __toString()
	{
		return $this->generateTests();
	}

	private function generateTests() : string
	{
		$tests = '';

		if(!$this->field->nullable)
		{
			$tests .= $this->required($this->field, $this->entity);
		}

		return $tests;
	}

}