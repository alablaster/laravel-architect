<?php


namespace Alablaster\Architect\Domain\SiteMap\Managers\Livewire;


use Alablaster\Architect\Domain\Core\Factories\FileLocationFactory;
use Alablaster\Architect\Domain\Core\Traits\InteractsWithFilesTrait;
use Alablaster\Architect\Domain\SiteMap\Method;

class LivewireFrontendManager
{
	use InteractsWithFilesTrait;

	private $method;

	public function __construct(Method $method)
	{
		$this->method = $method;

		$this->makeFiles();
	}

	protected function makeFiles()
	{
		$viewFileLocation = new FileLocationFactory('livewire.view', $this->method);
		$classFileLocation = new FileLocationFactory('livewire.class', $this->method);
		$testFileLocation = new FileLocationFactory('livewire.test', $this->method);

		new LivewireViewFactory('view', $this->method);
		new LivewireViewFactory('class', $this->method);
		new LivewireViewFactory('test', $this->method);
	}
}